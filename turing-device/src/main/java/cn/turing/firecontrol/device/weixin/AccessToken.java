package cn.turing.firecontrol.device.weixin;

import lombok.Data;

@Data
public class AccessToken {

        private String accessToken;
        //过期时间 当前系统时间+微信传来的过期时间
        private Long expiresTime;

        public AccessToken(String accessToken, String expiresIn) {
            this.accessToken = accessToken;
            this.expiresTime = System.currentTimeMillis()+Integer.parseInt(expiresIn)*1000;
        }

        /**
         * 判断token是否过期
         * @return
         */
        public boolean isExpired(){
            return System.currentTimeMillis()>expiresTime;
        }

}

