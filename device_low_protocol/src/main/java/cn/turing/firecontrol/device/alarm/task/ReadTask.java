package cn.turing.firecontrol.device.alarm.task;


import cn.turing.firecontrol.device.alarm.read.ModbusRtuRead;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ReadTask {

    @Autowired
    private ModbusRtuRead modbusRtuRead;

    /**
     * 执行完成后，间隔1秒再次读取设备数据
     */
    @Scheduled(fixedDelay = 1000)
    public void read() {
        try {
            String read = modbusRtuRead.read();
            System.out.println(read);
            //判断设备数据，是否检测到烟感，如果有，则报警
            //mq

        } catch (Exception e) {
            e.printStackTrace();
            log.info("设备异常");
            //上报数据，进行异常维护
        }
    }

}
