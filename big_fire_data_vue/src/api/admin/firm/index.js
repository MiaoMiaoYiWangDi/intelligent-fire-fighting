/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function page(query) {
    return request({
        url: '/api/device/deviceSensorManufacturer/pageList',
        method: 'get',
        params: query
    })
}

export function devicePage(query) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/pageList',
        method: 'get',
        params: query
    })
}

export function devicePoint() {
    return request({
        url: '/api/device/deviceMeasuringPoint/all',
        method: 'get',
    })
}

export function addObj(obj) {
    return request({
        url: '/api/device/deviceSensorManufacturer/add',
        method: 'post',
        data: obj
    })
}

export function deviceAddObj(obj) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/add',
        method: 'post',
        data: obj
    })
}

export function deleteQuery(id) {
    return request({
        url: '/api/device/deviceSensorManufacturer/deleteQuery?id=' + id,
        method: 'get',
    })
}

export function deviceDeleteQuery(id) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/deleteQuery?id=' + id,
        method: 'get',
    })
}

export function getObj(id) {
    return request({
        url: '/api/device/deviceSensorManufacturer/get?id='+id,
        method: 'get'
    })
}

export function deviceGetObj(id) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/get?id=' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/api/device/deviceSensorManufacturer/delete?id=' + id,
        method: 'get'
    })
}

export function deviceDelObj(id) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/delete?id=' + id,
        method: 'get'
    })
}

export function putObj(obj) {
    return request({
        url: '/api/device/deviceSensorManufacturer/update',
        method: 'post',
        data: obj
    })
}

export function devicePutObj(obj) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/update',
        method: 'post',
        data: obj
    })
}
