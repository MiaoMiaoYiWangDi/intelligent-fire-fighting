/*
  * Copyright (c) 2021.  北京维数科技有限公司
  * Date :  2021/3/25
  * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
  * Version ：1.0.0
  */

import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import permission from './modules/permission'
import user from './modules/user'
import pubconfig from './modules/pubconfig'
import getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        app,
        permission,
        user,
        pubconfig
    },
    getters
})

export default store
