/*
Navicat MySQL Data Transfer

Source Server         : 130
Source Server Version : 80025
Source Host           : 192.168.19.130:3306
Source Database       : turing-device

Target Server Type    : MYSQL
Target Server Version : 80025
File Encoding         : 65001

Date: 2021-07-14 14:53:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for device_abnormal
-- ----------------------------
DROP TABLE IF EXISTS `device_abnormal`;
CREATE TABLE `device_abnormal` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `BUILD_ID` int DEFAULT NULL COMMENT '建筑列表id',
  `EQU_ID` int DEFAULT NULL COMMENT '设备id',
  `ALRM_CATEGORY` char(100) DEFAULT NULL COMMENT '报警类型：0：故障，1：火警',
  `ALRM_TYPE` char(100) DEFAULT NULL COMMENT '报警类型',
  `ALRM_DATE` datetime DEFAULT NULL COMMENT '报警时间',
  `HANDLE_FLAG` char(1) DEFAULT NULL COMMENT '是否处理[1=是/0=否]',
  `FIRE_FLAG` char(1) DEFAULT NULL COMMENT '是否真实火警[2=火警测试/1=是/0=否]',
  `CONFIR_DATE` datetime DEFAULT NULL COMMENT '确认时间',
  `HANDLE_DATE` datetime DEFAULT NULL COMMENT '处理时间',
  `CONFIR_PERSON` varchar(100) DEFAULT NULL COMMENT '确认人',
  `HANDLE_PERSON` varchar(100) DEFAULT NULL COMMENT '处理人',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `ALRM_DATA` double(10,3) DEFAULT NULL COMMENT '传感器数据',
  `LOG_ID` char(64) DEFAULT NULL COMMENT '日志ID',
  `UNIT` varchar(100) DEFAULT NULL COMMENT '测点代号',
  `B_NAME` varchar(200) DEFAULT NULL COMMENT '建筑名称',
  `SENSOR_NO` varchar(50) DEFAULT NULL COMMENT '传感器编号',
  `EQUIPMENT_TYPE` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `FLOOR` int DEFAULT NULL COMMENT '楼层',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `MEASURING_POINT` varchar(100) DEFAULT NULL COMMENT '测点',
  `LEVEL` varchar(20) DEFAULT NULL COMMENT '报警等级',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统id',
  `DATA_UNIT` varchar(100) DEFAULT NULL COMMENT '数据单位',
  PRIMARY KEY (`ID`),
  KEY `device_abnormal_alrm_date` (`ALRM_DATE`),
  KEY `abnormal_index` (`ALRM_CATEGORY`,`HANDLE_FLAG`,`FIRE_FLAG`),
  KEY `countIndex` (`EQU_ID`,`ALRM_CATEGORY`),
  KEY `queryPage` (`ALRM_DATE`,`ALRM_CATEGORY`,`LEVEL`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_abnormal
-- ----------------------------
INSERT INTO `device_abnormal` VALUES ('1', '111', '150', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '1', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c24', null, null, '5', '建筑名称01', '111', '1', '1', '展厅01', ' 测点01', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('2', '111', '151', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '2', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c25', null, null, '6', '建筑名称02', '111', '1', '1', '展厅02', ' 测点02', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('3', '111', '152', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '3', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c26', null, null, '7', '建筑名称03', '111', '1', '1', '展厅03', ' 测点03', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('4', '111', '153', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '4', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c27', null, null, '8', '建筑名称04', '111', '1', '1', '展厅04', ' 测点04', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('5', '111', '154', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '5', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c28', null, null, '9', '建筑名称05', '111', '1', '1', '展厅05', ' 测点05', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('6', '111', '155', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '6', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c29', null, null, '10', '建筑名称06', '111', '1', '1', '展厅06', ' 测点06', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('7', '111', '156', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '1', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c24', null, null, '5', '建筑名称01', '111', '1', '2', '展厅01', ' 测点01', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('8', '111', '157', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '2', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c25', null, null, '6', '建筑名称02', '111', '1', '2', '展厅02', ' 测点02', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('9', '111', '158', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '3', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c26', null, null, '7', '建筑名称03', '111', '1', '3', '展厅03', ' 测点03', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('10', '111', '159', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '4', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c27', null, null, '8', '建筑名称04', '111', '1', '3', '展厅04', ' 测点04', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('11', '111', '160', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '5', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c28', null, null, '9', '建筑名称05', '111', '1', '3', '展厅05', ' 测点05', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('12', '111', '161', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '6', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c29', null, null, '10', '建筑名称06', '111', '1', '3', '展厅06', ' 测点06', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('13', '111', '162', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '1', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c24', null, null, '5', '建筑名称01', '111', '1', '-1', '房间11201', ' 测点0111', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('14', '111', '163', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '2', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c25', null, null, '6', '建筑名称02', '111', '1', '-1', '房间11304', ' 测点0112', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('15', '111', '164', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '3', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c26', null, null, '7', '建筑名称03', '111', '1', '-1', '房间11403', ' 测点0113', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('16', '111', '165', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '4', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c27', null, null, '8', '建筑名称04', '111', '1', '-1', '房间11508', ' 测点0114', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('17', '111', '166', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '5', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c28', null, null, '9', '建筑名称05', '111', '1', '-1', '房间11621', ' 测点0115', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('18', '111', '167', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '6', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c29', null, null, '10', '建筑名称06', '111', '1', '-1', '房间11751', ' 测点0116', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('19', '111', '168', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '7', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c30', null, null, '11', '建筑名称07', '111', '1', '-1', '房间11881', ' 测点0117', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('20', '111', '169', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '8', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c31', null, null, '12', '建筑名称08', '111', '1', '-1', '房间12011', ' 测点0118', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('21', '111', '170', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '9', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c32', null, null, '13', '建筑名称09', '111', '1', '-1', '房间12141', ' 测点0119', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('22', '111', '171', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '3', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c26', null, null, '7', '建筑名称03', '111', '1', '-2', '房间21751', ' 测点02111', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('23', '111', '172', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '4', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c27', null, null, '8', '建筑名称04', '111', '1', '-2', '房间21881', ' 测点02211', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('24', '111', '173', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '5', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c28', null, null, '9', '建筑名称05', '111', '1', '-2', '房间21881', ' 测点02311', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('25', '111', '174', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '6', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c29', null, null, '10', '建筑名称06', '111', '1', '-2', '房间21751', ' 测点02411', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('26', '111', '175', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '7', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c30', null, null, '11', '建筑名称07', '111', '1', '-2', '房间21881', ' 测点02511', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('27', '111', '176', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '8', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c31', null, null, '12', '建筑名称08', '111', '1', '-2', '房间21881', ' 测点02611', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('28', '111', '177', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '9', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c32', null, null, '13', '建筑名称09', '111', '1', '-2', '房间21751', ' 测点02711', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('29', '111', '178', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '10', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c33', null, null, '14', '建筑名称10', '111', '1', '-2', '房间21881', ' 测点02811', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('30', '111', '179', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '11', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c34', null, null, '15', '建筑名称11', '111', '1', '-2', '房间22011', ' 测点02911', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('31', '111', '180', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '10', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c33', null, null, '14', '建筑名称10', '111', '1', '3', '房间21881', ' 测点02811', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('32', '111', '181', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '11', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c34', null, null, '15', '建筑名称11', '111', '1', '3', '房间22011', ' 测点02911', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('33', '111', '182', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '12', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c35', null, null, '16', '建筑名称12', '111', '1', '4', '房间22141', ' 测点03011', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('34', '111', '183', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '13', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c36', null, null, '17', '建筑名称13', '111', '1', '4', '房间22271', ' 测点03111', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('35', '111', '184', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '14', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c37', null, null, '18', '建筑名称14', '111', '1', '4', '房间22401', ' 测点03211', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('36', '111', '185', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '15', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c38', null, null, '19', '建筑名称15', '111', '1', '5', '房间22531', ' 测点03311', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('37', '111', '186', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '16', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c39', null, null, '20', '建筑名称16', '111', '1', '5', '房间22661', ' 测点03411', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('38', '111', '187', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '17', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c40', null, null, '21', '建筑名称17', '111', '1', '6', '房间22791', ' 测点03511', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('39', '111', '188', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '18', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c41', null, null, '22', '建筑名称18', '111', '1', '6', '房间22921', ' 测点03611', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('40', '111', '189', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '19', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c42', null, null, '23', '建筑名称19', '111', '1', '7', '房间23051', ' 测点03711', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('41', '111', '190', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '20', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c43', null, null, '24', '建筑名称20', '111', '1', '8', '房间23181', ' 测点03811', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('42', '111', '191', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '21', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c44', null, null, '25', '建筑名称21', '111', '1', '8', '房间23311', ' 测点03911', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('43', '111', '192', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '22', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c45', null, null, '26', '建筑名称22', '111', '1', '8', '房间23441', ' 测点04011', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('44', '111', '193', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '23', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c46', null, null, '27', '建筑名称23', '111', '1', '8', '房间23571', ' 测点04111', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('45', '111', '194', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '24', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c47', null, null, '28', '建筑名称24', '111', '1', '8', '房间23701', ' 测点04211', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('46', '111', '195', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '25', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c48', null, null, '29', '建筑名称25', '111', '1', '9', '房间23831', ' 测点04311', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('47', '111', '196', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '26', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c49', null, null, '30', '建筑名称26', '111', '1', '10', '房间23961', ' 测点04411', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('48', '111', '197', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '27', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c50', null, null, '31', '建筑名称27', '111', '1', '11', '房间24091', ' 测点04511', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('49', '111', '198', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '28', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c51', null, null, '32', '建筑名称28', '111', '1', '11', '房间24221', ' 测点04611', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('50', '111', '199', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '29', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c52', null, null, '33', '建筑名称29', '111', '1', '11', '房间24351', ' 测点04711', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('51', '111', '200', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '30', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c53', null, null, '34', '建筑名称30', '111', '1', '11', '房间24481', ' 测点04811', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('52', '111', '201', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '31', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c54', null, null, '35', '建筑名称31', '111', '1', '11', '房间24611', ' 测点04911', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('53', '111', '202', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '32', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c55', null, null, '36', '建筑名称32', '111', '1', '11', '房间24741', ' 测点05011', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('54', '111', '203', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '33', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c56', null, null, '37', '建筑名称33', '111', '1', '12', '房间24871', ' 测点05111', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('55', '111', '204', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '34', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c57', null, null, '38', '建筑名称34', '111', '1', '12', '房间25001', ' 测点05211', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('56', '111', '205', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '35', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c58', null, null, '39', '建筑名称35', '111', '1', '12', '房间25131', ' 测点05311', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('57', '111', '206', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '36', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c59', null, null, '40', '建筑名称36', '111', '1', '12', '房间25261', ' 测点05411', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('58', '111', '207', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '37', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c60', null, null, '41', '建筑名称37', '111', '1', '12', '房间25391', ' 测点05511', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('59', '111', '208', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '38', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c61', null, null, '42', '建筑名称38', '111', '1', '12', '房间25521', ' 测点05611', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('60', '111', '209', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '39', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c62', null, null, '43', '建筑名称39', '111', '1', '12', '房间25651', ' 测点05711', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('61', '111', '210', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '40', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c63', null, null, '44', '建筑名称40', '111', '1', '12', '房间25781', ' 测点05811', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('62', '111', '211', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '41', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c64', null, null, '45', '建筑名称41', '111', '1', '12', '房间25911', ' 测点05911', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('63', '111', '212', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '42', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c65', null, null, '46', '建筑名称42', '111', '1', '12', '房间26041', ' 测点06011', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('64', '111', '213', '1', '1', '2021-05-24 19:19:17', '1', '1', '2021-05-24 19:19:17', '2021-05-24 19:19:17', 'amdin', 'amdin', '0', 'amdin', '1', '2021-05-24 19:19:17', 'amdin', '43', '2021-05-24 19:19:17', null, 'ac88ceb386aa4231b09bf472cb937c66', null, null, '47', '建筑名称43', '111', '1', '12', '房间26171', ' 测点06111', '1', '1', '次');

-- ----------------------------
-- Table structure for device_al_dn_relation
-- ----------------------------
DROP TABLE IF EXISTS `device_al_dn_relation`;
CREATE TABLE `device_al_dn_relation` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `AL_ID` int DEFAULT NULL COMMENT '报警等级id',
  `DN_ID` int DEFAULT NULL COMMENT '通知方式id',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_al_dn_relation
-- ----------------------------

-- ----------------------------
-- Table structure for device_alarm_level
-- ----------------------------
DROP TABLE IF EXISTS `device_alarm_level`;
CREATE TABLE `device_alarm_level` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `LEVEL` varchar(20) DEFAULT NULL COMMENT '报警等级',
  `COLOR` varchar(26) DEFAULT NULL COMMENT '报警颜色',
  `SORT` int DEFAULT NULL COMMENT '排序',
  `LEVEL_INSTRUCTIONS` varchar(255) DEFAULT NULL COMMENT '等级说明',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_alarm_level
-- ----------------------------
INSERT INTO `device_alarm_level` VALUES ('50', '1', '#FF003C', '1', '1级特大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('51', '2', '#FF003C', '2', '2级特大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('52', '3', '#FF003C', '3', '3级特大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('53', '4', '#FF003C', '4', '4级特大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('54', '5', '#FF003C', '5', '5级特大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('55', '6', '#FF003C', '6', '6级特大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('56', '7', '#FF003C', '7', '7级特大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('57', '8', '#FF6200', '8', '1级重大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('58', '9', '#FF6200', '9', '2级重大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('59', '10', '#FF6200', '10', '3级重大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('60', '11', '#FF6200', '11', '4级重大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('61', '12', '#FF6200', '12', '5级重大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('62', '13', '#FF6200', '13', '6级重大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('63', '14', '#FF6200', '14', '7级重大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_alarm_level` VALUES ('64', '15', '#FF6200', '15', '8级重大严重报警', '0', 'admin', '1', '2021-06-10 09:33:41', 'admin', '1', '2021-06-10 09:33:41', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for device_alarm_threshold
-- ----------------------------
DROP TABLE IF EXISTS `device_alarm_threshold`;
CREATE TABLE `device_alarm_threshold` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `MP_ID` int DEFAULT NULL COMMENT '测点id',
  `AL_ID` int DEFAULT NULL COMMENT '报警等级id',
  `ALARM_MIN` double DEFAULT NULL COMMENT '报警最小值',
  `ALARM_MAX` double DEFAULT NULL COMMENT '报警最大值',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `ALARM_SYMBOL_MAX` char(1) DEFAULT NULL COMMENT '预警等级是否包含最大值[0=不包含/1=包含]',
  `ALARM_SYMBOL_MIN` char(1) DEFAULT NULL COMMENT '预警等级是否包含最小值[0=不包含/1=包含]',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_alarm_threshold
-- ----------------------------

-- ----------------------------
-- Table structure for device_building
-- ----------------------------
DROP TABLE IF EXISTS `device_building`;
CREATE TABLE `device_building` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `BID` int DEFAULT NULL COMMENT '建筑物ID（字段无意义，暂时保留）',
  `OID` int DEFAULT NULL COMMENT '建筑管理单位ID',
  `B_NAME` varchar(200) DEFAULT NULL COMMENT '建筑名称',
  `B_ADDRESS` varchar(200) DEFAULT NULL COMMENT '建筑地址',
  `ZXQY` char(6) DEFAULT NULL COMMENT '6位行政区编码（应符合现行国家标准《中华人民共和国行政区划代码》GB2260的规定）',
  `STREET` char(3) DEFAULT NULL COMMENT '3位街道编码（应符合现行国家标准《县以下行政区划代码编码规则》GB10114的规定）',
  `ROAD` varchar(60) DEFAULT NULL COMMENT '路名',
  `MNPH` varchar(20) DEFAULT NULL COMMENT '门弄牌号',
  `LDZ` varchar(20) DEFAULT NULL COMMENT '楼栋幢',
  `ADDRESS_DETAIL` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `GIS` varchar(100) DEFAULT NULL COMMENT '地理坐标经纬度的十进制表达式并以逗号分开',
  `LINKMAN` varchar(50) DEFAULT NULL COMMENT '联系人',
  `LINKPHONE` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `B_STATE` char(1) DEFAULT NULL COMMENT '建筑情况[1=使用中,0未使用]',
  `B_TIME` datetime DEFAULT NULL COMMENT '竣工时间',
  `PROPERT_RIGHT` char(1) DEFAULT NULL COMMENT '建筑产权及使用情况[0=独家产权，独立使用、1=独立产权，多家使用、2=多家产权，多家使用]',
  `B_AREA` double(18,2) DEFAULT NULL COMMENT '建筑面积',
  `B_ZD_AREA` double(18,2) DEFAULT NULL COMMENT '占地面积',
  `B_HIGHT` double(6,2) DEFAULT NULL COMMENT '建筑高度',
  `B_ZC_AREA` double(18,2) DEFAULT NULL COMMENT '标准层面积',
  `UP_FLOOR` int DEFAULT NULL COMMENT '地上层数',
  `UP_FLOOR_AREA` double(18,2) DEFAULT NULL COMMENT '地上面积',
  `UNDER_FLOOR` int DEFAULT NULL COMMENT '地下层数',
  `UNDER_FLOOR_AREA` double(18,2) DEFAULT NULL COMMENT '地下面积',
  `B_STORE` varchar(20) DEFAULT NULL COMMENT '建筑分类',
  `B_STRTURE` varchar(20) DEFAULT NULL COMMENT '建筑结构',
  `B_STRTURE1` varchar(100) DEFAULT NULL COMMENT '建筑其他结构',
  `CTRLROOM_PLACE` varchar(100) DEFAULT NULL COMMENT '消防控制室位置',
  `FIRE_RATE` varchar(10) DEFAULT NULL COMMENT '耐火等级',
  `FIRE_DANGER` varchar(20) DEFAULT NULL COMMENT '火灾危险性',
  `MOSTWORKERR` int DEFAULT NULL COMMENT '最大容纳人数',
  `LIFT_COUNT` int DEFAULT NULL COMMENT '消防电梯数',
  `LIFT_PLACE` varchar(200) DEFAULT NULL COMMENT '消防电梯位置',
  `REFUGE_NUMBER` int DEFAULT NULL COMMENT '避难层数量',
  `REFUGE_AREA` double(18,2) DEFAULT NULL COMMENT '避难层面积',
  `REFUGE_PLACE` varchar(200) DEFAULT NULL COMMENT '避难层位置',
  `USE_KIND` varchar(200) DEFAULT NULL COMMENT '用户类型',
  `HAVE_FIREPROOF` char(1) DEFAULT NULL COMMENT '是否有自动消防设施[0=无、1=有]',
  `XFSS` varchar(255) DEFAULT NULL COMMENT '消防设施',
  `XFSS_OTHER` varchar(200) DEFAULT NULL COMMENT '其他消防设施',
  `XFSS_INTACT` char(1) DEFAULT NULL COMMENT '设施完好情况[1=合格；2=不合格]',
  `NEAR_BUILDING` varchar(500) DEFAULT NULL COMMENT '毗邻建筑情况',
  `GEOG_INFO` varchar(200) DEFAULT NULL COMMENT '地理情况',
  `HAVE_CTRLROOM` char(1) DEFAULT NULL COMMENT '消防控制室情况[0=无；1=有]',
  `USE_TYPE` varchar(10) DEFAULT NULL COMMENT '建筑用途分类',
  `SYS_ORGAN_ID` int DEFAULT '-1' COMMENT '消防管辖单位（默认-1）',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL COMMENT '创建者ID',
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL COMMENT '修改者ID',
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL COMMENT '部门ID',
  `TENANT_ID` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_building
-- ----------------------------
INSERT INTO `device_building` VALUES ('1', '1', '111', '东方电子科技大厦', '北京市海淀区创业路2号1号楼', '100029', '125', '上地八街', '7号', '1号楼', '北京市海淀区创业路2号1号楼', '116.313348,40.053734', '张三001', '13681241851', '1', '2019-05-15 08:58:48', '0', '12345.45', '154535445.54', '120.45', '120454.45', '12', '110454.45', '2', '10000.00', '商用及景点', '高楼', '含绿地及附属设施', '地上3楼', '2', '低级', '400', '2', '最左边', '3', '3000.00', '最左边', '普通用户', '0', '消防栓', '消防带', '1', '毗邻A座', '北京市海淀区创业路2号1号楼', '0', '商用及景点', '1', '0', 'admin', '1', '2019-05-15 08:58:48', 'admin', '1', '2020-05-15 08:58:48', '1111', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_building` VALUES ('2', '2', '112', '中关村国际孵化园', '北京市海淀区上地五街7号', '100030', '135', '上地五街', '2号', '3号楼', '北京市海淀区创业路2号1号楼', '116.312137,40.052837', '张三001', '13681241851', '1', '2019-04-15 08:58:48', '0', '189454.54', '154535445.54', '120.46', '120454.45', '13', '110454.45', '2', '10000.00', '商住两用', '钢筋混凝土', '含绿地及附属设施', '地上3楼', '2', '低级', '400', '2', '最左边', '3', '3000.00', '最左边', '物业操作员', '1', '消防栓', '消防带', '1', '毗邻A座', '北京市海淀区上地五街7号', '1', '商住两用', '1', '0', 'admin', '1', '2019-05-15 08:58:48', 'admin', '1', '2020-05-15 08:58:48', '1111', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_building` VALUES ('3', '3', '113', '上地国际创业园', '北京市海淀区上地信息路2号(上地七街)', '100031', '145', '上地七街', '7号院', '11号楼', '北京市海淀区创业路2号1号楼', '116.310298,40.052059', '张三001', '13681241851', '1', '2019-03-15 08:58:49', '1', '134454.54', '154535445.54', '120.47', '120454.45', '14', '110454.45', '2', '10000.00', '别墅', '阁楼', '含绿地及附属设施', '地上3楼', '2', '低级', '400', '2', '最左边', '3', '3000.00', '最左边', '物业管理者', '1', '消防栓', '消防带', '1', '毗邻A座', '北京市海淀区上地信息路2号(上地七街)', '1', '别墅', '1', '0', 'admin', '1', '2019-05-15 08:58:48', 'admin', '1', '2020-05-15 08:58:48', '1111', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_building` VALUES ('4', '4', '114', '融科融智创新园', '北京市海淀区上地八街7号院', '100032', '155', '上地八街', '139号', '19号楼', '北京市海淀区创业路2号1号楼', '116.312126,40.054485', '张三001', '13681241851', '1', '2019-04-15 08:58:49', '1', '134454.54', '154535445.54', '120.48', '120454.45', '15', '110454.45', '3', '10000.00', '高端住宅', '2层楼房', '含绿地及附属设施', '地上3楼', '2', '低级', '400', '2', '最左边', '3', '3000.00', '最左边', '物业管理者', '1', '消防栓', '消防带', '1', '毗邻A座', '北京市海淀区上地八街7号院', '1', '高端住宅', '1', '0', 'admin', '1', '2019-05-15 08:58:48', 'admin', '1', '2020-05-15 08:58:48', '1111', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_building` VALUES ('5', '5', '115', '融通高科大厦', '北京市海淀区北京市海淀区西二旗大街39号楼', '100033', '165', '西二旗大街', '129号', '39号楼', '北京市海淀区创业路2号1号楼', '116.315957,40.058251', '张三001', '13681241851', '1', '2019-05-15 08:58:50', '2', '134454.54', '154535445.54', '120.49', '120454.45', '16', '110454.45', '3', '10000.00', '社区', '高层楼房', '含绿地及附属设施', '地上3楼', '2', '低级', '400', '2', '最左边', '3', '3000.00', '最左边', '物业管理者', '1', '消防栓', '消防带', '1', '毗邻A座', '北京市海淀区北京市海淀区西二旗大街39号楼', '1', '社区', '1', '0', 'admin', '1', '2019-05-15 08:58:48', 'admin', '1', '2020-05-15 08:58:48', '1111', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_building` VALUES ('6', '111', '111', '东方电子科技大厦', '北京市海淀区创业路2号1号楼', '100029', '125', '上地八街', '7号', '1号楼', '北京市海淀区创业路2号1号楼', '116.313348,40.053734', '张三001', '13681241851', '1', '2019-05-15 08:58:48', '0', '12345.45', '154535445.54', '120.45', '120454.45', '12', '110454.45', '2', '10000.00', '商用及景点', '高楼', '含绿地及附属设施', '地上3楼', '2', '低级', '400', '2', '最左边', '3', '3000.00', '最左边', '普通用户', '0', '消防栓', '消防带', '1', '毗邻A座', '北京市海淀区创业路2号1号楼', '0', '商用及景点', '1', '0', 'admin', '1', '2019-05-15 08:58:48', 'admin', '1', '2020-05-15 08:58:48', '1111', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_building` VALUES ('7', '111', '111', '东方电子科技大厦', '北京市海淀区创业路2号1号楼', '100029', '125', '上地八街', '7号', '1号楼', '北京市海淀区创业路2号1号楼', '116.313348,40.053734', '张三001', '13681241851', '1', '2019-05-15 08:58:48', '0', '12345.45', '154535445.54', '120.45', '120454.45', '12', '110454.45', '2', '10000.00', '商用及景点', '高楼', '含绿地及附属设施', '地上3楼', '2', '低级', '400', '2', '最左边', '3', '3000.00', '最左边', '普通用户', '0', '消防栓', '消防带', '1', '毗邻A座', '北京市海淀区创业路2号1号楼', '0', '商用及景点', '1', '0', 'admin', '1', '2019-05-15 08:58:48', 'admin', '1', '2020-05-15 08:58:48', '1111', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_building` VALUES ('8', '111', '111', '东方电子科技大厦', '北京市海淀区创业路2号1号楼', '100029', '125', '上地八街', '7号', '1号楼', '北京市海淀区创业路2号1号楼', '116.313348,40.053734', '张三001', '13681241851', '1', '2019-05-15 08:58:48', '0', '12345.45', '154535445.54', '120.45', '120454.45', '12', '110454.45', '2', '10000.00', '商用及景点', '高楼', '含绿地及附属设施', '地上3楼', '2', '低级', '400', '2', '最左边', '3', '3000.00', '最左边', '普通用户', '0', '消防栓', '消防带', '1', '毗邻A座', '北京市海淀区创业路2号1号楼', '0', '商用及景点', '1', '0', 'admin', '1', '2019-05-15 08:58:48', 'admin', '1', '2020-05-15 08:58:48', '1111', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_building` VALUES ('9', '111', '111', '东方电子科技大厦', '北京市海淀区创业路2号1号楼', '100029', '125', '上地八街', '7号', '1号楼', '北京市海淀区创业路2号1号楼', '116.313348,40.053734', '张三001', '13681241851', '1', '2019-05-15 08:58:48', '0', '12345.45', '154535445.54', '120.45', '120454.45', '12', '110454.45', '2', '10000.00', '商用及景点', '高楼', '含绿地及附属设施', '地上3楼', '2', '低级', '400', '2', '最左边', '3', '3000.00', '最左边', '普通用户', '0', '消防栓', '消防带', '1', '毗邻A座', '北京市海淀区创业路2号1号楼', '0', '商用及景点', '1', '0', 'admin', '1', '2019-05-15 08:58:48', 'admin', '1', '2020-05-15 08:58:48', '1111', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_building` VALUES ('111', '111', '111', '东方电子科技大厦', '北京市海淀区创业路2号1号楼', '100029', '125', '上地八街', '7号', '1号楼', '北京市海淀区创业路2号1号楼', '116.313348,40.053734', '张三001', '13681241851', '1', '2019-05-15 08:58:48', '0', '12345.45', '154535445.54', '120.45', '120454.45', '12', '110454.45', '2', '10000.00', '商用及景点', '高楼', '含绿地及附属设施', '地上3楼', '2', '低级', '400', '2', '最左边', '3', '3000.00', '最左边', '普通用户', '0', '消防栓', '消防带', '1', '毗邻A座', '北京市海淀区创业路2号1号楼', '0', '商用及景点', '1', '0', 'admin', '1', '2019-05-15 08:58:48', 'admin', '1', '2020-05-15 08:58:48', '1111', 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for device_building_image
-- ----------------------------
DROP TABLE IF EXISTS `device_building_image`;
CREATE TABLE `device_building_image` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `BUILDING_NAME` varchar(255) DEFAULT NULL,
  `BUILDING_ID` int DEFAULT NULL,
  `BUILDING_IMAGE_X` double DEFAULT NULL,
  `BUILDING_IMAGE_Y` double DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='赤松观大屏-建筑物与在平面图的坐标关系表';

-- ----------------------------
-- Records of device_building_image
-- ----------------------------

-- ----------------------------
-- Table structure for device_check_test_item
-- ----------------------------
DROP TABLE IF EXISTS `device_check_test_item`;
CREATE TABLE `device_check_test_item` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `FACILITIES_TYPE_ID` int DEFAULT NULL COMMENT '设施类型id',
  `ITEM_FLAG` char(1) DEFAULT NULL COMMENT '检查项或检测项[0=检查项/1=检测项]',
  `CHECK_TEST_ITEM` varchar(255) DEFAULT NULL COMMENT '检查检测项',
  `FLAG` char(1) DEFAULT NULL COMMENT '单选或输入[0=单选/1=输入]',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=786 DEFAULT CHARSET=utf8mb3 COMMENT='检查和检测项表';

-- ----------------------------
-- Records of device_check_test_item
-- ----------------------------
INSERT INTO `device_check_test_item` VALUES ('1', '1', '0', '室内温度', '0', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_check_test_item` VALUES ('2', '2', '0', '室内温度', '0', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_check_test_item` VALUES ('3', '3', '0', '室内温度', '0', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_check_test_item` VALUES ('4', '4', '0', '室内温度', '0', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_check_test_item` VALUES ('5', '5', '0', '室内温度', '0', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_check_test_item` VALUES ('6', '1', '0', '室内温度', '0', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_check_test_item` VALUES ('778', '1', '0', '室内温度', '0', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_check_test_item` VALUES ('779', '1', '0', '室内湿度', '0', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_check_test_item` VALUES ('780', '1', '0', '16~28', '1', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_check_test_item` VALUES ('781', '1', '0', '45%~55%', '1', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for device_collecting_device
-- ----------------------------
DROP TABLE IF EXISTS `device_collecting_device`;
CREATE TABLE `device_collecting_device` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `COLLECTING_DEVIC_TYPE_ID` int DEFAULT NULL COMMENT '采集设备类型id',
  `STATUS` char(1) DEFAULT NULL COMMENT '状态[0=故障/1=正常]（需要产品确认值）',
  `NO` varchar(255) DEFAULT NULL COMMENT '编号',
  `NETWORK_FORM` char(1) DEFAULT NULL COMMENT '网络形式 [](需要产品确认值)',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `GEOGRAPHICAL_POSITION_SIGN` varchar(255) DEFAULT NULL COMMENT '地理位置',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_collecting_device
-- ----------------------------
INSERT INTO `device_collecting_device` VALUES ('1', '1', '1', '3644368456807', null, '555', '114.414917,30.495455', '1', '特斯联', 'abjYOQII', '2019-07-06 14:41:30', '特斯联', 'abjYOQII', '2019-07-06 14:55:03', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'otYBMuRj');
INSERT INTO `device_collecting_device` VALUES ('2', '3', '1', '2353', null, '345', '114.43558,30.44917', '1', '特斯联', 'abjYOQII', '2019-07-06 14:53:11', '特斯联', 'abjYOQII', '2019-07-06 14:55:01', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'otYBMuRj');
INSERT INTO `device_collecting_device` VALUES ('3', '4', '1', 'TSL123456', null, 'DESC', '114.32345,30.57391', '1', '特斯联', 'abjYOQII', '2019-07-19 17:01:18', '特斯联', 'abjYOQII', '2019-08-28 15:02:21', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'otYBMuRj');
INSERT INTO `device_collecting_device` VALUES ('4', '4', '1', '214235', null, '2345346346', '114.414917,30.495455', '1', '特斯联', 'abjYOQII', '2019-10-16 15:00:17', '特斯联', 'abjYOQII', '2019-10-16 15:00:22', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'otYBMuRj');
INSERT INTO `device_collecting_device` VALUES ('5', '4', '1', 'saa', null, '111', '114.45275,30.44236', '0', '郭放军', 'uxii5a7S', '2019-10-18 18:03:43', '郭放军', 'uxii5a7S', '2019-10-18 18:03:43', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'otYBMuRj');

-- ----------------------------
-- Table structure for device_collecting_device_series
-- ----------------------------
DROP TABLE IF EXISTS `device_collecting_device_series`;
CREATE TABLE `device_collecting_device_series` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '类型编号为了统一显示时采用网关设备类型ID',
  `TYPE` varchar(100) DEFAULT NULL COMMENT '采集设备类型',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `COLLECTING_TYPE_ID` int DEFAULT NULL COMMENT '网关设备类型ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_collecting_device_series
-- ----------------------------

-- ----------------------------
-- Table structure for device_collecting_device_type
-- ----------------------------
DROP TABLE IF EXISTS `device_collecting_device_type`;
CREATE TABLE `device_collecting_device_type` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `EQUIPMENT_TYPE` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `MANUFACTURER` varchar(255) DEFAULT NULL COMMENT '厂商',
  `MODEL` varchar(255) DEFAULT NULL COMMENT '型号',
  `MAINTENANCE_CYCLE_UNIT` char(1) DEFAULT NULL COMMENT '维保周期单位[0=天/1=月/2=年](单位暂定，待产品规定)',
  `MAINTENANCE_CYCLE_VALUE` int DEFAULT NULL COMMENT '维保周期数值',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `MANUFACTURER_ID` int DEFAULT NULL COMMENT '厂商ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_collecting_device_type
-- ----------------------------

-- ----------------------------
-- Table structure for device_collecting_manufacturer
-- ----------------------------
DROP TABLE IF EXISTS `device_collecting_manufacturer`;
CREATE TABLE `device_collecting_manufacturer` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `COLLECTING_MANUFACTURER` varchar(255) DEFAULT NULL COMMENT '网关厂商',
  `CODE_NAME` varchar(255) DEFAULT NULL COMMENT '网关厂商代号',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_collecting_manufacturer
-- ----------------------------
INSERT INTO `device_collecting_manufacturer` VALUES ('1', '南京肯麦思', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c24');
INSERT INTO `device_collecting_manufacturer` VALUES ('2', '北京用友薪福社云科技有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c25');
INSERT INTO `device_collecting_manufacturer` VALUES ('3', '青岛尚美生活集团有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c26');
INSERT INTO `device_collecting_manufacturer` VALUES ('4', '栗田工业（大连）有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c27');
INSERT INTO `device_collecting_manufacturer` VALUES ('5', '青岛恒业智能科技有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c28');
INSERT INTO `device_collecting_manufacturer` VALUES ('6', '东莞市逸鹏智能家居有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c29');
INSERT INTO `device_collecting_manufacturer` VALUES ('7', '昆明讯能信息技术有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c30');
INSERT INTO `device_collecting_manufacturer` VALUES ('8', '北京赑风科技有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c31');
INSERT INTO `device_collecting_manufacturer` VALUES ('9', '上海思尔达科学仪器有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c32');
INSERT INTO `device_collecting_manufacturer` VALUES ('10', '湖北云尚席科技信息有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c33');
INSERT INTO `device_collecting_manufacturer` VALUES ('11', '上海本腾汽车销售服务有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c34');
INSERT INTO `device_collecting_manufacturer` VALUES ('12', '河南祥和运通国际货运代理有限公司', '', '0', 'admin', '1', '2021-06-10 10:15:13', 'admin', '1', '2021-06-10 10:15:13', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'a4231b09bf472cb937c35');

-- ----------------------------
-- Table structure for device_coordinate
-- ----------------------------
DROP TABLE IF EXISTS `device_coordinate`;
CREATE TABLE `device_coordinate` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `device_id` int DEFAULT NULL COMMENT '设备ID',
  `channel_id` int DEFAULT NULL COMMENT '所属系统',
  `coordinate` varchar(255) DEFAULT NULL COMMENT '设备在3D模型中的3D坐标',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='设备在3D模型中的坐标信息表';

-- ----------------------------
-- Records of device_coordinate
-- ----------------------------

-- ----------------------------
-- Table structure for device_data_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `device_data_dictionary`;
CREATE TABLE `device_data_dictionary` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `PARM_TYPE` varchar(20) DEFAULT NULL COMMENT '参数类别',
  `PARM_NAME` varchar(20) DEFAULT NULL COMMENT '参数说明',
  `PARM_VALUE` varchar(120) DEFAULT NULL COMMENT '参数代号',
  `PARENT_TYPE` varchar(20) DEFAULT NULL COMMENT '父级参数类别',
  `PARENT_VALUE` varchar(120) DEFAULT NULL COMMENT '父级参数代号',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_data_dictionary
-- ----------------------------

-- ----------------------------
-- Table structure for device_early_warning
-- ----------------------------
DROP TABLE IF EXISTS `device_early_warning`;
CREATE TABLE `device_early_warning` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `BUILD_ID` int DEFAULT NULL COMMENT '建筑列表id',
  `EQU_ID` int DEFAULT NULL COMMENT '设备id',
  `ALRM_TYPE` char(100) DEFAULT NULL COMMENT '报警类型(预警等级)',
  `ALRM_DATE` datetime DEFAULT NULL COMMENT '报警时间',
  `HANDLE_FLAG` char(1) DEFAULT NULL COMMENT '是否处理[1=是/0=否]',
  `HANDLE_DATE` datetime DEFAULT NULL COMMENT '处理时间',
  `HANDLE_PERSON` varchar(100) DEFAULT NULL COMMENT '处理人',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `ALRM_DATA` double(10,3) DEFAULT NULL COMMENT '传感器数据',
  `LOG_ID` char(64) DEFAULT NULL COMMENT '日志ID',
  `UNIT` varchar(100) DEFAULT NULL COMMENT '测点代号',
  `B_NAME` varchar(200) DEFAULT NULL COMMENT '建筑名称',
  `SENSOR_NO` varchar(50) DEFAULT NULL COMMENT '传感器编号',
  `EQUIPMENT_TYPE` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `FLOOR` int DEFAULT NULL COMMENT '楼层',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `MEASURING_POINT` varchar(100) DEFAULT NULL COMMENT '测点',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统id',
  PRIMARY KEY (`ID`),
  KEY `device_abnormal_alrm_date` (`ALRM_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_early_warning
-- ----------------------------

-- ----------------------------
-- Table structure for device_equipment_specification
-- ----------------------------
DROP TABLE IF EXISTS `device_equipment_specification`;
CREATE TABLE `device_equipment_specification` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(128) NOT NULL COMMENT '名称',
  `abbreviation` varchar(128) NOT NULL COMMENT '简称 默认名称首字母大写缩写',
  `add_type` char(1) NOT NULL COMMENT '添加方式：1系统管理员，2站点管理员',
  `crt_user_name` varchar(128) DEFAULT NULL COMMENT '创建人',
  `crt_user_id` varchar(36) DEFAULT NULL COMMENT '创建人ID',
  `crt_time` date DEFAULT NULL COMMENT '创建时间',
  `upd_user_name` varchar(128) DEFAULT NULL COMMENT '最后更新人',
  `upd_user_id` varchar(36) DEFAULT NULL COMMENT '最后更新人ID',
  `upd_time` date DEFAULT NULL COMMENT '最后更新时间',
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='设备规格 ';

-- ----------------------------
-- Records of device_equipment_specification
-- ----------------------------

-- ----------------------------
-- Table structure for device_facilities_abnormal
-- ----------------------------
DROP TABLE IF EXISTS `device_facilities_abnormal`;
CREATE TABLE `device_facilities_abnormal` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `FIRE_COCK_ID` int DEFAULT NULL COMMENT '消火栓id',
  `EQU_ID` int DEFAULT NULL COMMENT '设备id',
  `ALRM_TYPE` char(100) DEFAULT NULL COMMENT '报警类型',
  `ALRM_DATE` datetime DEFAULT NULL COMMENT '报警时间',
  `HANDLE_FLAG` char(1) DEFAULT NULL COMMENT '是否处理[1=是/0=否]',
  `FAULT_FLAG` char(1) DEFAULT NULL COMMENT '是否故障[1=误报/0=故障]',
  `CONFIR_DATE` datetime DEFAULT NULL COMMENT '确认时间',
  `HANDLE_DATE` datetime DEFAULT NULL COMMENT '处理时间',
  `CONFIR_PERSON` varchar(100) DEFAULT NULL COMMENT '确认人',
  `HANDLE_PERSON` varchar(100) DEFAULT NULL COMMENT '处理人',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `ALRM_DATA` double(10,3) DEFAULT NULL COMMENT '传感器数据',
  `LOG_ID` char(64) DEFAULT NULL COMMENT '日志ID',
  `UNIT` varchar(100) DEFAULT NULL COMMENT '测点代号',
  `DATA_UNIT` varchar(100) DEFAULT NULL COMMENT '数据单位',
  `HYDRANT_NAME` varchar(200) DEFAULT NULL COMMENT '消火栓名称',
  `SENSOR_NO` varchar(50) DEFAULT NULL COMMENT '传感器编号',
  `EQUIPMENT_TYPE` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `MEASURING_POINT` varchar(100) DEFAULT NULL COMMENT '测点',
  `LEVEL` varchar(20) DEFAULT NULL COMMENT '报警等级',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统id',
  PRIMARY KEY (`ID`),
  KEY `device_abnormal_alrm_date` (`ALRM_DATE`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='硬件设施异常记录表';

-- ----------------------------
-- Records of device_facilities_abnormal
-- ----------------------------
INSERT INTO `device_facilities_abnormal` VALUES ('1', '111', '111', '0', '2021-05-13 14:48:47', '1', '1', '2021-05-13 18:48:47', '2021-05-13 19:48:47', 'admin', 'admin', '0', 'admin', '1', '2021-04-13 19:48:47', 'admin', '1', '2021-04-14 19:48:47', null, 'ac88ceb386aa4231b09bf472cb937c24', null, null, '5', null, null, null, null, '展厅111', null, null, '1');
INSERT INTO `device_facilities_abnormal` VALUES ('2', '112', '112', '0', '2021-05-13 19:16:51', '1', '1', '2021-05-13 19:16:51', '2021-05-13 19:16:51', 'admin', 'admin', '0', 'admin', '1', '2021-04-13 19:48:47', 'admin', '1', '2021-04-14 19:48:47', null, 'ac88ceb386aa4231b09bf472cb937c24', null, null, '5', null, null, null, null, '展厅112', null, null, '1');

-- ----------------------------
-- Table structure for device_facilities_type
-- ----------------------------
DROP TABLE IF EXISTS `device_facilities_type`;
CREATE TABLE `device_facilities_type` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `EQUIPMENT_TYPE` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `CHECK_ITEM_COUNT` varchar(255) DEFAULT NULL COMMENT '检查项数',
  `TEST_ITEM_COUNT` varchar(255) DEFAULT NULL COMMENT '检测项数',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COMMENT='设施类型表';

-- ----------------------------
-- Records of device_facilities_type
-- ----------------------------
INSERT INTO `device_facilities_type` VALUES ('1', '监控点NB24544545', '4', '1', '0', 'admin', '1', '2021-06-10 08:41:52', 'admin', '1', '2021-06-10 08:41:52', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_facilities_type` VALUES ('2', '测点CD44544545', '2', '2', '0', 'admin', '1', '2021-06-10 08:48:35', 'admin', '1', '2021-06-10 08:48:35', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_facilities_type` VALUES ('3', '监控点NB24544546', '4', '4', '0', 'admin', '1', '2021-06-10 08:41:53', 'admin', '1', '2021-06-10 08:41:53', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_facilities_type` VALUES ('4', '测点CD44544546', '2', '6', '0', 'admin', '1', '2021-06-10 08:48:36', 'admin', '1', '2021-06-10 08:48:36', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_facilities_type` VALUES ('5', '监控点NB24544547', '4', '8', '0', 'admin', '1', '2021-06-10 08:41:54', 'admin', '1', '2021-06-10 08:41:54', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_facilities_type` VALUES ('6', '测点CD44544547', '6', '10', '0', 'admin', '1', '2021-06-10 08:48:37', 'admin', '1', '2021-06-10 08:48:37', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_facilities_type` VALUES ('7', '监控点NB24544548', '8', '12', '0', 'admin', '1', '2021-06-10 08:41:55', 'admin', '1', '2021-06-10 08:41:55', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_facilities_type` VALUES ('8', '测点CD44544548', '10', '14', '0', 'admin', '1', '2021-06-10 08:48:38', 'admin', '1', '2021-06-10 08:48:38', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_facilities_type` VALUES ('9', '监控点NB24544549', '12', '16', '0', 'admin', '1', '2021-06-10 08:41:56', 'admin', '1', '2021-06-10 08:41:56', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for device_facilities_type_item
-- ----------------------------
DROP TABLE IF EXISTS `device_facilities_type_item`;
CREATE TABLE `device_facilities_type_item` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `facilities_type_id` int NOT NULL COMMENT '设施类型ID',
  `item_type` char(1) NOT NULL COMMENT '关联项类型 1规格，2设备类型',
  `item_id` varchar(36) NOT NULL COMMENT '关联项ID',
  `is_enabled` char(1) DEFAULT '1' COMMENT '是否启用 0否，1是',
  `add_type` char(1) NOT NULL COMMENT '添加方式 1系统管理员添加，2站点管理员添加',
  `crt_user_name` varchar(128) DEFAULT NULL COMMENT '创建人',
  `crt_user_id` varchar(36) DEFAULT NULL COMMENT '创建人ID',
  `crt_time` date DEFAULT NULL COMMENT '创建时间',
  `upd_user_name` varchar(128) DEFAULT NULL COMMENT '最后更新人',
  `upd_user_id` varchar(36) DEFAULT NULL COMMENT '最后更新人ID',
  `upd_time` date DEFAULT NULL COMMENT '最后更新时间',
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='设施类型-关联项关系表 ';

-- ----------------------------
-- Records of device_facilities_type_item
-- ----------------------------

-- ----------------------------
-- Table structure for device_factory_cmd
-- ----------------------------
DROP TABLE IF EXISTS `device_factory_cmd`;
CREATE TABLE `device_factory_cmd` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `SENSOR_TYPE` int NOT NULL COMMENT '设备类型',
  `CMD` varchar(50) NOT NULL COMMENT 'C通道命令',
  `SENSOR_NAME` varchar(50) NOT NULL COMMENT '传感器的设备名称',
  `CREATE_TIME` datetime(6) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='工厂设备C通道配置表';

-- ----------------------------
-- Records of device_factory_cmd
-- ----------------------------

-- ----------------------------
-- Table structure for device_factory_data
-- ----------------------------
DROP TABLE IF EXISTS `device_factory_data`;
CREATE TABLE `device_factory_data` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `DEVICE_ID` varchar(30) NOT NULL COMMENT '传感器编号',
  `CHANNEL_NO` varchar(16) NOT NULL,
  `SWITCH` int NOT NULL COMMENT '开关(0或1) ',
  `SENSOR_TYPE` int NOT NULL COMMENT '设备类型',
  `CMD` varchar(100) NOT NULL COMMENT '下发命令',
  `ACQUISITION_CYCLE` int NOT NULL COMMENT '采集周期',
  `USER_DEFINED` varchar(100) DEFAULT NULL COMMENT '自定义数据',
  `UPLOAD_TIME` datetime(6) DEFAULT NULL,
  `FACTORY_ID` int NOT NULL COMMENT '工厂id',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='工厂设备信息配置表';

-- ----------------------------
-- Records of device_factory_data
-- ----------------------------

-- ----------------------------
-- Table structure for device_factory_sensor
-- ----------------------------
DROP TABLE IF EXISTS `device_factory_sensor`;
CREATE TABLE `device_factory_sensor` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `DEVICE_ID` varchar(30) NOT NULL COMMENT '设备id',
  `CHANNELA_ID` varchar(16) NOT NULL COMMENT '通道A',
  `CHANNELB_ID` varchar(16) NOT NULL,
  `CHANNELC_ID` varchar(16) NOT NULL,
  `CHANNELD_ID` varchar(16) NOT NULL,
  `CHANNELE_ID` varchar(16) NOT NULL,
  `UPLOAD_TIME` datetime(6) DEFAULT NULL,
  `FACTORY_ID` int NOT NULL COMMENT '工厂id',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UQ_DEVICE_ID` (`DEVICE_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='工厂设备对应传感器编号表';

-- ----------------------------
-- Records of device_factory_sensor
-- ----------------------------

-- ----------------------------
-- Table structure for device_fire_door
-- ----------------------------
DROP TABLE IF EXISTS `device_fire_door`;
CREATE TABLE `device_fire_door` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `BUILDING_ID` int NOT NULL COMMENT '所属建筑物ID',
  `DOOR_NAME` varchar(100) NOT NULL COMMENT '防火门名称',
  `DOOR_NORMAL_STATUS` char(1) NOT NULL COMMENT '防火门类型：１常开门２常闭门',
  `FLOOR` int NOT NULL COMMENT '所属楼层',
  `POSITION_DESCRIPTION` varchar(100) NOT NULL COMMENT '防火门位置描述',
  `POSITION_SIGN` varchar(50) DEFAULT NULL COMMENT '在楼层平面图中的坐标，以逗号分隔',
  `SENSOR_NUM` int NOT NULL DEFAULT '0' COMMENT '传感器数量',
  `DOOR_STATUS` varchar(200) DEFAULT NULL COMMENT 'JSON:[{''deviceId'':1,''status'':1},{''deviceId'':1,''status'':1}]',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除标识1表示已删除 0表示正常',
  `CRT_USER_NAME` varchar(100) NOT NULL COMMENT '创建人名称',
  `CRT_USER_ID` varchar(32) NOT NULL COMMENT '创建人用户ID ',
  `CRT_TIME` datetime NOT NULL COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '更新人姓名',
  `UPD_USER_ID` varchar(32) DEFAULT NULL COMMENT '更新人用户ID',
  `UPD_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `DEPART_ID` varchar(32) DEFAULT NULL COMMENT '保留字段',
  `TENANT_ID` varchar(32) NOT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='防火门表';

-- ----------------------------
-- Records of device_fire_door
-- ----------------------------

-- ----------------------------
-- Table structure for device_fire_main
-- ----------------------------
DROP TABLE IF EXISTS `device_fire_main`;
CREATE TABLE `device_fire_main` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `SERVER_IP` varchar(200) DEFAULT NULL COMMENT '服务器IP',
  `PORT` varchar(100) DEFAULT NULL COMMENT '端口号',
  `GIS` varchar(100) DEFAULT NULL COMMENT '地理坐标经度的十进制表达式',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='消防主机表';

-- ----------------------------
-- Records of device_fire_main
-- ----------------------------

-- ----------------------------
-- Table structure for device_fire_main_abnormal
-- ----------------------------
DROP TABLE IF EXISTS `device_fire_main_abnormal`;
CREATE TABLE `device_fire_main_abnormal` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `BUILD_ID` int DEFAULT NULL COMMENT '建筑列表id',
  `FIRE_MAIN_ID` int DEFAULT NULL COMMENT '消防主机id',
  `EQU_ID` int DEFAULT NULL COMMENT '设备id',
  `SERIES` varchar(255) DEFAULT NULL COMMENT '系列',
  `SENSOR_LOOP` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL COMMENT '地址',
  `ALRM_CATEGORY` char(100) DEFAULT NULL COMMENT '报警类型：0：故障，1：火警',
  `ALRM_TYPE` char(100) DEFAULT NULL COMMENT '报警类型',
  `ALRM_DATE` datetime DEFAULT NULL COMMENT '报警时间',
  `HANDLE_FLAG` char(1) DEFAULT NULL COMMENT '是否处理[1=是/0=否]',
  `FIRE_FLAG` char(1) DEFAULT NULL COMMENT '是否真实火警[2=火警测试/1=是/0=否]',
  `HANDLE_DATE` datetime DEFAULT NULL COMMENT '处理时间',
  `HANDLE_PERSON` varchar(100) DEFAULT NULL COMMENT '处理人',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `ALRM_DATA` double(10,3) DEFAULT NULL COMMENT '传感器数据',
  `LOG_ID` char(64) DEFAULT NULL COMMENT '日志ID',
  `B_NAME` varchar(200) DEFAULT NULL COMMENT '建筑名称',
  `FLOOR` int DEFAULT NULL COMMENT '楼层',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统id',
  PRIMARY KEY (`ID`),
  KEY `device_abnormal_alrm_date` (`ALRM_DATE`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='消防主机异常表';

-- ----------------------------
-- Records of device_fire_main_abnormal
-- ----------------------------
INSERT INTO `device_fire_main_abnormal` VALUES ('1', '111', '111', '111', '系列A001', '100', ' 地址111', '1', '1', '2021-05-13 22:06:54', '1', '1', '2021-05-13 22:06:54', 'admin', '0', 'admin', '1', '2021-05-13 22:06:54', 'admin', '1', '2021-05-13 22:06:54', null, 'ac88ceb386aa4231b09bf472cb937c24', null, null, '建筑名称111', '3', '展厅111', '1');
INSERT INTO `device_fire_main_abnormal` VALUES ('2', '112', '112', '112', '系列A112', '100', ' 地址112', '1', '1', '2021-05-13 22:15:37', '0', '1', '2021-05-13 22:15:37', 'admin', '0', 'admin', '1', '2021-05-13 22:15:37', 'admin', '1', '2021-05-13 22:15:37', null, 'ac88ceb386aa4231b09bf472cb937c24', null, null, '建筑名称112', '3', '展厅112', '1');

-- ----------------------------
-- Table structure for device_fire_main_sensor
-- ----------------------------
DROP TABLE IF EXISTS `device_fire_main_sensor`;
CREATE TABLE `device_fire_main_sensor` (
  `ID` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `BUILDING_ID` int DEFAULT NULL COMMENT '建筑列表id',
  `FIRE_MAIN_ID` int DEFAULT NULL COMMENT '消防主机id',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统id',
  `SERIES` varchar(255) DEFAULT NULL COMMENT '系列',
  `SENSOR_LOOP` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL COMMENT '地址',
  `STATUS` char(1) DEFAULT NULL COMMENT '状态[0=故障/1=报警/2=正常/3=未启用/4=离线]（需要产品确认）',
  `FLOOR` int DEFAULT NULL COMMENT '楼层',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `POSITION_SIGN` varchar(50) DEFAULT NULL COMMENT '传感器在平面图的位置标记（中间用,号隔开）',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `STATUS_TIME` datetime DEFAULT NULL COMMENT '数据上传时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='消防主机传感器表';

-- ----------------------------
-- Records of device_fire_main_sensor
-- ----------------------------
INSERT INTO `device_fire_main_sensor` VALUES ('1', '1', '111', '1', 'A001', '往复循环', '5号楼', '2', '3', '展厅31', null, '0', 'admin', '1', null, 'admin', '1', null, null, 'ac88ceb386aa4231b09bf472cb937c24', null);

-- ----------------------------
-- Table structure for device_floor_layout
-- ----------------------------
DROP TABLE IF EXISTS `device_floor_layout`;
CREATE TABLE `device_floor_layout` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `BUILD_ID` int DEFAULT NULL COMMENT '建筑ID',
  `FILE_FLOOR` int DEFAULT NULL COMMENT '附件所属楼层',
  `FILE_NAME` varchar(50) DEFAULT NULL COMMENT '文件名',
  `FILE_TYPE` varchar(10) DEFAULT NULL COMMENT '文件类型/后缀名',
  `FILE_PATH` varchar(200) DEFAULT NULL COMMENT '文件路径',
  `MEMO` varchar(100) DEFAULT NULL COMMENT '说明',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '文件删除标记（0未删除，1已删除）',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_floor_layout
-- ----------------------------

-- ----------------------------
-- Table structure for device_gy_user
-- ----------------------------
DROP TABLE IF EXISTS `device_gy_user`;
CREATE TABLE `device_gy_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` varchar(50) NOT NULL COMMENT '用户id',
  `user` varchar(50) NOT NULL COMMENT '客户名',
  `sensorId` varchar(20) NOT NULL COMMENT '设备编号',
  `user_api` varchar(50) NOT NULL COMMENT '用户提供的api',
  `sensorTpye` varchar(30) NOT NULL COMMENT '设备类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_gy_user
-- ----------------------------

-- ----------------------------
-- Table structure for device_hardware_facilities
-- ----------------------------
DROP TABLE IF EXISTS `device_hardware_facilities`;
CREATE TABLE `device_hardware_facilities` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `OID` int DEFAULT NULL COMMENT '联网单位单位ID',
  `HYDRANT_NAME` varchar(255) DEFAULT NULL COMMENT '消火栓名称',
  `AREA` varchar(255) DEFAULT NULL COMMENT '所属区域',
  `ZXQY` char(6) DEFAULT NULL COMMENT '6位行政区编码（应符合现行国家标准《中华人民共和国行政区划代码》GB2260的规定）',
  `FACILITY_TYPE` char(1) DEFAULT NULL COMMENT '设施类型[0=室外消火栓]',
  `HYDRANT_TYPE` char(1) DEFAULT NULL COMMENT '消火栓类型[0=地下式/1=地上式/2=直埋伸缩]',
  `OUTLET` char(1) DEFAULT NULL COMMENT '出水口[0=单口式/1=双口式/2=三出水口式]',
  `OUTLET_TYPE_ONE` char(1) DEFAULT NULL COMMENT '第一出水口类型[0=外螺旋式/1=内扣式]',
  `OUTLET_VALUE_ONE` int DEFAULT NULL COMMENT '第一出水口数值',
  `OUTLET_TYPE_TWO` char(1) DEFAULT NULL COMMENT '第二出水口类型[0=外螺旋式/1=内扣式]',
  `OUTLET_VALUE_TWO` int DEFAULT NULL COMMENT '第二出水口数值',
  `OUTLET_TYPE_THREE` char(1) DEFAULT NULL COMMENT '第三出水口类型[0=外螺旋式/1=内扣式]',
  `OUTLET_VALUE_THREE` int DEFAULT NULL COMMENT '第三出水口数值',
  `PROTECTION_RADIUS` int DEFAULT NULL COMMENT '保护半径',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `WATER_PIPE` char(1) DEFAULT NULL COMMENT '水管道[0=高压给水/1=临时高压给水/2=低压给水]',
  `GIS` varchar(255) DEFAULT NULL COMMENT '地理坐标经度的十进制表达式',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb3 COMMENT='硬件设施表';

-- ----------------------------
-- Records of device_hardware_facilities
-- ----------------------------
INSERT INTO `device_hardware_facilities` VALUES ('1', '111', '消火栓111', 'A区', '100029', '0', '1', '0', '2', '4', '2', '4', '2', '4', '150', '展厅', '0', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_hardware_facilities` VALUES ('2', '111', '消火栓112', 'A区', '100030', '0', '1', '0', '2', '4', '2', '4', '2', '4', '150', '展厅', '0', '116.312137,40.052837', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c25');
INSERT INTO `device_hardware_facilities` VALUES ('3', '111', '消火栓113', 'A区', '100031', '0', '1', '0', '2', '4', '2', '4', '2', '4', '150', '展厅', '0', '116.310298,40.052059', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c26');
INSERT INTO `device_hardware_facilities` VALUES ('4', '111', '消火栓114', 'A区', '100032', '0', '1', '0', '2', '4', '2', '4', '2', '4', '150', '展厅', '0', '116.312126,40.054485', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c27');
INSERT INTO `device_hardware_facilities` VALUES ('5', '111', '消火栓115', 'A区', '100033', '0', '1', '0', '2', '4', '2', '4', '2', '4', '150', '展厅', '0', '116.315957,40.058251', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c28');
INSERT INTO `device_hardware_facilities` VALUES ('6', '111', '消火栓116', 'A区', '100034', '0', '1', '1', '2', '4', '2', '4', '2', '4', '150', '展厅', '1', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c29');
INSERT INTO `device_hardware_facilities` VALUES ('7', '111', '消火栓117', 'A区', '100035', '0', '1', '1', '2', '4', '2', '4', '2', '4', '150', '展厅', '1', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c30');
INSERT INTO `device_hardware_facilities` VALUES ('8', '111', '消火栓118', 'A区', '100036', '0', '1', '1', '2', '4', '2', '4', '2', '4', '150', '展厅', '1', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c31');
INSERT INTO `device_hardware_facilities` VALUES ('9', '111', '消火栓119', 'A区', '100037', '0', '1', '1', '2', '4', '2', '4', '2', '4', '150', '展厅', '1', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c32');
INSERT INTO `device_hardware_facilities` VALUES ('10', '111', '消火栓120', 'A区', '100038', '0', '1', '1', '2', '4', '2', '4', '2', '4', '150', '展厅', '1', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c33');
INSERT INTO `device_hardware_facilities` VALUES ('11', '111', '消火栓121', 'A区', '100039', '0', '1', '1', '2', '4', '2', '4', '2', '4', '150', '展厅', '1', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c34');
INSERT INTO `device_hardware_facilities` VALUES ('12', '111', '消火栓122', 'A区', '100040', '0', '1', '1', '2', '4', '2', '4', '2', '4', '150', '展厅', '1', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c35');
INSERT INTO `device_hardware_facilities` VALUES ('13', '111', '消火栓123', 'A区', '100041', '0', '1', '2', '2', '4', '2', '4', '2', '4', '150', '展厅', '1', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c36');
INSERT INTO `device_hardware_facilities` VALUES ('14', '111', '消火栓124', 'A区', '100042', '0', '1', '2', '2', '4', '2', '4', '2', '4', '150', '展厅', '2', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c37');
INSERT INTO `device_hardware_facilities` VALUES ('15', '111', '消火栓125', 'A区', '100043', '0', '1', '2', '2', '4', '2', '4', '2', '4', '150', '展厅', '2', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c38');
INSERT INTO `device_hardware_facilities` VALUES ('16', '111', '消火栓126', 'A区', '100044', '0', '1', '2', '2', '4', '2', '4', '2', '4', '150', '展厅', '2', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c39');
INSERT INTO `device_hardware_facilities` VALUES ('17', '111', '消火栓127', 'A区', '100045', '0', '1', '2', '2', '4', '2', '4', '2', '4', '150', '展厅', '2', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c40');
INSERT INTO `device_hardware_facilities` VALUES ('18', '111', '消火栓128', 'A区', '100046', '0', '1', '2', '2', '4', '2', '4', '2', '4', '150', '展厅', '2', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c41');
INSERT INTO `device_hardware_facilities` VALUES ('19', '111', '消火栓129', 'A区', '100047', '0', '1', '2', '2', '4', '2', '4', '2', '4', '150', '展厅', '2', '116.313348,40.053734', '0', 'admin', '1', '2021-06-02 11:20:30', 'admin', '1', '2021-06-02 11:20:30', null, 'ac88ceb386aa4231b09bf472cb937c42');

-- ----------------------------
-- Table structure for device_indoor_label
-- ----------------------------
DROP TABLE IF EXISTS `device_indoor_label`;
CREATE TABLE `device_indoor_label` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `FACILITIES_TYPE_ID` int DEFAULT NULL COMMENT '设施类型id',
  `BUILDING_ID` int DEFAULT NULL COMMENT '建筑id',
  `FACILITIES_NO` varchar(200) DEFAULT NULL COMMENT '设施编号',
  `STATUS` char(1) DEFAULT NULL COMMENT '状态[0=正常/1=维修中]',
  `FLOOR` int DEFAULT NULL COMMENT '楼层',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `LAST_INSPECTION_TIME` datetime DEFAULT NULL COMMENT '最近巡检时间',
  `QR_CODE_PATH` varchar(200) DEFAULT NULL COMMENT '二维码路径',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `USE_FLAG` char(1) DEFAULT NULL COMMENT '是否生成路线[1=是/0=否]',
  `RESULT_FLAG` char(1) DEFAULT NULL COMMENT '检测结果[1=正常/0=未检测/2=异常3=跳过]',
  `USE_TEST_FLAG` char(1) DEFAULT '0' COMMENT '是否生成维保路线[1=是/0=否]',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='室内标签表';

-- ----------------------------
-- Records of device_indoor_label
-- ----------------------------

-- ----------------------------
-- Table structure for device_indoor_record_inspection_results
-- ----------------------------
DROP TABLE IF EXISTS `device_indoor_record_inspection_results`;
CREATE TABLE `device_indoor_record_inspection_results` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `UPLOAD_PICTURE_IDS` varchar(255) DEFAULT NULL COMMENT '上传图片ids',
  `LABEL_ID` int DEFAULT NULL COMMENT '标签id',
  `LEAK_FLAG` char(1) DEFAULT NULL COMMENT '是否漏检[1=是/0=否]',
  `B_NAME` varchar(200) DEFAULT NULL COMMENT '建筑名称',
  `FLOOR` int DEFAULT NULL COMMENT '楼层',
  `FACILITIES_NO` varchar(200) DEFAULT NULL COMMENT '设施编号',
  `EQUIPMENT_TYPE` varchar(255) DEFAULT NULL COMMENT '设施类型',
  `EQUIPMENT_STATUS` varchar(255) DEFAULT NULL COMMENT '设施状态变更',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `INSPECTION_RESULT` char(1) DEFAULT NULL COMMENT '巡检结果[0=正常/1=异常]',
  `PROBLEM_DESCRIPTION` varchar(200) DEFAULT NULL COMMENT '问题描述',
  `HANDLING` char(1) DEFAULT NULL COMMENT '处理方式[0=已自行处理/1=上报维修]',
  `INSPECTION_PERSON` varchar(100) DEFAULT NULL COMMENT '巡检人',
  `INSPECTION_DATE` datetime DEFAULT NULL COMMENT '巡检时间',
  `MOBILE_PHONE` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `PLANNED_COMPLETION_TIME` datetime DEFAULT NULL COMMENT '计划完成时间',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `TASK_ID` int DEFAULT NULL COMMENT '任务id',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='室内设施巡检结果记录表';

-- ----------------------------
-- Records of device_indoor_record_inspection_results
-- ----------------------------

-- ----------------------------
-- Table structure for device_inspection_route
-- ----------------------------
DROP TABLE IF EXISTS `device_inspection_route`;
CREATE TABLE `device_inspection_route` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `ROUTE_NAME` varchar(200) DEFAULT NULL COMMENT '路线名',
  `LABEL_COUNT` varchar(200) DEFAULT NULL COMMENT '巡检设施个数',
  `ROUTE_FLAG` char(1) DEFAULT NULL COMMENT '0=室内路线,1=室外路线',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='巡检路线表';

-- ----------------------------
-- Records of device_inspection_route
-- ----------------------------
INSERT INTO `device_inspection_route` VALUES ('1', '中关村软件园巡检路线', '0', '0', '0', 'Mr.AG', '1', '2021-06-10 12:13:01', 'Mr.AG', '1', '2021-06-10 12:14:14', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_inspection_route` VALUES ('2', '电子大厦2楼室内巡检路线', '0', '0', '0', 'Mr.AG', '1', '2021-06-10 12:14:46', 'Mr.AG', '1', '2021-06-10 12:14:55', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for device_inspection_scheme
-- ----------------------------
DROP TABLE IF EXISTS `device_inspection_scheme`;
CREATE TABLE `device_inspection_scheme` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `INSPECTION_ROUTE_ID` int DEFAULT NULL COMMENT '巡检路线id',
  `START_TIME` date DEFAULT NULL COMMENT '开始时间',
  `END_TIME` date DEFAULT NULL COMMENT '开始时间',
  `PATROL_CYCLE` int DEFAULT NULL COMMENT '巡检周期（单位：天）',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `TIME_COUNT` int DEFAULT NULL COMMENT '巡检时间个数',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='巡检计划表';

-- ----------------------------
-- Records of device_inspection_scheme
-- ----------------------------
INSERT INTO `device_inspection_scheme` VALUES ('1', '2', '2021-06-10', '2021-06-10', '1', '0', 'Mr.AG', '1', '2021-06-10 12:16:32', 'Mr.AG', '1', '2021-06-10 12:16:32', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24', '2');

-- ----------------------------
-- Table structure for device_inspection_tasks
-- ----------------------------
DROP TABLE IF EXISTS `device_inspection_tasks`;
CREATE TABLE `device_inspection_tasks` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `USER_ID` varchar(36) DEFAULT NULL COMMENT '巡检人员id',
  `INSPECTION_ROUTE_ID` int DEFAULT NULL COMMENT '巡检路线id',
  `INSPECTION_DATE` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '巡检时间',
  `INSPECTION_TIME_PERIOD` varchar(16) DEFAULT NULL COMMENT '巡检时段',
  `PATROL_CYCLE` int DEFAULT NULL COMMENT '巡检时长',
  `STATUS` char(1) DEFAULT NULL COMMENT '状态[0=未接取/1=已接取/2=进行中/3=完成]',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='巡检任务表';

-- ----------------------------
-- Records of device_inspection_tasks
-- ----------------------------

-- ----------------------------
-- Table structure for device_inspection_team
-- ----------------------------
DROP TABLE IF EXISTS `device_inspection_team`;
CREATE TABLE `device_inspection_team` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) DEFAULT NULL COMMENT '巡检组名称',
  `crt_user_name` varchar(128) DEFAULT NULL COMMENT '创建人',
  `crt_user_id` varchar(36) DEFAULT NULL COMMENT '创建人ID',
  `crt_time` date DEFAULT NULL COMMENT '创建时间',
  `upd_user_name` varchar(128) DEFAULT NULL COMMENT '最后更新人',
  `upd_user_id` varchar(36) DEFAULT NULL COMMENT '最后更新人ID',
  `upd_time` date DEFAULT NULL COMMENT '最后更新时间',
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='巡检组 ';

-- ----------------------------
-- Records of device_inspection_team
-- ----------------------------

-- ----------------------------
-- Table structure for device_inspection_team_member
-- ----------------------------
DROP TABLE IF EXISTS `device_inspection_team_member`;
CREATE TABLE `device_inspection_team_member` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `inspection_team_id` bigint DEFAULT NULL COMMENT '巡检组ID',
  `member_user_id` varchar(36) DEFAULT NULL COMMENT '成员用户ID',
  `is_leader` char(1) DEFAULT NULL COMMENT '是否组长 1是，0否',
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='巡检组与用户关联表';

-- ----------------------------
-- Records of device_inspection_team_member
-- ----------------------------

-- ----------------------------
-- Table structure for device_inspection_time
-- ----------------------------
DROP TABLE IF EXISTS `device_inspection_time`;
CREATE TABLE `device_inspection_time` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `INSPECTION_SCHEME_ID` int DEFAULT NULL COMMENT '巡检计划id',
  `INSPECTION_TIME` varchar(16) DEFAULT NULL COMMENT '巡检时段',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='巡检时段表';

-- ----------------------------
-- Records of device_inspection_time
-- ----------------------------
INSERT INTO `device_inspection_time` VALUES ('1', '1', '01:15-02:15', '0', 'Mr.AG', '1', '2021-06-10 12:16:32', 'Mr.AG', '1', '2021-06-10 12:16:32', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_inspection_time` VALUES ('2', '1', '13:16-14:17', '0', 'Mr.AG', '1', '2021-06-10 12:16:32', 'Mr.AG', '1', '2021-06-10 12:16:32', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for device_item_value
-- ----------------------------
DROP TABLE IF EXISTS `device_item_value`;
CREATE TABLE `device_item_value` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `ITEM_ID` int DEFAULT NULL COMMENT '检查项id',
  `RESULTS_ID` int DEFAULT NULL COMMENT '巡检结果记录id',
  `INSPECTION_FLAG` char(1) DEFAULT NULL COMMENT '0=室内设施巡检,1=室外设施巡检',
  `ITEM_VAULE` varchar(255) DEFAULT NULL COMMENT '检查值',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='巡检结果记录对应检测项值表';

-- ----------------------------
-- Records of device_item_value
-- ----------------------------

-- ----------------------------
-- Table structure for device_measuring_point
-- ----------------------------
DROP TABLE IF EXISTS `device_measuring_point`;
CREATE TABLE `device_measuring_point` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `MEASURING_POINT` varchar(100) DEFAULT NULL COMMENT '测点',
  `CODE_NAME` varchar(100) DEFAULT NULL COMMENT '测点代号',
  `DATA_UNIT` varchar(100) DEFAULT NULL COMMENT '数据单位',
  `MEASURING_POINT_TYPE` char(1) DEFAULT NULL COMMENT '测点类型[0=火警测点/1=监测测点]',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `NORMAL_VALUE_MAX` double DEFAULT NULL COMMENT '测点正常值最大',
  `NORMAL_SYMBOL_MAX` char(1) DEFAULT NULL COMMENT '测点正常值是否包含最大值[0=不包含/1=包含]',
  `NORMAL_VALUE_MIN` double DEFAULT NULL COMMENT '测点正常值最小',
  `NORMAL_SYMBOL_MIN` char(1) DEFAULT NULL COMMENT '测点正常值是否包含最小值[0=不包含/1=包含]',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_measuring_point
-- ----------------------------
INSERT INTO `device_measuring_point` VALUES ('152', 'MOYN0540', '759186', '个', '0', '0', 'admin', '1', '2021-05-29 12:04:33', 'admin', '1', '2021-11-09 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24', '70', '1', '50', '1');
INSERT INTO `device_measuring_point` VALUES ('153', 'PYYG2749', '622012', '个', '0', '0', 'admin', '1', '2021-05-29 12:04:33', 'admin', '1', '2021-07-07 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24', '70', '1', '50', '1');
INSERT INTO `device_measuring_point` VALUES ('154', 'CJSO8216', '294570', '个', '0', '0', 'admin', '1', '2021-05-29 12:04:33', 'admin', '1', '2021-05-30 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24', '70', '1', '50', '1');
INSERT INTO `device_measuring_point` VALUES ('155', 'FLBJ6166', '390235', '个', '0', '0', 'admin', '1', '2021-05-29 12:04:33', 'admin', '1', '2021-09-12 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24', '70', '1', '50', '1');
INSERT INTO `device_measuring_point` VALUES ('156', 'XDBC6232', '482864', '个', '1', '0', 'admin', '1', '2021-05-29 12:04:33', 'admin', '1', '2021-05-23 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24', '70', '1', '50', '1');
INSERT INTO `device_measuring_point` VALUES ('157', 'VGNB2341', '164952', '个', '1', '0', 'admin', '1', '2021-05-29 12:04:33', 'admin', '1', '2021-05-08 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24', '70', '1', '50', '1');
INSERT INTO `device_measuring_point` VALUES ('158', 'CGPF9071', '882374', '个', '1', '0', 'admin', '1', '2021-05-29 12:04:33', 'admin', '1', '2021-09-02 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24', '70', '1', '50', '1');
INSERT INTO `device_measuring_point` VALUES ('159', 'FSQZ8215', '669416', '个', '1', '0', 'admin', '1', '2021-05-29 12:04:33', 'admin', '1', '2021-10-07 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24', '70', '1', '50', '1');
INSERT INTO `device_measuring_point` VALUES ('160', 'FWIJ0775', '590033', '个', '1', '0', 'admin', '1', '2021-05-29 12:04:33', 'admin', '1', '2021-04-30 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24', '70', '1', '50', '1');

-- ----------------------------
-- Table structure for device_message_notice
-- ----------------------------
DROP TABLE IF EXISTS `device_message_notice`;
CREATE TABLE `device_message_notice` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `NOTICE_TYPE` char(1) DEFAULT NULL COMMENT '通知类型[1=报警通知/2=故障通知]',
  `NOTICE_ID` int DEFAULT NULL COMMENT '通知方式表id',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='消息通知方式表';

-- ----------------------------
-- Records of device_message_notice
-- ----------------------------

-- ----------------------------
-- Table structure for device_message_recipients
-- ----------------------------
DROP TABLE IF EXISTS `device_message_recipients`;
CREATE TABLE `device_message_recipients` (
  `ID` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `NOTICE_TYPE` char(1) DEFAULT NULL COMMENT '通知类型[0=预警通知/1=报警通知/2=故障通知]',
  `MESSAGE_RECIPIENTS_USERID` varchar(255) DEFAULT NULL COMMENT '消息接收人userId',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='消息接收人表';

-- ----------------------------
-- Records of device_message_recipients
-- ----------------------------

-- ----------------------------
-- Table structure for device_networking_unit
-- ----------------------------
DROP TABLE IF EXISTS `device_networking_unit`;
CREATE TABLE `device_networking_unit` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键UUID',
  `OID` int DEFAULT NULL COMMENT '单位ID',
  `O_NAME` varchar(100) DEFAULT NULL COMMENT '单位名称',
  `O_LICENSE` varchar(100) DEFAULT NULL COMMENT '统一社会信用代码',
  `O_LICENSE_TIME` datetime DEFAULT NULL COMMENT '单位注册时间',
  `O_ADDRESS` varchar(200) DEFAULT NULL COMMENT '单位地址',
  `XZQY` char(6) DEFAULT NULL COMMENT '6位行政区编码（应符合现行国家标准《中华人民共和国行政区划代码》GB2260的规定）',
  `STREET` char(3) DEFAULT NULL COMMENT '3位街道编码（应符合现行国家标准《县以下行政区划代码编码规则》GB10114的规定）',
  `ROAD` varchar(60) DEFAULT NULL COMMENT '路名',
  `MNPH` varchar(200) DEFAULT NULL,
  `LDZ` varchar(20) DEFAULT NULL COMMENT '楼栋幢',
  `ADDRESS_DETAIL` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `GIS` varchar(100) DEFAULT NULL COMMENT '地理坐标经纬度',
  `O_PHONE` varchar(50) DEFAULT NULL COMMENT '单位电话',
  `SAFE_DUTY_NAME` varchar(50) DEFAULT NULL COMMENT '单位消防安全责任人姓名',
  `SAFE_DUTY_PHONE` varchar(50) DEFAULT NULL COMMENT '单位消防安全责任人电话',
  `SAFE_DUTY_IDCARD` varchar(50) DEFAULT NULL COMMENT '单位安全消防责任人身份证号',
  `LEGAL_NAME` varchar(50) DEFAULT NULL COMMENT '企业法人姓名',
  `LEGAL_PHONE` varchar(50) DEFAULT NULL COMMENT '企业法人电话',
  `LEGAL_IDCARD` varchar(50) DEFAULT NULL COMMENT '企业法人身份证号',
  `SAFE_MANAGER_NAME` varchar(50) DEFAULT NULL COMMENT '单位消防安全管理人员姓名',
  `SAFE_MANAGER_PHONE` varchar(50) DEFAULT NULL COMMENT '单位消防安全管理人员电话',
  `SAFE_MANAGER_IDCARD` varchar(50) DEFAULT NULL COMMENT '单位安全消防管理人员身份证号',
  `O_LINKMAN` varchar(50) DEFAULT NULL COMMENT '单位联系人',
  `O_LINKPHONE` varchar(50) DEFAULT NULL COMMENT '单位联系电话',
  `O_TYPE` char(1) DEFAULT NULL COMMENT '单位类别[1=重点单位；2=一般单位；3=九小场所；4=其他单位',
  `O_NATURE` varchar(255) DEFAULT NULL COMMENT '单位性质',
  `O_CLASS` varchar(20) DEFAULT NULL COMMENT '单位类型',
  `KEYUNIT_TIME` datetime DEFAULT NULL COMMENT '确定重点单位时间',
  `IS_KEYUNIT` char(1) DEFAULT NULL COMMENT '是否重点单位[0=否；1=是]',
  `O_OTHER` varchar(200) DEFAULT NULL COMMENT '单位其他情况',
  `CHANGETIME` datetime DEFAULT NULL COMMENT '修改时间',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CHANGEACC` varchar(255) DEFAULT NULL,
  `CREATEACC` varchar(255) DEFAULT NULL,
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_networking_unit
-- ----------------------------
INSERT INTO `device_networking_unit` VALUES ('111', '111', '东方电子科技大厦', '111111111111', '2021-05-16 00:00:00', '北京市海淀区创业路2号1号楼', '100029', '125', '上地八街', '7号', '1号楼', '北京市海淀区创业路2号1号楼', '111.313348,30.053734', '-52869993', '李四001', '13581241851', '110106200301103327', '企业法人姓名001', '13581241851', '110106200301103327', ' 单位消防安全管理人员姓名001', '13581241851', '110106200301103327', ' 单位联系人001', '13581241851', '2', '国有', '大型央企', '2021-05-16 00:00:00', '1', '单位其他情况001', '2021-05-16 11:00:11', '2021-05-16 11:00:11', '修改接受001', '创建接受001', '0', 'admin', '1', '2021-05-19 22:56:28', 'admin', '1', '2021-05-19 22:56:28', null, 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for device_notice
-- ----------------------------
DROP TABLE IF EXISTS `device_notice`;
CREATE TABLE `device_notice` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `NOTICE` varchar(50) DEFAULT NULL COMMENT '通知方式',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_notice
-- ----------------------------
INSERT INTO `device_notice` VALUES ('1', 'APP推送', '0', null, null, '2018-08-09 06:00:54', null, null, '2018-08-09 06:00:54', null, null);
INSERT INTO `device_notice` VALUES ('2', '短信推送', '0', null, null, '2018-08-09 06:11:50', null, null, '2018-08-09 06:11:50', null, null);
INSERT INTO `device_notice` VALUES ('3', '语音电话', '0', null, null, '2018-09-30 15:25:08', null, null, '2018-09-30 15:25:08', null, null);

-- ----------------------------
-- Table structure for device_outdoor_label
-- ----------------------------
DROP TABLE IF EXISTS `device_outdoor_label`;
CREATE TABLE `device_outdoor_label` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `FACILITIES_TYPE_ID` int DEFAULT NULL COMMENT '设施类型id',
  `OID` int DEFAULT NULL COMMENT '联网单位ID',
  `FACILITIES_NO` varchar(200) DEFAULT NULL COMMENT '设施编号',
  `STATUS` char(1) DEFAULT NULL COMMENT '状态[0=正常/1=维修中]',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `LAST_INSPECTION_TIME` datetime DEFAULT NULL COMMENT '最近巡检时间',
  `QR_CODE_PATH` varchar(200) DEFAULT NULL COMMENT '二维码路径',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `POSITION_SIGN` varchar(50) DEFAULT NULL COMMENT '位置标记（中间用,号隔开）',
  `USE_FLAG` char(1) DEFAULT NULL COMMENT '是否生成路线[1=是/0=否]',
  `RESULT_FLAG` char(1) DEFAULT NULL COMMENT '检测结果[1=正常/0=未检测/2=异常3=跳过]',
  `USE_TEST_FLAG` char(1) DEFAULT '0' COMMENT '是否维保生成路线[1=是/0=否]',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='室外标签表';

-- ----------------------------
-- Records of device_outdoor_label
-- ----------------------------
INSERT INTO `device_outdoor_label` VALUES ('1', '9', '111', '6000000517441473', '0', '用于1号楼监控', null, null, '0', 'Mr.AG', '1', '2021-06-10 12:11:50', 'Mr.AG', '1', '2021-06-10 12:11:50', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24', '116.308158,40.045546', '0', '0', '0');

-- ----------------------------
-- Table structure for device_outdoor_record_inspection_results
-- ----------------------------
DROP TABLE IF EXISTS `device_outdoor_record_inspection_results`;
CREATE TABLE `device_outdoor_record_inspection_results` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `UPLOAD_PICTURE_IDS` varchar(255) DEFAULT NULL COMMENT '上传图片ids',
  `LABEL_ID` int DEFAULT NULL COMMENT '标签id',
  `LEAK_FLAG` char(1) DEFAULT NULL COMMENT '是否漏检[1=是/0=否]',
  `FACILITIES_NO` varchar(200) DEFAULT NULL COMMENT '设施编号',
  `EQUIPMENT_TYPE` varchar(255) DEFAULT NULL COMMENT '设施类型',
  `EQUIPMENT_STATUS` varchar(255) DEFAULT NULL COMMENT '设施状态变更',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `INSPECTION_RESULT` char(1) DEFAULT NULL COMMENT '巡检结果[0=正常/1=异常]',
  `PROBLEM_DESCRIPTION` varchar(200) DEFAULT NULL COMMENT '问题描述',
  `HANDLING` char(1) DEFAULT NULL COMMENT '处理方式[0=已自行处理/1=上报维修]',
  `INSPECTION_PERSON` varchar(100) DEFAULT NULL COMMENT '巡检人',
  `INSPECTION_DATE` datetime DEFAULT NULL COMMENT '巡检时间',
  `MOBILE_PHONE` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `PLANNED_COMPLETION_TIME` datetime DEFAULT NULL COMMENT '计划完成时间',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `TASK_ID` int DEFAULT NULL COMMENT '任务id',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='室外设施巡检结果记录表';

-- ----------------------------
-- Records of device_outdoor_record_inspection_results
-- ----------------------------

-- ----------------------------
-- Table structure for device_route_label
-- ----------------------------
DROP TABLE IF EXISTS `device_route_label`;
CREATE TABLE `device_route_label` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `ROUTE_ID` int DEFAULT NULL COMMENT '路线id',
  `LABEL_ID` int DEFAULT NULL COMMENT '标签id',
  `LABEL_FLAG` char(1) DEFAULT NULL COMMENT '0=室内标签,1=室外标签',
  `ROUTE_FLAG` char(1) DEFAULT NULL COMMENT '0=巡检路线,1=检测路线',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='路线和标签关联表';

-- ----------------------------
-- Records of device_route_label
-- ----------------------------

-- ----------------------------
-- Table structure for device_sensor
-- ----------------------------
DROP TABLE IF EXISTS `device_sensor`;
CREATE TABLE `device_sensor` (
  `ID` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `BUILDING_ID` int DEFAULT NULL COMMENT '建筑列表id',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统id',
  `SENSOR_TYPE_ID` int DEFAULT NULL COMMENT '传感器类型id',
  `CD_ID` int DEFAULT NULL COMMENT '采集设备id(备用字段)',
  `FIELD_STATUS` char(1) DEFAULT NULL COMMENT '数据字段状态[0=不正常/1=正常]（设备，厂商，型号，楼层是否正常）',
  `STATUS` char(1) DEFAULT NULL COMMENT '状态[0=故障/1=报警/2=正常]（需要产品确认）',
  `SENSOR_NO` varchar(200) DEFAULT NULL COMMENT '传感器编号',
  `FLOOR` int DEFAULT NULL COMMENT '楼层',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `POSITION_SIGN` varchar(50) DEFAULT NULL COMMENT '传感器在平面图的位置标记（中间用,号隔开）',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL COMMENT '创建者ID',
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL COMMENT '修改者ID',
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL COMMENT '部门ID',
  `TENANT_ID` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `STATUS_TIME` datetime DEFAULT NULL COMMENT '数据上传时间',
  `HYDRANT_ID` int DEFAULT NULL COMMENT '室外消火栓id',
  PRIMARY KEY (`ID`),
  KEY `sensor_index` (`STATUS`,`SENSOR_NO`,`CHANNEL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1275 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_sensor
-- ----------------------------
INSERT INTO `device_sensor` VALUES ('1', '111', '1', '1', '111', '1', '3', 'QVFTRVTMUAOP', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('2', '111', '1', '1', '111', '1', '3', 'ZLCBSJMBQJJX', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('3', '111', '1', '1', '111', '1', '2', 'BHZIYJZYFQCS', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('4', '111', '1', '1', '111', '1', '2', 'ZASSPBWLFOAS', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('5', '111', '1', '1', '111', '1', '2', 'RNDXFRBREYHU', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('6', '111', '1', '1', '111', '1', '2', 'BWENGTYOWEZJ', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('7', '111', '1', '2', '111', '1', '2', 'JHNDAFAVZZSW', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('8', '111', '1', '2', '111', '1', '2', 'DIGKBYETMWXD', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('9', '111', '1', '3', '111', '1', '2', 'QJONKZSWYQCM', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('10', '111', '1', '3', '111', '1', '0', 'ZJZKEJIQBKCG', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('11', '111', '1', '3', '111', '1', '0', 'SEJBDTFPAZSJ', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('12', '111', '1', '3', '111', '1', '1', 'YUXVQNJKPBKR', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('13', '111', '1', '3', '111', '1', '1', 'SZZXSSBMMYRO', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('14', '111', '1', '1', '111', '1', '2', 'RLWXIGWORPTR', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('15', '111', '1', '1', '111', '1', '2', 'RSVSZKZNMKOV', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('16', '111', '1', '1', '111', '1', '2', 'VKSTKSTJLDBM', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('17', '111', '1', '1', '111', '1', '2', 'YMVBUDTNFVVS', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('18', '111', '1', '1', '111', '1', '2', 'OYXPWEOGWQWL', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('19', '111', '1', '1', '111', '1', '2', 'BZFUPZSVFUFY', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('20', '111', '1', '2', '111', '1', '2', 'BFIUDBSFQSNV', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('21', '111', '1', '2', '111', '1', '2', 'WXZPBDSWWURF', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('22', '111', '1', '3', '111', '1', '2', 'FDSGJEZBZSSM', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('23', '111', '1', '3', '111', '1', '0', 'BDNSMQEXQZWP', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('24', '111', '1', '3', '111', '1', '0', 'JYPXFDDCUEAS', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('25', '111', '1', '3', '111', '1', '1', 'SXEXYHERFPXL', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('26', '111', '1', '3', '111', '1', '1', 'AWKGBHKPFERK', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('27', '111', '1', '1', '111', '1', '2', 'JPMWFSWTJEOP', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('28', '111', '1', '1', '111', '1', '2', 'EKCJMWPWQGRY', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('29', '111', '1', '1', '111', '1', '2', 'XJUMTWOLGNIO', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('30', '111', '1', '1', '111', '1', '2', 'CUGCMDXTNNDF', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('31', '111', '1', '1', '111', '1', '2', 'ECTQYVNPKRSH', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('32', '111', '1', '1', '111', '1', '2', 'HFRFFDOAVWRS', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('33', '111', '1', '2', '111', '1', '2', 'MQQGQKUWBSMF', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('34', '111', '1', '2', '111', '1', '2', 'VKKAVADOEIYK', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('35', '111', '1', '3', '111', '1', '2', 'WFRNMJHJWYQL', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('36', '111', '1', '3', '111', '1', '2', 'BUNTACPWEDYJ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('37', '111', '1', '3', '111', '1', '2', 'DJQWEBOVHWCV', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('38', '111', '1', '3', '111', '1', '2', 'OBLUWXXBRHVI', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('39', '111', '1', '3', '111', '1', '2', 'SJFJJCTKDJNC', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('40', '111', '1', '1', '111', '1', '2', 'UAQQZNBOXFRC', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('41', '111', '1', '1', '111', '1', '2', 'RKCSWFTRZFTK', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('42', '111', '1', '1', '111', '1', '2', 'GGYNCEDSFPRE', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('43', '111', '1', '1', '111', '1', '2', 'MNRNBENWUEZQ', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('44', '111', '1', '1', '111', '1', '2', 'ETSNEUFXJIDD', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('45', '111', '1', '1', '111', '1', '2', 'XETRKGQUMSNC', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('46', '111', '1', '2', '111', '1', '2', 'JAMRLJYASRUG', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('47', '111', '1', '2', '111', '1', '2', 'SVMGXKKGIQZU', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('48', '111', '1', '3', '111', '1', '2', 'JJDBEMHADHCZ', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('49', '111', '1', '3', '111', '1', '2', 'SOHGIZFXITGP', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('50', '111', '1', '3', '111', '1', '2', 'XMBKSGUHUFPW', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('51', '111', '1', '3', '111', '1', '2', 'FDJHONQVDEJA', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('52', '111', '1', '3', '111', '1', '2', 'JIDEJNVBOBJS', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('53', '111', '1', '1', '111', '1', '2', 'NPGXHUTPTLCN', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('54', '111', '1', '1', '111', '1', '2', 'JDFGXHXDELCJ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('55', '111', '1', '1', '111', '1', '2', 'OJZWDOIDAHLW', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('56', '111', '1', '1', '111', '1', '2', 'IXGMWICCSNYA', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('57', '111', '1', '1', '111', '1', '2', 'XLWJNYSSOWVJ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('58', '111', '1', '1', '111', '1', '2', 'DSBRATEDVUJE', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('59', '111', '1', '2', '111', '1', '2', 'CYTZQDAHQSKR', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('60', '111', '1', '2', '111', '1', '2', 'LBXBGTMQIYPX', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('61', '111', '1', '3', '111', '1', '2', 'ZABNQAPFWURJ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('62', '111', '1', '3', '111', '1', '2', 'HNEFJEUCEDET', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('63', '111', '1', '3', '111', '1', '2', 'XMINBTDLPEGB', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('64', '111', '1', '3', '111', '1', '2', 'ZJEJSEQETRCW', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('65', '111', '1', '3', '111', '1', '2', 'SNYQHHMUOZUB', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('66', '111', '1', '1', '111', '1', '2', 'RLCAXPAMWZOO', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('67', '111', '1', '1', '111', '1', '2', 'CEWBTUFHURLG', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('68', '111', '1', '1', '111', '1', '2', 'QOGTXFDQCQKM', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('69', '111', '1', '1', '111', '1', '2', 'KICUPIUSMBUF', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('70', '111', '1', '1', '111', '1', '2', 'MFHKFUGHKEOI', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('71', '111', '1', '1', '111', '1', '2', 'AOLUNQBODUYH', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('72', '111', '1', '2', '111', '1', '2', 'THQJADLELFUK', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('73', '111', '1', '2', '111', '1', '2', 'CSZBBOMSORPC', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('74', '111', '1', '3', '111', '1', '2', 'NZJCQIUUSMHQ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('75', '111', '1', '3', '111', '1', '2', 'ENHAGRQTBXHT', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('76', '111', '1', '3', '111', '1', '2', 'IDELEGJEDWTT', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('77', '111', '1', '3', '111', '1', '2', 'DTOJDSZZPRYZ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('78', '111', '1', '3', '111', '1', '2', 'QEYMVOYSNMLL', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('79', '111', '1', '1', '111', '1', '2', 'UKAOKOZRIDKR', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('80', '111', '1', '1', '111', '1', '2', 'PMLMEPUPLYFT', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('81', '111', '1', '1', '111', '1', '2', 'QNRWVQCGDZZD', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('82', '111', '1', '1', '111', '1', '2', 'VRMUNEPGVEKT', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('83', '111', '1', '1', '111', '1', '2', 'QIELZJAMEFGZ', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('84', '111', '1', '1', '111', '1', '2', 'IUQEKQNGKXCG', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('85', '111', '1', '2', '111', '1', '2', 'GAGMKDNPJEYU', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('86', '111', '1', '2', '111', '1', '2', 'QDQFSYZEDIJL', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('87', '111', '1', '3', '111', '1', '2', 'ZLJCBWMBUVRU', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('88', '111', '1', '3', '111', '1', '2', 'OGJICVLVGZUB', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('89', '111', '1', '3', '111', '1', '2', 'DQLZAEBWZKGE', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('90', '111', '1', '3', '111', '1', '2', 'USKCERUGREPQ', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('91', '111', '1', '3', '111', '1', '2', 'KPCYIGWQQIPJ', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('92', '111', '1', '1', '111', '1', '2', 'JWNYSZILQVMR', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('93', '111', '1', '1', '111', '1', '2', 'GQWRXFYEPRUO', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('94', '111', '1', '1', '111', '1', '2', 'GPEYFMTPAYTY', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('95', '111', '1', '1', '111', '1', '2', 'EESNQRPPKUBG', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('96', '111', '1', '1', '111', '1', '2', 'BJRMSGMHRFAB', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('97', '111', '1', '1', '111', '1', '2', 'TJOUWTCSCKZN', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('98', '111', '1', '2', '111', '1', '2', 'XJLDJDMATFAA', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('99', '111', '1', '2', '111', '1', '2', 'QYUGUSAZLJXN', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('100', '111', '1', '3', '111', '1', '2', 'EIKTXBAOTEHP', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('101', '111', '1', '3', '111', '1', '2', 'EDIFKYJHKQMF', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('102', '111', '1', '3', '111', '1', '2', 'NMQYYCSEVAIX', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('103', '111', '1', '3', '111', '1', '2', 'EXUESPRAVZTC', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('104', '111', '1', '3', '111', '1', '2', 'SIUEPTJPTWQE', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('105', '111', '1', '1', '111', '1', '2', 'KHAOXFZOOVAV', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('106', '111', '1', '1', '111', '1', '2', 'UOQOLYGCGJPT', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('107', '111', '1', '1', '111', '1', '2', 'RRGIBOOQTPXS', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('108', '111', '1', '1', '111', '1', '2', 'QHSIMGRXVBIC', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('109', '111', '1', '1', '111', '1', '2', 'GBMQYFWQFQTM', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('110', '111', '1', '1', '111', '1', '2', 'EPYVKJFMKKHG', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('111', '111', '1', '2', '111', '1', '2', 'FXBSQOJEXAYP', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('112', '111', '1', '2', '111', '1', '2', 'YBNQWIHRPVLJ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('113', '111', '1', '3', '111', '1', '2', 'YCRCORPKKNJE', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('114', '111', '1', '3', '111', '1', '2', 'BOEACCDDSSHX', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('115', '111', '1', '3', '111', '1', '2', 'QFWGZQEGGMUC', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('116', '111', '1', '3', '111', '1', '2', 'LSVEVUXCKRQL', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('117', '111', '1', '3', '111', '1', '2', 'UCASSYATIVRN', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('118', '111', '1', '1', '111', '1', '2', 'GGWJWDMAQMJS', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('119', '111', '1', '1', '111', '1', '2', 'ALINPYOAQVAU', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('120', '111', '1', '1', '111', '1', '2', 'KPYSDDZXHTQR', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('121', '111', '1', '1', '111', '1', '2', 'HTMKYZEMILAB', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('122', '111', '1', '1', '111', '1', '2', 'VKDPAVSEPZJH', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('123', '111', '1', '1', '111', '1', '2', 'WGIGJQLXGDUO', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('124', '111', '1', '2', '111', '1', '2', 'TTYGBVHITUBN', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('125', '111', '1', '2', '111', '1', '2', 'XYSETCGZOHGZ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('126', '111', '1', '3', '111', '1', '2', 'ENAZNSFSQBUK', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('127', '111', '1', '3', '111', '1', '2', 'WBHXBPGFXXDI', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('128', '111', '1', '3', '111', '1', '2', 'WOXRMBSZIMUC', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('129', '111', '1', '3', '111', '1', '2', 'DQBWQSPMBBEZ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('130', '111', '1', '3', '111', '1', '2', 'RQEMJZJTHHKM', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('131', '111', '1', '1', '111', '1', '2', 'PLGUQZLSICSS', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('132', '111', '1', '1', '111', '1', '2', 'JCSGRCYMTSLK', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('133', '111', '1', '1', '111', '1', '2', 'QCJMUALDHNEV', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('134', '111', '1', '1', '111', '1', '2', 'IBCTOFPLAIWA', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('135', '111', '1', '1', '111', '1', '2', 'FBTBBQHMXWBH', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('136', '111', '1', '1', '111', '1', '2', 'LNDGYRIITUKJ', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('137', '111', '1', '2', '111', '1', '2', 'XHVPBFIQSCPC', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('138', '111', '1', '2', '111', '1', '2', 'CDAJRQPMCVAG', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('139', '111', '1', '3', '111', '1', '2', 'JBSZAZHSRSOD', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('140', '111', '1', '3', '111', '1', '2', 'YXWXXCJQZEOM', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('141', '111', '1', '3', '111', '1', '2', 'LYPIMHYFZNQH', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('142', '111', '1', '3', '111', '1', '2', 'GAXHMLXGWNJA', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('143', '111', '1', '3', '111', '1', '1', 'ECFFNFGYZCVI', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('144', '111', '1', '1', '111', '1', '2', 'SYIBIZFSTEII', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('145', '111', '1', '1', '111', '1', '2', 'UQWNPDKLXCHB', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('146', '111', '1', '1', '111', '1', '2', 'ORJQZNGVIHWY', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('147', '111', '1', '1', '111', '1', '2', 'PSOXYRXXJPMX', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('148', '111', '1', '1', '111', '1', '2', 'YAKVCUYDUDLA', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('149', '111', '1', '1', '111', '1', '2', 'RBCZEVTOXFGE', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('150', '111', '1', '2', '111', '1', '2', 'QPFVYNZQPOLX', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('151', '111', '1', '2', '111', '1', '2', 'NTIBLLEJKFJO', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('152', '111', '1', '3', '111', '1', '2', 'BGVGOVUVMMRW', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('153', '111', '1', '3', '111', '1', '0', 'VWLSKMFCTBRU', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('154', '111', '1', '3', '111', '1', '0', 'BSLGMXPPWUPA', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('155', '111', '1', '3', '111', '1', '1', 'NUOTMZGCTPFS', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('156', '111', '1', '3', '111', '1', '1', 'EBFBVDQAKUZG', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('157', '111', '1', '1', '111', '1', '2', 'OUEISYNQICKO', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('158', '111', '1', '1', '111', '1', '2', 'RHTOKQICUGJM', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('159', '111', '1', '1', '111', '1', '2', 'YPYQRGFPCHRV', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('160', '111', '1', '1', '111', '1', '2', 'YLEBXMZOBUQG', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('161', '111', '1', '1', '111', '1', '2', 'IJNRMZHUQZWS', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('162', '111', '1', '1', '111', '1', '2', 'TMXAHXMDIKFJ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('163', '111', '1', '2', '111', '1', '2', 'DVFTLAQIXNEG', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('164', '111', '1', '2', '111', '1', '2', 'TZVKRZTJSFTN', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('165', '111', '1', '3', '111', '1', '2', 'RLYDTSVBTLLF', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('166', '111', '1', '3', '111', '1', '0', 'NKRLHYVXVBYR', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('167', '111', '1', '3', '111', '1', '0', 'NQZBXRSPSJKR', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('168', '111', '1', '3', '111', '1', '1', 'IHNGCQFMFJHI', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('169', '111', '1', '3', '111', '1', '1', 'GDRWUWUWOUNM', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('170', '111', '1', '1', '111', '1', '2', 'VECELARFGBEC', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('171', '111', '1', '1', '111', '1', '2', 'ZLVEOMQHMOJI', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('172', '111', '1', '1', '111', '1', '2', 'BMVVXDKZRUHK', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('173', '111', '1', '1', '111', '1', '2', 'DUIDXZJYRSQQ', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('174', '111', '1', '1', '111', '1', '2', 'QKETJFAZVCRR', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('175', '111', '1', '1', '111', '1', '2', 'WQNZCRWIIGEK', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('176', '111', '1', '2', '111', '1', '2', 'USFIYSUXWPRK', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('177', '111', '1', '2', '111', '1', '2', 'TBRZKAJRHNDT', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('178', '111', '1', '3', '111', '1', '2', 'ETZHSKIOJKGO', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('179', '111', '1', '3', '111', '1', '0', 'EPTGZIXKBTVC', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('180', '111', '1', '3', '111', '1', '0', 'GRJFGXQYVIJO', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('181', '111', '1', '3', '111', '1', '1', 'MNKWJMGBDICC', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('182', '111', '1', '3', '111', '1', '1', 'BJEUIUOJQUSY', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('183', '111', '2', '1', '111', '1', '2', 'EXWLIDCRHQYK', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('184', '111', '2', '1', '111', '1', '2', 'DEXVBINWWEXO', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('185', '111', '2', '1', '111', '1', '2', 'HLNIFVSSABGT', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('186', '111', '2', '1', '111', '1', '2', 'IYGAVIRNASIV', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('187', '111', '2', '1', '111', '1', '2', 'BVKICGAWFRKE', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('188', '111', '2', '1', '111', '1', '2', 'WDIUGBDKKZUB', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('189', '111', '2', '2', '111', '1', '2', 'OWROBCVEUJZF', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('190', '111', '2', '2', '111', '1', '2', 'FNPLCPXBNHGP', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('191', '111', '2', '3', '111', '1', '2', 'EJZBERYZYVKX', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('192', '111', '2', '3', '111', '1', '0', 'PUDKGJJCAYZR', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('193', '111', '2', '3', '111', '1', '0', 'LMYXWSSNCNCK', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('194', '111', '2', '3', '111', '1', '1', 'KZUZSADSZHQB', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('195', '111', '2', '3', '111', '1', '1', 'ZOTUTXZQKOOT', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('196', '111', '2', '1', '111', '1', '2', 'RKBXVPSSDTFN', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('197', '111', '2', '1', '111', '1', '2', 'SMMXPBUELTZC', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('198', '111', '2', '1', '111', '1', '2', 'FBZBTDBHAHWI', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('199', '111', '2', '1', '111', '1', '2', 'MSTAEZXLXTFR', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('200', '111', '2', '1', '111', '1', '2', 'APZZDTTAEBUW', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('201', '111', '2', '1', '111', '1', '2', 'PJJMXKLMNIEB', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('202', '111', '2', '2', '111', '1', '2', 'WCPRTIEDPHUB', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('203', '111', '2', '2', '111', '1', '2', 'QUKJZYKGGGMI', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('204', '111', '2', '3', '111', '1', '2', 'HKXTVKOBEZBF', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('205', '111', '2', '3', '111', '1', '0', 'VRCETADKMLJQ', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('206', '111', '2', '3', '111', '1', '0', 'WOZUOVSGMNLS', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('207', '111', '2', '3', '111', '1', '1', 'YUKSSFOZMJRC', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('208', '111', '2', '3', '111', '1', '1', 'DJOIDTOEMYMC', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('209', '111', '2', '1', '111', '1', '2', 'JDXLYOGGBJOB', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('210', '111', '2', '1', '111', '1', '2', 'JKLZFTAPJLYB', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('211', '111', '2', '1', '111', '1', '2', 'BRGMTPGWCFRN', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('212', '111', '2', '1', '111', '1', '2', 'BJGUVQPAXAUZ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('213', '111', '2', '1', '111', '1', '2', 'QJRCBLROSXGC', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('214', '111', '2', '1', '111', '1', '2', 'FHKYDYZOAXUU', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('215', '111', '2', '2', '111', '1', '2', 'NMCNOHKREQLT', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('216', '111', '2', '2', '111', '1', '2', 'IIKOSIPMHMIW', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('217', '111', '2', '3', '111', '1', '2', 'CLSJSTWWKOSO', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('218', '111', '2', '3', '111', '1', '2', 'DYQXAYBOAPYG', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('219', '111', '2', '3', '111', '1', '2', 'KEECNJDVVBFD', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('220', '111', '2', '3', '111', '1', '2', 'MAFGZXNVLNGO', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('221', '111', '2', '3', '111', '1', '2', 'XGDZVMNOZVWJ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('222', '111', '2', '1', '111', '1', '2', 'PQBPWIIOJLZE', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('223', '111', '2', '1', '111', '1', '2', 'WFBVPTJRPIIP', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('224', '111', '2', '1', '111', '1', '2', 'WGITQCNRTPFI', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('225', '111', '2', '1', '111', '1', '2', 'LMCGVCBLJJUQ', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('226', '111', '2', '1', '111', '1', '2', 'WTZLOVDZLVCR', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('227', '111', '2', '1', '111', '1', '2', 'VFVVIREPHIKK', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('228', '111', '2', '2', '111', '1', '2', 'RIQFEMGKPKOT', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('229', '111', '2', '2', '111', '1', '2', 'YGIWZLBSXWTM', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('230', '111', '2', '3', '111', '1', '2', 'XOYRWBCUTQJL', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('231', '111', '2', '3', '111', '1', '2', 'BSOUVFLWFFLE', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('232', '111', '2', '3', '111', '1', '2', 'TEKXWKOLZXIX', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('233', '111', '2', '3', '111', '1', '2', 'XWJXUVTLOHHX', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('234', '111', '2', '3', '111', '1', '2', 'ZMWISCGXNENM', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('235', '111', '2', '1', '111', '1', '2', 'ESPBGEQTQDNM', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('236', '111', '2', '1', '111', '1', '2', 'YDHTKVJSHFUZ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('237', '111', '2', '1', '111', '1', '2', 'VEJLGNAPKTLV', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('238', '111', '2', '1', '111', '1', '2', 'QBZKANVUUMFJ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('239', '111', '2', '1', '111', '1', '2', 'AKEVEWLVJPZQ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('240', '111', '2', '1', '111', '1', '2', 'YFRTHDBULPNX', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('241', '111', '2', '2', '111', '1', '2', 'CMJOPPNWREZE', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('242', '111', '2', '2', '111', '1', '2', 'WJLMRXAWRIIQ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('243', '111', '2', '3', '111', '1', '2', 'DGUKXOXTFPFZ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('244', '111', '2', '3', '111', '1', '2', 'NQOSODYQKVBQ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('245', '111', '2', '3', '111', '1', '2', 'JULRSXQYCIOB', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('246', '111', '2', '3', '111', '1', '2', 'ZBWPSELKMYEK', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('247', '111', '2', '3', '111', '1', '2', 'GOPMBPJSNBKF', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('248', '111', '2', '1', '111', '1', '2', 'AOEEFOYKDSLV', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('249', '111', '2', '1', '111', '1', '2', 'HHDTTYNGIASK', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('250', '111', '2', '1', '111', '1', '2', 'USQTRTMSOPFI', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('251', '111', '2', '1', '111', '1', '2', 'MYKRTZHUAVTR', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('252', '111', '2', '1', '111', '1', '2', 'PMEJRLUTFDIK', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('253', '111', '2', '1', '111', '1', '2', 'GZCGVZWWFLQB', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('254', '111', '2', '2', '111', '1', '2', 'IBPRZZVVTGER', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('255', '111', '2', '2', '111', '1', '2', 'OAFIEXJQUNJA', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('256', '111', '2', '3', '111', '1', '2', 'CBOAVOUNUXGW', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('257', '111', '2', '3', '111', '1', '2', 'OMNRVAQMPUSZ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('258', '111', '2', '3', '111', '1', '2', 'QHTPWXVKLUQA', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('259', '111', '2', '3', '111', '1', '2', 'RFWYVSSJLUGG', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('260', '111', '2', '3', '111', '1', '2', 'LCVVMKKYELIY', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('261', '111', '2', '1', '111', '1', '2', 'DGKPTYWQDIPM', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('262', '111', '2', '1', '111', '1', '2', 'YXYARKUSGNOI', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('263', '111', '2', '1', '111', '1', '2', 'VZIVWYKQNJWN', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('264', '111', '2', '1', '111', '1', '2', 'YQFLTRYXNKTI', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('265', '111', '2', '1', '111', '1', '2', 'OSFWHJOAGEGZ', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('266', '111', '2', '1', '111', '1', '2', 'USQODDWEHREG', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('267', '111', '2', '2', '111', '1', '2', 'QLTVAKSHEVCM', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('268', '111', '2', '2', '111', '1', '2', 'QPCFMEPGGHSY', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('269', '111', '2', '3', '111', '1', '2', 'EREMOPAXEJQS', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('270', '111', '2', '3', '111', '1', '2', 'IXOWKIGQWAYO', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('271', '111', '2', '3', '111', '1', '2', 'TBQVHOIRCLUA', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('272', '111', '2', '3', '111', '1', '2', 'UDROYDTQWEOT', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('273', '111', '2', '3', '111', '1', '2', 'HCXWCLPEMMYQ', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('274', '111', '2', '1', '111', '1', '2', 'DTMTCOHZCFMZ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('275', '111', '2', '1', '111', '1', '2', 'EUSOHBZOXMCI', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('276', '111', '2', '1', '111', '1', '2', 'KSLBVNCRKHSZ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('277', '111', '2', '1', '111', '1', '2', 'OTLYKUPPAEOQ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('278', '111', '2', '1', '111', '1', '2', 'VSZWCDYGSXJD', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('279', '111', '2', '1', '111', '1', '2', 'CMWIDZQJZGHS', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('280', '111', '2', '2', '111', '1', '2', 'PPILHVNMPUFN', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('281', '111', '2', '2', '111', '1', '2', 'RZVIBUNJPDYV', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('282', '111', '2', '3', '111', '1', '2', 'NBMJHIOLETDV', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('283', '111', '2', '3', '111', '1', '2', 'RFHXHENYAQIG', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('284', '111', '2', '3', '111', '1', '2', 'CVVMAFSHBUDW', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('285', '111', '2', '3', '111', '1', '2', 'BOJGBSSNRDGP', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('286', '111', '2', '3', '111', '1', '2', 'TQPWEVKMKRYM', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('287', '111', '2', '1', '111', '1', '2', 'QETNOVDGSYOO', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('288', '111', '2', '1', '111', '1', '2', 'ATJRKHNSBRVQ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('289', '111', '2', '1', '111', '1', '2', 'SRGCFQSAJECK', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('290', '111', '2', '1', '111', '1', '2', 'KXCONCAVXUUE', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('291', '111', '2', '1', '111', '1', '2', 'DPDEAXDTJBFS', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('292', '111', '2', '1', '111', '1', '2', 'MMCLINPASJIH', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('293', '111', '2', '2', '111', '1', '2', 'YUQJGEKQZTEZ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('294', '111', '2', '2', '111', '1', '2', 'TWCBCZJZFJLJ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('295', '111', '2', '3', '111', '1', '2', 'MAUQZWKWFASA', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('296', '111', '2', '3', '111', '1', '2', 'PNBEFDSVKAQN', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('297', '111', '2', '3', '111', '1', '2', 'MHDFVHTYPXEB', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('298', '111', '2', '3', '111', '1', '2', 'MKKVQWXNCHQO', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('299', '111', '2', '3', '111', '1', '2', 'PBZFGKBMIEUX', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('300', '111', '2', '1', '111', '1', '2', 'XYEVJHYMDYJE', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('301', '111', '2', '1', '111', '1', '2', 'VIPVPTGPQXVU', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('302', '111', '2', '1', '111', '1', '2', 'IJSGLZBGGWAJ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('303', '111', '2', '1', '111', '1', '2', 'SZTVXTFXQAHM', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('304', '111', '2', '1', '111', '1', '2', 'FAIWDZELLIRR', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('305', '111', '2', '1', '111', '1', '2', 'CMPLYXJPRINU', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('306', '111', '2', '2', '111', '1', '2', 'BKRAUEEMMZSG', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('307', '111', '2', '2', '111', '1', '2', 'AZNVVWVVFJXV', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('308', '111', '2', '3', '111', '1', '2', 'BVFTJKMLSOUX', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('309', '111', '2', '3', '111', '1', '2', 'ENBTVVQUJANA', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('310', '111', '2', '3', '111', '1', '2', 'PXFKXEKFNDVJ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('311', '111', '2', '3', '111', '1', '2', 'HPHXWEPFPQMZ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('312', '111', '2', '3', '111', '1', '2', 'AJLRSFTOEUQI', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('313', '111', '2', '1', '111', '1', '2', 'TFRGKWCIVAPB', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('314', '111', '2', '1', '111', '1', '2', 'YYFFTMPPJEHX', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('315', '111', '2', '1', '111', '1', '2', 'XHSHQKBLWAMI', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('316', '111', '2', '1', '111', '1', '2', 'ZXOHJPXPWVUB', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('317', '111', '2', '1', '111', '1', '2', 'PYLNNTOTOCZI', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('318', '111', '2', '1', '111', '1', '2', 'CBTUCFLLUFQT', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('319', '111', '2', '2', '111', '1', '2', 'FBLNCMIQRSKZ', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('320', '111', '2', '2', '111', '1', '2', 'UFLOGGUHRWSX', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('321', '111', '2', '3', '111', '1', '2', 'QPMQZVYKPXYO', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('322', '111', '2', '3', '111', '1', '2', 'RIBKAQJCORTK', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('323', '111', '2', '3', '111', '1', '2', 'DZDSHTQLSNEV', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('324', '111', '2', '3', '111', '1', '2', 'KONCYCJJWRDT', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('325', '111', '2', '3', '111', '1', '1', 'DONOKGBYWBBT', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('326', '111', '2', '1', '111', '1', '2', 'KSTEMTNKYGFX', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('327', '111', '2', '1', '111', '1', '2', 'CRPZGQWYLEBL', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('328', '111', '2', '1', '111', '1', '2', 'SYRHCCAWHLTG', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('329', '111', '2', '1', '111', '1', '2', 'QQTDJCOOYLDJ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('330', '111', '2', '1', '111', '1', '2', 'YQGZGHWLIFCH', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('331', '111', '2', '1', '111', '1', '2', 'LDFVPPYTBONT', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('332', '111', '2', '2', '111', '1', '2', 'VUUXAYEAQDHB', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('333', '111', '2', '2', '111', '1', '2', 'PMWNMQNYSFOZ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('334', '111', '2', '3', '111', '1', '2', 'HFGHBUSFKSMV', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('335', '111', '2', '3', '111', '1', '0', 'DVJVAQCYGITO', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('336', '111', '2', '3', '111', '1', '0', 'EOUJZAZUWKZQ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('337', '111', '2', '3', '111', '1', '1', 'HWUDYNNFFENE', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('338', '111', '2', '3', '111', '1', '1', 'KHJXTWKZIJYE', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('339', '111', '2', '1', '111', '1', '2', 'LVOJMPYZBTYP', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('340', '111', '2', '1', '111', '1', '2', 'IFGKLEMJBOSG', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('341', '111', '2', '1', '111', '1', '2', 'AQJXKSLCGRIY', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('342', '111', '2', '1', '111', '1', '2', 'EPDULEVJEYSP', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('343', '111', '2', '1', '111', '1', '2', 'QMWYCTTKYNPU', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('344', '111', '2', '1', '111', '1', '2', 'CLNLTXMREEUT', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('345', '111', '2', '2', '111', '1', '2', 'HPQSJIHJPNPB', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('346', '111', '2', '2', '111', '1', '2', 'QPMVDVMSXCRX', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('347', '111', '2', '3', '111', '1', '2', 'WURWRBMKUQKQ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('348', '111', '2', '3', '111', '1', '0', 'ANUJRIPIXSRM', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('349', '111', '2', '3', '111', '1', '0', 'LEGSRDLJIBMR', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('350', '111', '2', '3', '111', '1', '1', 'XVSRESTQVVYN', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('351', '111', '2', '3', '111', '1', '1', 'QVGNPGMVZFFS', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('352', '111', '2', '1', '111', '1', '2', 'YEARELVJCUEI', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('353', '111', '2', '1', '111', '1', '2', 'PZLICMXNYZOU', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('354', '111', '2', '1', '111', '1', '2', 'KEDNLQVRWJBJ', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('355', '111', '2', '1', '111', '1', '2', 'JSGNRSXDLUNM', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('356', '111', '2', '1', '111', '1', '2', 'TNSPMVUMBHAI', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('357', '111', '2', '1', '111', '1', '2', 'CFEAQSUXCOIR', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('358', '111', '2', '2', '111', '1', '2', 'MUPDDRSPQKZP', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('359', '111', '2', '2', '111', '1', '2', 'POKWRKATBDCZ', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('360', '111', '2', '3', '111', '1', '2', 'SKLOPFXMYTDR', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('361', '111', '2', '3', '111', '1', '0', 'OKYGTMUMSVHX', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('362', '111', '2', '3', '111', '1', '0', 'YJBLNAFAIQZK', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('363', '111', '2', '3', '111', '1', '1', 'JQKDONJBPPFG', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('364', '111', '2', '3', '111', '1', '1', 'AFZSIFUKBFSX', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('365', '111', '6', '5', '111', '1', '3', 'OLAACTUDVMYT', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('366', '111', '6', '5', '111', '1', '3', 'ABMVQOCPSBOA', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('367', '111', '6', '5', '111', '1', '2', 'RJQGOUXWVXOU', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('368', '111', '6', '5', '111', '1', '2', 'HWVQJETBKSDQ', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('369', '111', '6', '5', '111', '1', '2', 'LOGLTJPYLEWZ', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('370', '111', '6', '5', '111', '1', '2', 'IFQJULUBOVHQ', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('371', '111', '6', '5', '111', '1', '2', 'KKZHODGKIGST', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('372', '111', '6', '5', '111', '1', '2', 'WRDYVDDVSMOY', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('373', '111', '6', '5', '111', '1', '2', 'NQKYMBJMJDEX', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('374', '111', '6', '5', '111', '1', '0', 'OKZDDOEMZPRY', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('375', '111', '6', '5', '111', '1', '0', 'YMMFZUAUJGAI', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('376', '111', '6', '5', '111', '1', '1', 'WSYPANANBPYU', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('377', '111', '6', '5', '111', '1', '1', 'CEFBPQCCZGUV', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('378', '111', '6', '5', '111', '1', '2', 'VEBDUMGEUDHS', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('379', '111', '6', '5', '111', '1', '2', 'ZCAOAPTHBHSF', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('380', '111', '6', '5', '111', '1', '2', 'PQDYEPLIOLWU', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('381', '111', '6', '5', '111', '1', '2', 'EEXYBJQKXLWJ', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('382', '111', '6', '5', '111', '1', '2', 'QEYQMHPLCZOU', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('383', '111', '6', '5', '111', '1', '2', 'XSGEXJTZYTBE', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('384', '111', '6', '6', '111', '1', '2', 'JQNWWTMHVPSP', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('385', '111', '6', '6', '111', '1', '2', 'CLDHDNXJWTQE', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('386', '111', '6', '6', '111', '1', '2', 'UXRLZKGSCYTM', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('387', '111', '6', '6', '111', '1', '0', 'JKHRXJOBPHZN', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('388', '111', '6', '6', '111', '1', '0', 'VIHEMDCBLZIL', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('389', '111', '6', '6', '111', '1', '1', 'ZDPMAYEXMEUC', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('390', '111', '6', '6', '111', '1', '1', 'YPVNVKATMLTO', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('391', '111', '6', '6', '111', '1', '2', 'OVAQQNFTKDLF', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('392', '111', '6', '6', '111', '1', '2', 'DYTJMKDEGSPM', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('393', '111', '6', '6', '111', '1', '2', 'XFBVMFCTYGQH', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('394', '111', '6', '6', '111', '1', '2', 'EHHURVDURHRI', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('395', '111', '6', '6', '111', '1', '2', 'OVZQQCQMKIBO', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('396', '111', '6', '6', '111', '1', '2', 'TZSJZZPCZPFL', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('397', '111', '6', '6', '111', '1', '2', 'XIZMHRSRGMUT', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('398', '111', '6', '6', '111', '1', '2', 'CNPMBCTCSXZJ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('399', '111', '6', '6', '111', '1', '2', 'UUTYKYGMIKVA', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('400', '111', '6', '6', '111', '1', '2', 'FHCXPLPSTPOT', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('401', '111', '6', '6', '111', '1', '2', 'VNTWTUKMBXHV', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('402', '111', '6', '6', '111', '1', '2', 'RKPXFGGCHVBQ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('403', '111', '6', '6', '111', '1', '2', 'ERZZRTHJJETU', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('404', '111', '6', '6', '111', '1', '2', 'HGIFYEDAZQFV', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('405', '111', '6', '6', '111', '1', '2', 'CGQRHSVZLYOJ', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('406', '111', '6', '6', '111', '1', '2', 'DQTFVFBXUYZU', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('407', '111', '6', '6', '111', '1', '2', 'HTYISDVQRGHJ', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('408', '111', '6', '6', '111', '1', '2', 'UXQZQNJHJDPG', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('409', '111', '6', '6', '111', '1', '2', 'CNHNKDVPDJPC', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('410', '111', '6', '6', '111', '1', '2', 'PYGBWFPZLGZA', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('411', '111', '6', '6', '111', '1', '2', 'RXUEDRCANFDE', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('412', '111', '6', '6', '111', '1', '2', 'WPVEZLYPLBRP', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('413', '111', '6', '6', '111', '1', '2', 'ZNDEZNHDJCJA', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('414', '111', '6', '6', '111', '1', '2', 'GBJYSSLWNMQQ', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('415', '111', '6', '6', '111', '1', '2', 'EPHCQMMRPWIG', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('416', '111', '6', '6', '111', '1', '2', 'JTHRZIVXRFJC', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('417', '111', '6', '6', '111', '1', '2', 'GEQIBDFOOSSS', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('418', '111', '6', '6', '111', '1', '2', 'FHINCEEOKMNB', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('419', '111', '6', '6', '111', '1', '2', 'BZBDWYGZXJPI', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('420', '111', '6', '6', '111', '1', '2', 'SGAMNSFOVBGG', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('421', '111', '6', '6', '111', '1', '2', 'NLDARCUGYSYV', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('422', '111', '6', '6', '111', '1', '2', 'DMIQXIQQVWJT', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('423', '111', '6', '6', '111', '1', '2', 'KVSORMOEDAMO', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('424', '111', '6', '6', '111', '1', '2', 'XZHYTFFDVYXT', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('425', '111', '6', '6', '111', '1', '2', 'SFSGPJVOOYZL', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('426', '111', '6', '6', '111', '1', '2', 'SXPUWYMEZQFJ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('427', '111', '6', '6', '111', '1', '2', 'QJQDIJIASQIT', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('428', '111', '6', '6', '111', '1', '2', 'YKXZQMDBDDCQ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('429', '111', '6', '6', '111', '1', '2', 'SYQNRBIWYYKG', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('430', '111', '6', '6', '111', '1', '2', 'OSDNVFGTLDHT', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('431', '111', '6', '6', '111', '1', '2', 'FAVWKZEAEGGS', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('432', '111', '6', '6', '111', '1', '2', 'XZXLPQXPONDU', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('433', '111', '6', '6', '111', '1', '2', 'HXLIIUGOHTVZ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('434', '111', '6', '6', '111', '1', '2', 'TFSVTEUHYZVE', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('435', '111', '6', '6', '111', '1', '2', 'NRLAGMCBYZNB', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('436', '111', '6', '6', '111', '1', '2', 'IVOHVBRBJKKN', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('437', '111', '6', '6', '111', '1', '2', 'NZMIZXFWUCHA', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('438', '111', '6', '6', '111', '1', '2', 'ALKNOSHXKEPN', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('439', '111', '6', '6', '111', '1', '2', 'SZXDQEODYEBY', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('440', '111', '6', '6', '111', '1', '2', 'EZNWIEZXYJFM', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('441', '111', '6', '6', '111', '1', '2', 'NESGYWXVYHMV', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('442', '111', '6', '6', '111', '1', '2', 'JUWJRSIDDIPX', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('443', '111', '6', '6', '111', '1', '2', 'AFSWCXZDBOAO', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('444', '111', '6', '6', '111', '1', '2', 'RCXOCMNHHMJM', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('445', '111', '6', '6', '111', '1', '2', 'IOLUDTKAWHXR', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('446', '111', '6', '6', '111', '1', '2', 'QYVAXLXTNGNT', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('447', '111', '6', '6', '111', '1', '2', 'LLZZNENWJTDK', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('448', '111', '6', '6', '111', '1', '2', 'GHKPWIOKLFRE', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('449', '111', '6', '6', '111', '1', '2', 'IKJORIHNVAQN', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('450', '111', '6', '6', '111', '1', '2', 'QPHTQYLJLTND', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('451', '111', '6', '6', '111', '1', '2', 'KLQPWQAHUMCZ', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('452', '111', '6', '6', '111', '1', '2', 'HFUKDGTCSKYH', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('453', '111', '6', '6', '111', '1', '2', 'LBCNRBPSGQXP', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('454', '111', '6', '6', '111', '1', '2', 'KRYTYYGDOSJQ', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('455', '111', '6', '6', '111', '1', '2', 'ENDKEAWVQNDC', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('456', '111', '6', '6', '111', '1', '2', 'TEOBAIMYMRRI', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('457', '111', '6', '6', '111', '1', '2', 'ZFCBVGERCNXO', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('458', '111', '6', '6', '111', '1', '2', 'VCOBHUUMNAQR', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('459', '111', '6', '6', '111', '1', '2', 'QFOTMOVRXVZR', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('460', '111', '6', '6', '111', '1', '2', 'HDNZGMBKIJRP', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('461', '111', '6', '6', '111', '1', '2', 'CRHTZFYGZEPM', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('462', '111', '6', '6', '111', '1', '2', 'FIKYDAGRADJR', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('463', '111', '6', '6', '111', '1', '2', 'YDRHHZQFNKND', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('464', '111', '6', '6', '111', '1', '2', 'OLXJBWUJRBJN', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('465', '111', '6', '6', '111', '1', '2', 'MVSYLMDWROCQ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('466', '111', '6', '6', '111', '1', '2', 'UIKEMKRBPZTT', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('467', '111', '6', '6', '111', '1', '2', 'DWKKFISMFKCO', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('468', '111', '6', '6', '111', '1', '2', 'TDIYXPPDBAFO', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('469', '111', '6', '6', '111', '1', '2', 'CKRBWMIBKVRB', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('470', '111', '6', '6', '111', '1', '2', 'ODOGMRUWSDYB', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('471', '111', '6', '6', '111', '1', '2', 'UJRTDIPVGSXU', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('472', '111', '6', '6', '111', '1', '2', 'ZAOSZXXXLMCO', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('473', '111', '6', '6', '111', '1', '2', 'ANINAQLUNJKQ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('474', '111', '6', '6', '111', '1', '2', 'FUTIHFKMDNRX', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('475', '111', '6', '6', '111', '1', '2', 'WFIIKCRWLDWB', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('476', '111', '6', '6', '111', '1', '2', 'HAZWYZVGLEBC', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('477', '111', '6', '6', '111', '1', '2', 'PSSJBQTCWLPD', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('478', '111', '6', '6', '111', '1', '2', 'DZEWOBMUFNWK', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('479', '111', '6', '6', '111', '1', '2', 'JACLEKPXMCDP', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('480', '111', '6', '6', '111', '1', '2', 'WRAHZSLWQJYP', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('481', '111', '6', '6', '111', '1', '2', 'ATAQQZGTOJXU', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('482', '111', '6', '6', '111', '1', '2', 'CMHIEFYPIVHI', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('483', '111', '6', '6', '111', '1', '2', 'NTAGRXSDATMY', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('484', '111', '6', '6', '111', '1', '2', 'DGDYPUVRYTCJ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('485', '111', '6', '6', '111', '1', '2', 'EVFDPGTLOLBD', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('486', '111', '6', '6', '111', '1', '2', 'ZAOLFGSUGPPY', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('487', '111', '6', '6', '111', '1', '2', 'OLCEBJGBQMDN', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('488', '111', '6', '6', '111', '1', '2', 'KKQICFWULTAD', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('489', '111', '6', '6', '111', '1', '2', 'BMEBKKIGRTWO', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('490', '111', '6', '6', '111', '1', '2', 'PKMZYFKZFHPN', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('491', '111', '6', '6', '111', '1', '2', 'WQPOLBUXRXBB', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('492', '111', '6', '6', '111', '1', '2', 'GGDMCGKUANWV', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('493', '111', '6', '6', '111', '1', '2', 'XPJNUTIZOMQA', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('494', '111', '6', '6', '111', '1', '2', 'QVLUWRQPQIHZ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('495', '111', '6', '6', '111', '1', '2', 'UARUHIJYTSVN', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('496', '111', '6', '6', '111', '1', '2', 'YXSSXUVZZOCF', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('497', '111', '6', '6', '111', '1', '2', 'SGEVQNGIBHNG', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('498', '111', '6', '6', '111', '1', '2', 'QKVTQCXLMYIF', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('499', '111', '6', '6', '111', '1', '2', 'DYAIKFOFHNYO', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('500', '111', '6', '6', '111', '1', '2', 'BPOTTEBZVOIE', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('501', '111', '6', '6', '111', '1', '2', 'IRCIGXBCLPGP', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('502', '111', '6', '6', '111', '1', '2', 'UMCJYMHANURJ', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('503', '111', '6', '6', '111', '1', '2', 'AWSLROSMFOVH', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('504', '111', '6', '6', '111', '1', '2', 'COEWPHSKJZOW', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('505', '111', '6', '6', '111', '1', '2', 'QUQZOVEQHIRZ', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('506', '111', '6', '6', '111', '1', '2', 'RHQTTIASYHCJ', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('507', '111', '6', '6', '111', '1', '1', 'RNXZNDCQEZMT', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('508', '111', '6', '6', '111', '1', '2', 'RSTXMXWNDOWF', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('509', '111', '6', '6', '111', '1', '2', 'UMDGPPFULDQW', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('510', '111', '6', '6', '111', '1', '2', 'ZKKUWCMPGJPI', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('511', '111', '6', '6', '111', '1', '2', 'PDPDOIOMAWAE', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('512', '111', '6', '6', '111', '1', '2', 'MZNKHCFHRGBI', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('513', '111', '6', '6', '111', '1', '2', 'UBMLYNAJMWWX', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('514', '111', '6', '6', '111', '1', '2', 'RCSQLLHUAXPF', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('515', '111', '6', '6', '111', '1', '2', 'NGBACSLLUWIL', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('516', '111', '6', '6', '111', '1', '2', 'STVBFUKVOYKX', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('517', '111', '6', '6', '111', '1', '0', 'GKVSDUETTRQC', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('518', '111', '6', '6', '111', '1', '0', 'SRMNFBAMYMLP', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('519', '111', '6', '6', '111', '1', '1', 'PVNYOQEWNNPJ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('520', '111', '6', '6', '111', '1', '1', 'UBFIHOBXEFKP', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('521', '111', '6', '6', '111', '1', '2', 'ZJXCATXKTRGL', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('522', '111', '6', '6', '111', '1', '2', 'JJQEBYYKWECQ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('523', '111', '6', '6', '111', '1', '2', 'DJIGADHTNQYN', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('524', '111', '6', '6', '111', '1', '2', 'WGEDHFFDZTKI', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('525', '111', '6', '6', '111', '1', '2', 'WMOXPPEEDDYW', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('526', '111', '6', '6', '111', '1', '2', 'IMHAMANFPSJZ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('527', '111', '6', '6', '111', '1', '2', 'RHVCRQVAFEKY', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('528', '111', '6', '6', '111', '1', '2', 'BMTOWDDFTXBV', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('529', '111', '6', '6', '111', '1', '2', 'GQGTULJJKBCN', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('530', '111', '6', '6', '111', '1', '0', 'XOYYQXQULFUF', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('531', '111', '6', '6', '111', '1', '0', 'MERKIYOOINEX', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('532', '111', '6', '6', '111', '1', '1', 'SMHKPLNDAZYQ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('533', '111', '6', '6', '111', '1', '1', 'BMNATRTXEAAG', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('534', '111', '6', '6', '111', '1', '2', 'PMOIJIQYDOCU', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('535', '111', '6', '6', '111', '1', '2', 'MJHFGCPXZEKN', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('536', '111', '6', '6', '111', '1', '2', 'SJITLLBPQNZV', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('537', '111', '6', '6', '111', '1', '2', 'RMZJSGCSIJAP', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('538', '111', '6', '6', '111', '1', '2', 'VUPDNZFCXNDE', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('539', '111', '6', '6', '111', '1', '2', 'QVTMXUPFDMLE', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('540', '111', '6', '6', '111', '1', '2', 'APYQVOZTFJIP', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('541', '111', '6', '6', '111', '1', '2', 'RIGHWXKBSGON', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('542', '111', '6', '6', '111', '1', '2', 'PSMVSNWOCFBA', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('543', '111', '6', '6', '111', '1', '0', 'IGXZNKWNRIEV', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('544', '111', '6', '6', '111', '1', '0', 'WPDOTNTSUMGQ', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('545', '111', '6', '6', '111', '1', '1', 'AMPQCZHMEGNI', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('546', '111', '6', '6', '111', '1', '1', 'GQJCCQSVXQNR', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('547', '111', '6', '6', '111', '1', '2', 'TKKOIPMIZIUK', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('548', '111', '6', '6', '111', '1', '2', 'BTEZURAKXVJD', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('549', '111', '6', '6', '111', '1', '2', 'OVCMMROJRRXJ', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('550', '111', '6', '6', '111', '1', '2', 'WYPKRBDFRMOW', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('551', '111', '6', '6', '111', '1', '2', 'KFMQGUMATNBE', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('552', '111', '6', '6', '111', '1', '2', 'DORBQXIVYEFR', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('553', '111', '6', '6', '111', '1', '2', 'ONJFKZOCDSNK', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('554', '111', '6', '6', '111', '1', '2', 'XXQSFDCCFLZE', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('555', '111', '6', '6', '111', '1', '2', 'VQVEFCPLNDWC', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('556', '111', '6', '6', '111', '1', '0', 'YCBUZJDZNJEZ', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('557', '111', '6', '6', '111', '1', '0', 'NZOYTZLLOCDM', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('558', '111', '6', '6', '111', '1', '1', 'CWXNBJIOKEYD', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('559', '111', '6', '6', '111', '1', '1', 'RAYQTGMPGRLP', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('560', '111', '6', '6', '111', '1', '2', 'IBEHVOMRTPVW', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('561', '111', '6', '6', '111', '1', '2', 'IUTXLETQMBOI', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('562', '111', '6', '6', '111', '1', '2', 'DZDAAWZEBJDQ', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('563', '111', '6', '6', '111', '1', '2', 'NBLJBOQFQSWF', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('564', '111', '6', '6', '111', '1', '2', 'XXTVIVCGBAOW', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('565', '111', '6', '6', '111', '1', '2', 'NOEPPCGSBSIR', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('566', '111', '6', '6', '111', '1', '2', 'TPHSEKUMEEWF', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('567', '111', '6', '6', '111', '1', '2', 'RKEIGCPTURXF', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('568', '111', '6', '6', '111', '1', '2', 'TTNUEARNOGKI', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('569', '111', '6', '6', '111', '1', '0', 'QPUMRZPMJMMQ', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('570', '111', '6', '6', '111', '1', '0', 'RCFLYBPGPHQV', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('571', '111', '6', '6', '111', '1', '1', 'FNMDNPLZBVLV', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('572', '111', '6', '6', '111', '1', '1', 'VWTLGSZXSQHU', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('573', '111', '6', '6', '111', '1', '2', 'EAWHOIOEUCBY', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('574', '111', '6', '6', '111', '1', '2', 'FZHERAZFMSMO', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('575', '111', '6', '6', '111', '1', '2', 'HPFOXEFOYKZX', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('576', '111', '6', '6', '111', '1', '2', 'BLSTFIWATJKX', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('577', '111', '6', '6', '111', '1', '2', 'CPTRDYGBFLRA', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('578', '111', '6', '6', '111', '1', '2', 'QUUSYSYGEHGM', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('579', '111', '6', '6', '111', '1', '2', 'RCCHZHRDYQJF', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('580', '111', '6', '6', '111', '1', '2', 'STDTXSCKQMRW', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('581', '111', '6', '6', '111', '1', '2', 'CYSMHTRAHRYZ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('582', '111', '6', '6', '111', '1', '2', 'VGWCYNRDGNXE', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('583', '111', '6', '6', '111', '1', '2', 'YYCNJUIVJSNP', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('584', '111', '6', '6', '111', '1', '2', 'POQVHPCZCKFK', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('585', '111', '6', '6', '111', '1', '2', 'DJQMXXUVMZNH', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('586', '111', '6', '6', '111', '1', '2', 'WMNOPPCAZHPA', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('587', '111', '6', '6', '111', '1', '2', 'NDEYNGXBAWTK', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('588', '111', '6', '6', '111', '1', '2', 'AFNQJTDQXRMS', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('589', '111', '6', '6', '111', '1', '2', 'MLTNGWAXLOME', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('590', '111', '6', '6', '111', '1', '2', 'YBSRRZQMYPXW', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('591', '111', '6', '6', '111', '1', '2', 'GBQTOLFKWZJQ', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('592', '111', '6', '6', '111', '1', '2', 'ULPFECWQSSPB', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('593', '111', '6', '6', '111', '1', '2', 'ZDKWGISTUXYL', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('594', '111', '6', '6', '111', '1', '2', 'GURIQYTJTAYM', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('595', '111', '6', '6', '111', '1', '2', 'LWIQRKEGCSWV', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('596', '111', '6', '6', '111', '1', '2', 'KIRLJFOVBAFK', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('597', '111', '6', '6', '111', '1', '2', 'YNZHEOJXAYDD', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('598', '111', '6', '6', '111', '1', '2', 'UCGPFWFKDAFV', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('599', '111', '6', '6', '111', '1', '2', 'DKRUUMNOFSNY', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('600', '111', '6', '6', '111', '1', '2', 'MCVOFBMJZXOZ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('601', '111', '6', '6', '111', '1', '2', 'IXSDEKKRYYOE', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('602', '111', '6', '6', '111', '1', '2', 'BOHOQHPNTSIE', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('603', '111', '6', '6', '111', '1', '2', 'FFNQKTHWVMCG', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('604', '111', '6', '6', '111', '1', '2', 'MHXYCOOZBHZN', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('605', '111', '6', '6', '111', '1', '2', 'SVSAOVRWZNGG', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('606', '111', '6', '6', '111', '1', '2', 'PDJLIZUZSLVB', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('607', '111', '6', '6', '111', '1', '2', 'FPBQPLHVTPWR', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('608', '111', '6', '6', '111', '1', '2', 'DSDMATMYBUZS', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('609', '111', '6', '6', '111', '1', '2', 'PVNGXUFNKWTR', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('610', '111', '6', '6', '111', '1', '2', 'NJPURJLULZGL', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('611', '111', '6', '6', '111', '1', '2', 'XJQNVOLKSVLY', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('612', '111', '6', '6', '111', '1', '2', 'QIILXSZITDNB', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('613', '111', '6', '6', '111', '1', '2', 'HHNXUCUKPRIT', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('614', '111', '6', '6', '111', '1', '2', 'TBLNOSDNWRMM', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('615', '111', '6', '6', '111', '1', '2', 'JPXWJGBUKLPX', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('616', '111', '6', '6', '111', '1', '2', 'HGJNMEJBAAFX', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('617', '111', '6', '6', '111', '1', '2', 'AAFHSWNOKVJL', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('618', '111', '6', '6', '111', '1', '2', 'CAMIWCGYXDCB', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('619', '111', '6', '6', '111', '1', '2', 'MHKEQMCYWYJQ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('620', '111', '6', '6', '111', '1', '2', 'VPQAOJPGCTOU', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('621', '111', '6', '6', '111', '1', '2', 'APUPVRFZFCVD', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('622', '111', '6', '6', '111', '1', '2', 'QVUEQLJGRHNB', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('623', '111', '6', '6', '111', '1', '2', 'LPSASIWRTHEO', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('624', '111', '6', '6', '111', '1', '2', 'BQHAKZHJYMZP', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('625', '111', '6', '6', '111', '1', '2', 'HCIXGXMAIEZC', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('626', '111', '6', '6', '111', '1', '2', 'HLQPBPRVWFLX', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('627', '111', '6', '6', '111', '1', '2', 'GBHZTCPGEPYH', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('628', '111', '6', '6', '111', '1', '2', 'VZUHEPCWXNSO', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('629', '111', '6', '6', '111', '1', '2', 'OHTHRRYXIGPS', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('630', '111', '6', '6', '111', '1', '2', 'QTRVLNLRJEZJ', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('631', '111', '6', '6', '111', '1', '2', 'TRNJBIFYJTHU', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('632', '111', '6', '6', '111', '1', '2', 'BPNKPUPLYVYG', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('633', '111', '6', '6', '111', '1', '2', 'FTKLMZAWQPTQ', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('634', '111', '6', '6', '111', '1', '2', 'RCIIEAIBMOPH', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('635', '111', '6', '6', '111', '1', '2', 'KANOCPDADSWG', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('636', '111', '6', '6', '111', '1', '2', 'XKMNLMQVWZDO', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('637', '111', '6', '6', '111', '1', '2', 'SEBAXJRDFXDH', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('638', '111', '6', '6', '111', '1', '2', 'GGXYFSPPKTQQ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('639', '111', '6', '6', '111', '1', '2', 'FEZCHMFQVJIB', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('640', '111', '6', '6', '111', '1', '2', 'QMRMZIIGDENA', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('641', '111', '6', '6', '111', '1', '2', 'TFSEAEGAAOJB', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('642', '111', '6', '6', '111', '1', '2', 'CVYUFZZETWBG', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('643', '111', '6', '6', '111', '1', '2', 'SVHGAOZMUNKZ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('644', '111', '6', '6', '111', '1', '2', 'USWEMADIXMBG', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('645', '111', '6', '6', '111', '1', '2', 'USFIJJWQURZN', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('646', '111', '6', '6', '111', '1', '2', 'DECWCCTAFJPZ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('647', '111', '6', '6', '111', '1', '2', 'ZYZTEIZIBQUI', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('648', '111', '6', '6', '111', '1', '2', 'JADISXXEISMT', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('649', '111', '6', '6', '111', '1', '2', 'HWXQCPOTLCXJ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('650', '111', '6', '6', '111', '1', '2', 'WPTKGWQDDCSZ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('651', '111', '6', '6', '111', '1', '2', 'YKVNEXQIGGEF', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('652', '111', '6', '6', '111', '1', '2', 'HJIKFGDAVRQE', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('653', '111', '6', '6', '111', '1', '2', 'EMVNFRYNMGTX', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('654', '111', '6', '6', '111', '1', '2', 'DPZYHJUPAKNH', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('655', '111', '6', '6', '111', '1', '2', 'XQKXPFXAKYWK', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('656', '111', '6', '6', '111', '1', '2', 'JEXXNRLIREPW', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('657', '111', '6', '6', '111', '1', '2', 'TESNNNZTRGXR', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('658', '111', '6', '6', '111', '1', '2', 'AOMTNUVNMEVJ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('659', '111', '6', '6', '111', '1', '2', 'GMKGIJPJJHGN', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('660', '111', '6', '6', '111', '1', '2', 'ACHFREONBYFT', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('661', '111', '6', '6', '111', '1', '2', 'ZXCFMEAATPUR', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('662', '111', '6', '6', '111', '1', '2', 'CTFUOQCAOOJT', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('663', '111', '6', '6', '111', '1', '2', 'DAYQLYBTJNBG', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('664', '111', '6', '6', '111', '1', '2', 'RLXGLHLVATYM', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('665', '111', '6', '6', '111', '1', '2', 'MJODWOTPEQWL', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('666', '111', '6', '6', '111', '1', '2', 'DEVIYKVBQSDQ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('667', '111', '6', '6', '111', '1', '2', 'NUKSMJCRSCWR', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('668', '111', '6', '6', '111', '1', '2', 'XWBUHRUOZKRO', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('669', '111', '6', '6', '111', '1', '2', 'VWJSGVQXPZFP', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('670', '111', '6', '6', '111', '1', '2', 'MNODDIJBUVAF', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('671', '111', '6', '6', '111', '1', '2', 'IYXOLIJUAAMN', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('672', '111', '6', '6', '111', '1', '2', 'HYIPEDDURTRE', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('673', '111', '6', '6', '111', '1', '2', 'JUQINMHYUGTG', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('674', '111', '6', '6', '111', '1', '2', 'FJRUAVVTTQIW', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('675', '111', '6', '6', '111', '1', '2', 'VBMTWSOOSSSO', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('676', '111', '6', '6', '111', '1', '2', 'LXYZUUGRCEBU', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('677', '111', '6', '6', '111', '1', '2', 'DHMDAPRUBISP', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('678', '111', '6', '6', '111', '1', '2', 'UHRGSNPSZENV', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('679', '111', '6', '6', '111', '1', '2', 'OZRWKJHWMCAK', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('680', '111', '6', '6', '111', '1', '2', 'KNIETHXYZNJQ', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('681', '111', '6', '6', '111', '1', '2', 'ATOCKCHQTXRC', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('682', '111', '6', '6', '111', '1', '2', 'QUFMLQCRLSHZ', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('683', '111', '6', '6', '111', '1', '2', 'NVOZNYZISRKN', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('684', '111', '6', '6', '111', '1', '2', 'KFFBWEYYAGOO', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('685', '111', '6', '6', '111', '1', '2', 'DMXVMXWECFHH', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('686', '111', '6', '6', '111', '1', '2', 'WYDHSBXMUMPO', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('687', '111', '6', '6', '111', '1', '2', 'DYSIJKSOMQXK', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('688', '111', '6', '6', '111', '1', '2', 'AXRFHCNFHWZY', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('689', '111', '6', '6', '111', '1', '1', 'GZVTYKCFTYNX', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('690', '111', '6', '6', '111', '1', '2', 'MJPFXOFCYHFQ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('691', '111', '6', '6', '111', '1', '2', 'CNVWEXGLWJQB', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('692', '111', '6', '6', '111', '1', '2', 'JLGQFJYAOGXJ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('693', '111', '6', '6', '111', '1', '2', 'XMYVVJUJXGKE', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('694', '111', '6', '6', '111', '1', '2', 'NXFYKXLVPKGB', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('695', '111', '6', '6', '111', '1', '2', 'MDWFVPGZSAHG', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('696', '111', '6', '6', '111', '1', '2', 'QYAXZMXBDYAT', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('697', '111', '6', '6', '111', '1', '2', 'FVKPBVFBQYHV', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('698', '111', '6', '6', '111', '1', '2', 'FNAFDUBTLLLL', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('699', '111', '6', '6', '111', '1', '0', 'BXDAXPHVPRRH', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('700', '111', '6', '6', '111', '1', '0', 'ZBKJHYKJWUDG', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('701', '111', '6', '6', '111', '1', '1', 'PBXLWMLTXOPC', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('702', '111', '6', '6', '111', '1', '1', 'ZSSXFKYEZMZX', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('703', '111', '6', '6', '111', '1', '2', 'MEBIGHJUIJQF', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('704', '111', '6', '6', '111', '1', '2', 'AKEXJZVYFBPC', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('705', '111', '6', '6', '111', '1', '2', 'IAEFDMTITAGR', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('706', '111', '6', '6', '111', '1', '2', 'RUZAJKYWGZZO', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('707', '111', '6', '6', '111', '1', '2', 'CIWYEDEQOKVO', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('708', '111', '6', '6', '111', '1', '2', 'YHZZGLOPRLUR', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('709', '111', '6', '6', '111', '1', '2', 'MWGDBWRRXRSA', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('710', '111', '6', '6', '111', '1', '2', 'WPBVVJQRWQBH', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('711', '111', '6', '6', '111', '1', '2', 'VCZTTRVQJYGG', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('712', '111', '6', '6', '111', '1', '0', 'INVUMQKTMOET', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('713', '111', '6', '6', '111', '1', '0', 'EVKCCWTGSXPY', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('714', '111', '6', '6', '111', '1', '1', 'EEOZSCVEHIZW', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('715', '111', '6', '6', '111', '1', '1', 'LQDVWOISOULT', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('716', '111', '6', '6', '111', '1', '2', 'IKZYZVIXPUDG', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('717', '111', '6', '6', '111', '1', '2', 'XFVVNCMOZBPF', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('718', '111', '6', '6', '111', '1', '2', 'DKVJBFTAZRXR', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('719', '111', '6', '6', '111', '1', '2', 'PCSLRVCBGSKR', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('720', '111', '6', '6', '111', '1', '2', 'SJZDJJFCVPPB', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('721', '111', '6', '6', '111', '1', '2', 'MKFBYNDCJLDZ', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('722', '111', '6', '6', '111', '1', '2', 'LIFUPBCVLUIW', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('723', '111', '6', '6', '111', '1', '2', 'HGCCDPYUYCHV', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('724', '111', '6', '6', '111', '1', '2', 'FWXDQDRZSSQL', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('725', '111', '6', '6', '111', '1', '0', 'CZSHUDUAUECF', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('726', '111', '6', '6', '111', '1', '0', 'HONKFIRZGGWR', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('727', '111', '6', '6', '111', '1', '1', 'DPIWKHBQCOSE', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('728', '111', '6', '6', '111', '1', '1', 'IHFXOGSQEQVX', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('729', '111', '4', '11', '111', '1', '3', 'NVMCEVHKSOUS', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('730', '111', '4', '11', '111', '1', '3', 'TPMYKLXSHZIY', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('731', '111', '4', '12', '111', '1', '2', 'VLRIVUBFHQHP', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('732', '111', '4', '11', '111', '1', '2', 'XWTFEKZIALLL', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('733', '111', '4', '10', '111', '1', '2', 'ZUUVQHPSSEFJ', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('734', '111', '4', '11', '111', '1', '2', 'GOGMKGASNHDR', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('735', '111', '4', '10', '111', '1', '2', 'FZQIZTIPWWJS', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('736', '111', '4', '11', '111', '1', '2', 'ECHWOEUBESXW', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('737', '111', '4', '12', '111', '1', '2', 'UXNMVCLPTMCC', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('738', '111', '4', '11', '111', '1', '0', 'REEEVJOKUUGY', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('739', '111', '4', '12', '111', '1', '0', 'PSNPLJSELTWI', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('740', '111', '4', '12', '111', '1', '1', 'QUFVRXXNWEKH', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('741', '111', '4', '12', '111', '1', '1', 'CAJHZTBASXGA', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('742', '111', '4', '10', '111', '1', '2', 'DLMRSPFMBJRB', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('743', '111', '4', '11', '111', '1', '2', 'NUPBHQIRCVYE', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('744', '111', '4', '11', '111', '1', '2', 'JUGFDSGMSYZU', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('745', '111', '4', '11', '111', '1', '2', 'RQMHOQFNPKFB', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('746', '111', '4', '11', '111', '1', '2', 'NZCWWUWGSIAC', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('747', '111', '4', '12', '111', '1', '2', 'CASMENBUTNKD', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('748', '111', '4', '10', '111', '1', '2', 'CRWWYZKZZMYW', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('749', '111', '4', '10', '111', '1', '2', 'MZKISZCAPPWD', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('750', '111', '4', '10', '111', '1', '2', 'EHTBHMCLOGRV', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('751', '111', '4', '10', '111', '1', '0', 'WEPVVUNGCXTN', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('752', '111', '4', '10', '111', '1', '0', 'WOYWMLEUQYCL', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('753', '111', '4', '11', '111', '1', '1', 'YEOCPYNASYMW', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('754', '111', '4', '12', '111', '1', '1', 'ZWWXMPDENNZV', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('755', '111', '4', '12', '111', '1', '2', 'MXDGUXFHKNSL', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('756', '111', '4', '12', '111', '1', '2', 'BCSHROJBJXSM', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('757', '111', '4', '11', '111', '1', '2', 'IDKKFKZTEAZG', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('758', '111', '4', '12', '111', '1', '2', 'DVSHIDYRVQRM', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('759', '111', '4', '10', '111', '1', '2', 'GEAVSUAAXIYQ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('760', '111', '4', '11', '111', '1', '2', 'NTNNILOKDXYV', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('761', '111', '4', '11', '111', '1', '2', 'PVNFRZAJLOFP', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('762', '111', '4', '12', '111', '1', '2', 'AWRLLIBYXXUJ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('763', '111', '4', '12', '111', '1', '2', 'JBREOVANTEWQ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('764', '111', '4', '11', '111', '1', '2', 'CLWHLDLLPPRA', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('765', '111', '4', '10', '111', '1', '2', 'EEBXFRJQHXWJ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('766', '111', '4', '12', '111', '1', '2', 'PLUHNKEXIYIF', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('767', '111', '4', '10', '111', '1', '2', 'JZFNTRRQZEXT', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('768', '111', '4', '12', '111', '1', '2', 'VMINWZKGYLGF', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('769', '111', '4', '10', '111', '1', '2', 'HFOTJHURPOGF', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('770', '111', '4', '11', '111', '1', '2', 'SXBBKIDHFOQR', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('771', '111', '4', '10', '111', '1', '2', 'FLUTZCMIZLMX', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('772', '111', '4', '11', '111', '1', '2', 'QOTJTPULJATR', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('773', '111', '4', '10', '111', '1', '2', 'NKGXAMTTPTHR', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('774', '111', '4', '11', '111', '1', '2', 'KKDJQRWMIEYH', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('775', '111', '4', '12', '111', '1', '2', 'LBNFHJZAYCKE', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('776', '111', '4', '12', '111', '1', '2', 'PWNNAYCYLLEW', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('777', '111', '4', '10', '111', '1', '2', 'LUUDLPQEQQAP', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('778', '111', '4', '12', '111', '1', '2', 'OPDKXVPMCENV', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('779', '111', '4', '12', '111', '1', '2', 'JIGPBQKDOZKS', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('780', '111', '4', '11', '111', '1', '2', 'BFTRRFVRDNYE', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('781', '111', '4', '11', '111', '1', '2', 'MUGFMMVTMINF', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('782', '111', '4', '12', '111', '1', '2', 'HFBVWFIOPVMD', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('783', '111', '4', '10', '111', '1', '2', 'NMNBADQWQMKG', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('784', '111', '4', '11', '111', '1', '2', 'VVKUJSEVBKVC', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('785', '111', '4', '11', '111', '1', '2', 'BDJYPWMHCBJA', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('786', '111', '4', '11', '111', '1', '2', 'YJBXAIAWBVCV', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('787', '111', '4', '12', '111', '1', '2', 'GUPWEHNPFGMM', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('788', '111', '4', '12', '111', '1', '2', 'UFTLHETDCNFJ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('789', '111', '4', '10', '111', '1', '2', 'BDAQOJIRWJUM', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('790', '111', '4', '10', '111', '1', '2', 'DYXQFPFZZUNE', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('791', '111', '4', '10', '111', '1', '2', 'XZZZNSDMSDNI', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('792', '111', '4', '11', '111', '1', '2', 'HSLUKKFNGXFX', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('793', '111', '4', '10', '111', '1', '2', 'GTGMLSXOGWQQ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('794', '111', '4', '10', '111', '1', '2', 'LNGJGLJYTERL', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('795', '111', '4', '10', '111', '1', '2', 'PFWAIERQLPNN', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('796', '111', '4', '11', '111', '1', '2', 'UHPPHYWORJXF', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('797', '111', '4', '12', '111', '1', '2', 'BTKRHCRLVMFH', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('798', '111', '4', '12', '111', '1', '2', 'QEAYAHDQENKR', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('799', '111', '4', '11', '111', '1', '2', 'BZWBKUVUYLHV', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('800', '111', '4', '12', '111', '1', '2', 'YRNJVBPRKPID', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('801', '111', '4', '12', '111', '1', '2', 'WGCORJEGOUMN', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('802', '111', '4', '12', '111', '1', '2', 'ZPMXDMBSIBWO', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('803', '111', '4', '11', '111', '1', '2', 'ZYEGHLSQLCQZ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('804', '111', '4', '11', '111', '1', '2', 'EHDKSGZVUTAT', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('805', '111', '4', '11', '111', '1', '2', 'WNNOJUECAMDU', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('806', '111', '4', '10', '111', '1', '2', 'UCKQJTOLRMBM', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('807', '111', '4', '12', '111', '1', '2', 'FSOPBETCLKRO', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('808', '111', '4', '12', '111', '1', '2', 'JBBKGQGWRPHV', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('809', '111', '4', '12', '111', '1', '2', 'LVDUAHUPIRWL', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('810', '111', '4', '10', '111', '1', '2', 'MWHYOGEBTHKE', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('811', '111', '4', '12', '111', '1', '2', 'NYOKXCJTRTZS', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('812', '111', '4', '12', '111', '1', '2', 'OBNEZINSXFCR', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('813', '111', '4', '11', '111', '1', '2', 'WOWNSWBLSEPR', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('814', '111', '4', '11', '111', '1', '2', 'KKJYKESTPXSG', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('815', '111', '4', '11', '111', '1', '2', 'UTNPONYTQCRH', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('816', '111', '4', '11', '111', '1', '2', 'UWGDSPPCGQIE', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('817', '111', '4', '10', '111', '1', '2', 'CKQBFWKKNDKH', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('818', '111', '4', '10', '111', '1', '2', 'ATCTKMUNFCPK', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('819', '111', '4', '11', '111', '1', '2', 'HMMJSWJTSPOL', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('820', '111', '4', '11', '111', '1', '2', 'ATQEAWOXNQNP', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('821', '111', '4', '11', '111', '1', '2', 'FPEOJFOVLFFI', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('822', '111', '4', '10', '111', '1', '2', 'DEVFTLQCRXDA', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('823', '111', '4', '11', '111', '1', '2', 'ILFQUJXDNUPA', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('824', '111', '4', '10', '111', '1', '2', 'TUQKXGDEYFZU', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('825', '111', '4', '12', '111', '1', '2', 'HSYETPJWFIIQ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('826', '111', '4', '11', '111', '1', '2', 'NFQDGXBFRFFH', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('827', '111', '4', '12', '111', '1', '2', 'AMCICDSEKONP', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('828', '111', '4', '10', '111', '1', '2', 'VHJUHEGSGLYS', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('829', '111', '4', '10', '111', '1', '2', 'SNVYDMWGAVMY', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('830', '111', '4', '10', '111', '1', '2', 'YNRNOUBHPKLZ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('831', '111', '4', '12', '111', '1', '2', 'OYURGSYTIVDZ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('832', '111', '4', '12', '111', '1', '2', 'JIUQENDSGUPE', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('833', '111', '4', '10', '111', '1', '2', 'JENXBDQEYWYL', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('834', '111', '4', '10', '111', '1', '2', 'ODDWDIKGNNWN', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('835', '111', '4', '11', '111', '1', '2', 'DVDZJGOYNFJL', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('836', '111', '4', '12', '111', '1', '2', 'XMMEIWNGZUZN', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('837', '111', '4', '11', '111', '1', '2', 'SQADYHLVGVWV', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('838', '111', '4', '12', '111', '1', '2', 'VDPCZWHSPOBX', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('839', '111', '4', '12', '111', '1', '2', 'FUSOCOXJZPYY', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('840', '111', '4', '11', '111', '1', '2', 'KCWUDAFULQMV', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('841', '111', '4', '11', '111', '1', '2', 'TFVOLIIPFSRC', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('842', '111', '4', '12', '111', '1', '2', 'XPQVCZHQKGKS', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('843', '111', '4', '10', '111', '1', '2', 'AIJPRBSZPLWO', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('844', '111', '4', '11', '111', '1', '2', 'XDAUHUBNPYGC', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('845', '111', '4', '12', '111', '1', '2', 'CVEOZIZIBJJJ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('846', '111', '4', '10', '111', '1', '2', 'HIGFPAWXFXEK', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('847', '111', '4', '12', '111', '1', '2', 'BSLMMYOJZLAC', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('848', '111', '4', '10', '111', '1', '2', 'JUEPXPKRKZSF', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('849', '111', '4', '11', '111', '1', '2', 'KOJMTDOLIVSF', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('850', '111', '4', '11', '111', '1', '2', 'WIWTZMZHECTY', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('851', '111', '4', '12', '111', '1', '2', 'MUVLDCUBUNGJ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('852', '111', '4', '11', '111', '1', '2', 'GOENMPKRVYYF', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('853', '111', '4', '11', '111', '1', '2', 'HEYNTHKLKYYH', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('854', '111', '4', '12', '111', '1', '2', 'FHBWZDLPQXIF', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('855', '111', '4', '10', '111', '1', '2', 'FHQMBMLKUHZD', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('856', '111', '4', '12', '111', '1', '2', 'UGPYTDZBTFCB', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('857', '111', '4', '12', '111', '1', '2', 'GZEYDENPJPWA', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('858', '111', '4', '11', '111', '1', '2', 'XVXYDDQXZZLC', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('859', '111', '4', '11', '111', '1', '2', 'TTJZYGOISLKI', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('860', '111', '4', '12', '111', '1', '2', 'PNTTUFOCESID', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('861', '111', '4', '10', '111', '1', '2', 'IDNEIQUILNID', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('862', '111', '4', '12', '111', '1', '2', 'WXKKICNFUYEL', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('863', '111', '4', '10', '111', '1', '2', 'XBQXQCHAAYXT', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('864', '111', '4', '10', '111', '1', '2', 'RJAGRNBYCZIW', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('865', '111', '4', '12', '111', '1', '2', 'KVUMYBGMSGXS', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('866', '111', '4', '12', '111', '1', '2', 'NTDTESSPUHEM', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('867', '111', '4', '11', '111', '1', '2', 'ZJWZPTJINEBQ', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('868', '111', '4', '11', '111', '1', '2', 'WQKWFZDNLBMY', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('869', '111', '4', '10', '111', '1', '2', 'KWRWDAKUZFRB', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('870', '111', '4', '10', '111', '1', '2', 'QXJAORHAQLBX', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('871', '111', '4', '11', '111', '1', '1', 'SZJQJDOMBTUX', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('872', '111', '4', '11', '111', '1', '2', 'SXELZGNPBRDX', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('873', '111', '4', '10', '111', '1', '2', 'JCHEDJJEMZWO', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('874', '111', '4', '10', '111', '1', '2', 'WQNAXIYHOSGH', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('875', '111', '4', '10', '111', '1', '2', 'BKSOSWWXFTJF', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('876', '111', '4', '12', '111', '1', '2', 'MIPYJEJHYGGL', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('877', '111', '4', '10', '111', '1', '2', 'DXLHZSVVSFKK', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('878', '111', '4', '11', '111', '1', '2', 'RWYZZTMOZHRX', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('879', '111', '4', '10', '111', '1', '2', 'PFQUMFTXQOZR', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('880', '111', '4', '11', '111', '1', '2', 'JOGUECOZJUMZ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('881', '111', '4', '12', '111', '1', '0', 'HYCSVXSACJHC', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('882', '111', '4', '11', '111', '1', '0', 'ERZERKYDNVFA', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('883', '111', '4', '12', '111', '1', '1', 'PDJXEZDLOHEN', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('884', '111', '4', '11', '111', '1', '1', 'TZOYYFJZCVXA', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('885', '111', '4', '12', '111', '1', '2', 'XZJSFGSIVFQH', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('886', '111', '4', '10', '111', '1', '2', 'SZLWSVBACMUT', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('887', '111', '4', '11', '111', '1', '2', 'NBKHIRPYJDMC', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('888', '111', '4', '10', '111', '1', '2', 'KNGOQZHOQBDS', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('889', '111', '4', '10', '111', '1', '2', 'FEVEYDKQQUXR', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('890', '111', '4', '11', '111', '1', '2', 'PXNISNRQIBQK', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('891', '111', '4', '12', '111', '1', '2', 'ZCIWWTUYQGZR', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('892', '111', '4', '11', '111', '1', '2', 'NAUVZJPRFNTO', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('893', '111', '4', '11', '111', '1', '2', 'ZGBBHJUXPXNJ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('894', '111', '4', '10', '111', '1', '0', 'TVHUUPBGCYEH', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('895', '111', '4', '11', '111', '1', '0', 'IXZLRLNIXUFJ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('896', '111', '4', '10', '111', '1', '1', 'ABAVWVQZQIQQ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('897', '111', '4', '10', '111', '1', '1', 'GIWVURXZFOUP', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('898', '111', '4', '10', '111', '1', '2', 'BBQVYKBKNEEW', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('899', '111', '4', '12', '111', '1', '2', 'DCICASNTEBAV', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('900', '111', '4', '12', '111', '1', '2', 'HRWIMDXEIBJE', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('901', '111', '4', '12', '111', '1', '2', 'SOVOTEKENITI', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('902', '111', '4', '12', '111', '1', '2', 'CJELOUPEWDQY', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('903', '111', '4', '11', '111', '1', '2', 'FXIOXYGIBPQH', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('904', '111', '4', '10', '111', '1', '2', 'OFNHUYUSGFZX', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('905', '111', '4', '11', '111', '1', '2', 'ANWTIHYHMDMA', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('906', '111', '4', '12', '111', '1', '2', 'QCNOIVMCEPRJ', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('907', '111', '4', '12', '111', '1', '0', 'KRUFAXDGRAJW', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('908', '111', '4', '10', '111', '1', '0', 'HSVFJPWGEGDD', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('909', '111', '4', '11', '111', '1', '1', 'RXWUXCOQKQRN', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('910', '111', '4', '11', '111', '1', '1', 'BSESIPHMBYFT', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('911', '111', '7', '15', '111', '1', '3', 'TGADQRVGUKTB', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('912', '111', '7', '15', '111', '1', '3', 'DMWGAWNOYFZQ', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('913', '111', '7', '13', '111', '1', '2', 'FRVWIAYAHVRF', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('914', '111', '7', '14', '111', '1', '2', 'MDNECAWRAPVC', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('915', '111', '7', '14', '111', '1', '2', 'ZYLZTERSLJVZ', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('916', '111', '7', '14', '111', '1', '2', 'DTULWMNEGQTR', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('917', '111', '7', '13', '111', '1', '2', 'MQBBZNMJXBAV', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('918', '111', '7', '15', '111', '1', '2', 'BJFORSCZMXJX', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('919', '111', '7', '14', '111', '1', '2', 'HLEVYGRUIDAN', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('920', '111', '7', '14', '111', '1', '0', 'ISGZAHPVUXLN', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('921', '111', '7', '14', '111', '1', '0', 'JFDNRTCYNKQA', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('922', '111', '7', '13', '111', '1', '1', 'JLEISPIEFSHI', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('923', '111', '7', '13', '111', '1', '1', 'NEUHOIREQTHN', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('924', '111', '7', '15', '111', '1', '2', 'VQWZQQPYSEGJ', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('925', '111', '7', '14', '111', '1', '2', 'JCJWIBITMBDT', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('926', '111', '7', '14', '111', '1', '2', 'UWGXMQFIBCLU', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('927', '111', '7', '13', '111', '1', '2', 'AQSTXUQWUXED', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('928', '111', '7', '15', '111', '1', '2', 'JVSOVGHJNMSE', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('929', '111', '7', '15', '111', '1', '2', 'GTHVTJTEUEXT', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('930', '111', '7', '13', '111', '1', '2', 'CCRCRIOTWDND', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('931', '111', '7', '15', '111', '1', '2', 'QKNKZSVSVBCM', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('932', '111', '7', '13', '111', '1', '2', 'ZFQEKMNEYVSJ', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('933', '111', '7', '14', '111', '1', '0', 'WVIWXIVHGMPZ', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('934', '111', '7', '13', '111', '1', '0', 'FUIWQLJBZOAG', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('935', '111', '7', '15', '111', '1', '1', 'NJGIBCVIIFCM', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('936', '111', '7', '14', '111', '1', '1', 'NHGMJIYAPEIU', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('937', '111', '7', '15', '111', '1', '2', 'UKIAADJJMHCT', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('938', '111', '7', '15', '111', '1', '2', 'PNHLJBKFHUZH', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('939', '111', '7', '13', '111', '1', '2', 'SAHPXDMAIVPN', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('940', '111', '7', '15', '111', '1', '2', 'IPCXURAOWMPM', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('941', '111', '7', '14', '111', '1', '2', 'WHNVVQYBVYST', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('942', '111', '7', '13', '111', '1', '2', 'ASVCVYBCDKPL', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('943', '111', '7', '14', '111', '1', '2', 'GTQLZQNHSZYH', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('944', '111', '7', '14', '111', '1', '2', 'ZVLNTQDEIFWT', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('945', '111', '7', '13', '111', '1', '2', 'TGTOLJFWBJAR', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('946', '111', '7', '15', '111', '1', '2', 'LBYHRNFJGCBP', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('947', '111', '7', '14', '111', '1', '2', 'RFGQUKZJZTYC', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('948', '111', '7', '15', '111', '1', '2', 'IFZTBJGHAYFA', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('949', '111', '7', '15', '111', '1', '2', 'JQLJRQPFJDEM', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('950', '111', '7', '14', '111', '1', '2', 'FJMOTLHGJQTV', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('951', '111', '7', '15', '111', '1', '2', 'SXIQMCYFZGZQ', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('952', '111', '7', '13', '111', '1', '2', 'FCRACITNUMBX', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('953', '111', '7', '15', '111', '1', '2', 'GORAMQIEXCEV', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('954', '111', '7', '15', '111', '1', '2', 'CPDMKTUCPSVO', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('955', '111', '7', '14', '111', '1', '2', 'RESFKCGUMQRG', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('956', '111', '7', '13', '111', '1', '2', 'HNKIUGWHGGPW', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('957', '111', '7', '14', '111', '1', '2', 'DFMXFDFMINXW', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('958', '111', '7', '13', '111', '1', '2', 'LHBVEIPYLJKH', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('959', '111', '7', '14', '111', '1', '2', 'HDGMAETZUQGI', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('960', '111', '7', '14', '111', '1', '2', 'IEHOISFSPRKH', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('961', '111', '7', '15', '111', '1', '2', 'UZCQELFCMSRV', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('962', '111', '7', '15', '111', '1', '2', 'CLTQEXJCYBCQ', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('963', '111', '7', '15', '111', '1', '2', 'LREKCYPSPQVI', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('964', '111', '7', '13', '111', '1', '2', 'WGCFNEQJJUYU', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('965', '111', '7', '14', '111', '1', '2', 'HEBPUTFTZZSK', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('966', '111', '7', '15', '111', '1', '2', 'WPMRABIKSOVV', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('967', '111', '7', '13', '111', '1', '2', 'SXXRXDTEBBHP', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('968', '111', '7', '15', '111', '1', '2', 'IJDVXZJNDRJZ', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('969', '111', '7', '15', '111', '1', '2', 'SSKCOWGMNGLC', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('970', '111', '7', '14', '111', '1', '2', 'IUWKNHSXVGWE', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('971', '111', '7', '15', '111', '1', '2', 'BJBMCPHTEZVF', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('972', '111', '7', '13', '111', '1', '2', 'HREFPQRIWZSC', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('973', '111', '7', '13', '111', '1', '2', 'SIQAVMLSTJKU', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('974', '111', '7', '14', '111', '1', '2', 'ZEXSLARAHLAW', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('975', '111', '7', '15', '111', '1', '2', 'DWXWWGNTMZXA', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('976', '111', '7', '14', '111', '1', '2', 'FCHTLOGPBBQO', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('977', '111', '7', '15', '111', '1', '2', 'UBKYDBCYZLBC', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('978', '111', '7', '15', '111', '1', '2', 'FZNDTFNRHMAN', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('979', '111', '7', '13', '111', '1', '2', 'VYGCHAOTFGYJ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('980', '111', '7', '15', '111', '1', '2', 'DELTVCUOTSHT', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('981', '111', '7', '13', '111', '1', '2', 'FCVTAGELPNSW', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('982', '111', '7', '13', '111', '1', '2', 'VRLZCWLPCGUQ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('983', '111', '7', '13', '111', '1', '2', 'PESSCBWYMADH', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('984', '111', '7', '13', '111', '1', '2', 'LSDVGVKKEJGL', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('985', '111', '7', '15', '111', '1', '2', 'EEGNSEOEDUJC', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('986', '111', '7', '15', '111', '1', '2', 'YRGHAHLSOTGI', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('987', '111', '7', '13', '111', '1', '2', 'AOOAJDUDKLWZ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('988', '111', '7', '15', '111', '1', '2', 'OEYZHUDMPJBD', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('989', '111', '7', '14', '111', '1', '2', 'IIGSSQVWZWIX', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('990', '111', '7', '13', '111', '1', '2', 'AZIHXSUTPAGE', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('991', '111', '7', '13', '111', '1', '2', 'EMNYUQNFJICM', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('992', '111', '7', '13', '111', '1', '2', 'XSGYSJOAYGCO', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('993', '111', '7', '15', '111', '1', '2', 'KATLYUYSVCCD', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('994', '111', '7', '14', '111', '1', '2', 'VPNATQKFNOBB', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('995', '111', '7', '15', '111', '1', '2', 'AUUKMYOOJOAU', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('996', '111', '7', '15', '111', '1', '2', 'BEHNHTKSFNEM', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('997', '111', '7', '15', '111', '1', '2', 'TTGAUAKVKRRO', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('998', '111', '7', '15', '111', '1', '2', 'ZRYOCLTSMJSQ', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('999', '111', '7', '13', '111', '1', '2', 'XSGTZMMLWFKO', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1000', '111', '7', '13', '111', '1', '2', 'TYJWVBBGAYWY', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1001', '111', '7', '15', '111', '1', '2', 'RGVMCHKPHJKD', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1002', '111', '7', '15', '111', '1', '2', 'RVOHFQWWLWUL', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1003', '111', '7', '14', '111', '1', '2', 'GALSDXSKAYJY', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1004', '111', '7', '13', '111', '1', '2', 'BQUQVTHCOONQ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1005', '111', '7', '15', '111', '1', '2', 'QFCWEZYGPQWT', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1006', '111', '7', '15', '111', '1', '2', 'IFLSPUVIXUBH', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1007', '111', '7', '13', '111', '1', '2', 'XMKHEONRUVCP', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1008', '111', '7', '13', '111', '1', '2', 'ZJUNXQAJGRZM', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1009', '111', '7', '14', '111', '1', '2', 'PLXBLGBIRGXW', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1010', '111', '7', '14', '111', '1', '2', 'XUWTHZZPXEAQ', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1011', '111', '7', '13', '111', '1', '2', 'OQDLVAOWUZRN', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1012', '111', '7', '15', '111', '1', '2', 'NVJTZUXITZIO', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1013', '111', '7', '14', '111', '1', '2', 'SQOFIYBRYRRH', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1014', '111', '7', '15', '111', '1', '2', 'VVTWABJMNZHX', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1015', '111', '7', '13', '111', '1', '2', 'GDGPFRCZWFCH', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1016', '111', '7', '15', '111', '1', '2', 'WEWVZUVEAUTC', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1017', '111', '7', '13', '111', '1', '2', 'SNDDGMSJJJJT', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1018', '111', '7', '13', '111', '1', '2', 'VSQXRNOBFRAI', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1019', '111', '7', '15', '111', '1', '2', 'FURGAQEAPKYW', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1020', '111', '7', '15', '111', '1', '2', 'TVMNLDIHYSYL', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1021', '111', '7', '15', '111', '1', '2', 'XOMBLCUHCXFZ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1022', '111', '7', '13', '111', '1', '2', 'KWXLLTPQSRRZ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1023', '111', '7', '15', '111', '1', '2', 'JMAHHGEGBTGY', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1024', '111', '7', '13', '111', '1', '2', 'ZMRQQFEHBVLF', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1025', '111', '7', '14', '111', '1', '2', 'MWTTOWQVJORR', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1026', '111', '7', '14', '111', '1', '2', 'TXYJMDNGNZFP', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1027', '111', '7', '14', '111', '1', '2', 'WJXGXIGSYKSZ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1028', '111', '7', '15', '111', '1', '2', 'FAVUIAFUNDJV', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1029', '111', '7', '13', '111', '1', '2', 'ZQWOSYREUALF', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1030', '111', '7', '14', '111', '1', '2', 'NOGBLHZSXPQF', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1031', '111', '7', '15', '111', '1', '2', 'MABCSXJWIFMY', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1032', '111', '7', '14', '111', '1', '2', 'CQDCEGHMNTWT', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1033', '111', '7', '13', '111', '1', '2', 'QEJUVCCSDHSQ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1034', '111', '7', '15', '111', '1', '2', 'OYCOXZYEHKJG', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1035', '111', '7', '13', '111', '1', '2', 'MLRQAJBIKBVU', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1036', '111', '7', '13', '111', '1', '2', 'EOFCPEFBASSF', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1037', '111', '7', '15', '111', '1', '2', 'UKJPRMKTOSQO', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1038', '111', '7', '13', '111', '1', '2', 'YBKVGPOKILIM', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1039', '111', '7', '14', '111', '1', '2', 'ZXRDMQKRCILZ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1040', '111', '7', '13', '111', '1', '2', 'XCGYPFYWXJMW', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1041', '111', '7', '13', '111', '1', '2', 'BCPFQWYKQXKT', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1042', '111', '7', '15', '111', '1', '2', 'BLFZGVYLDBZF', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1043', '111', '7', '14', '111', '1', '2', 'WUVMZDMEIOSM', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1044', '111', '7', '15', '111', '1', '2', 'IMAXDSPTLYLE', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1045', '111', '7', '14', '111', '1', '2', 'CSPDSKQRIIYC', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1046', '111', '7', '15', '111', '1', '2', 'IBGHWXYJNDPH', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1047', '111', '7', '13', '111', '1', '2', 'VCTLZDGAVQCV', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1048', '111', '7', '15', '111', '1', '2', 'IBNSDJOMIPKY', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1049', '111', '7', '13', '111', '1', '2', 'IHYYWRTBNFOI', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1050', '111', '7', '15', '111', '1', '2', 'TWKYVPFPPPAI', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1051', '111', '7', '15', '111', '1', '2', 'EVJUGJXJENTY', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1052', '111', '7', '14', '111', '1', '2', 'RINVDGQBMBAM', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1053', '111', '7', '15', '111', '1', '1', 'PGNXVGTXTQID', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1054', '111', '7', '14', '111', '1', '2', 'BYZEHBHCPDQZ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1055', '111', '7', '13', '111', '1', '2', 'MTSXZGXMOPCM', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1056', '111', '7', '13', '111', '1', '2', 'YATBGTAJPOEW', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1057', '111', '7', '13', '111', '1', '2', 'ZYTSVOEAWISP', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1058', '111', '7', '14', '111', '1', '2', 'CJHULAXVKKVO', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1059', '111', '7', '13', '111', '1', '2', 'HPBVGWLAORBJ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1060', '111', '7', '15', '111', '1', '2', 'VACBJZNPPKIT', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1061', '111', '7', '13', '111', '1', '2', 'JITCBQFQRSNK', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1062', '111', '7', '15', '111', '1', '2', 'CAMPGZALMINH', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1063', '111', '7', '13', '111', '1', '0', 'RFAGQYXILAUJ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1064', '111', '7', '14', '111', '1', '0', 'GRDOUHEKPUGY', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1065', '111', '7', '13', '111', '1', '1', 'HAWEOUTDVLFI', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1066', '111', '7', '13', '111', '1', '1', 'WBRJVINCKPJT', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1067', '111', '7', '15', '111', '1', '2', 'FTTHKRTBZRDQ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1068', '111', '7', '13', '111', '1', '2', 'EOJGJLLFTIFL', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1069', '111', '7', '14', '111', '1', '2', 'IIHWBDAJTXCU', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1070', '111', '7', '15', '111', '1', '2', 'POKTYHEMKXBG', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1071', '111', '7', '15', '111', '1', '2', 'COSIUXTRRHDW', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1072', '111', '7', '13', '111', '1', '2', 'XEUWSIFAZMNZ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1073', '111', '7', '13', '111', '1', '2', 'KTADOSPJDCJD', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1074', '111', '7', '15', '111', '1', '2', 'UJXAXVBCZWND', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1075', '111', '7', '14', '111', '1', '2', 'YPMCFCYMJMGU', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1076', '111', '7', '13', '111', '1', '0', 'YVCXCLKHNEGD', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1077', '111', '7', '15', '111', '1', '0', 'XMWQKQAHUBVX', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1078', '111', '7', '15', '111', '1', '1', 'AITCKKOITIQI', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1079', '111', '7', '15', '111', '1', '1', 'IHEGLCJFKBBW', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1080', '111', '7', '14', '111', '1', '2', 'FOYMLNYLIRBX', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1081', '111', '7', '13', '111', '1', '2', 'GRTPIZBXEWSQ', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1082', '111', '7', '15', '111', '1', '2', 'GOAMKWYLAGLX', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1083', '111', '7', '15', '111', '1', '2', 'ONCXGNFLSPVJ', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1084', '111', '7', '15', '111', '1', '2', 'LVLIMEWQATBX', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1085', '111', '7', '13', '111', '1', '2', 'AYSXAZDLDYQR', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1086', '111', '7', '13', '111', '1', '2', 'PONUSFCRHXTP', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1087', '111', '7', '13', '111', '1', '2', 'DDIANOAXEKLZ', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1088', '111', '7', '13', '111', '1', '2', 'LICPYPGUKMQT', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1089', '111', '7', '13', '111', '1', '0', 'YVGLJDUHZORQ', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1090', '111', '7', '15', '111', '1', '0', 'BRZUFSYOWSWW', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1091', '111', '7', '14', '111', '1', '1', 'XEDWCTBUFGSI', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1092', '111', '7', '13', '111', '1', '1', 'AMNGDBJSAQOT', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1093', '111', '8', '18', '111', '1', '3', 'TZMZFGKYSMTR', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1094', '111', '8', '16', '111', '1', '3', 'SMDNQDRWCILN', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1095', '111', '8', '18', '111', '1', '2', 'DUZSWHEDWNQN', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1096', '111', '8', '16', '111', '1', '2', 'MUPVQNOKQXZB', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1097', '111', '8', '18', '111', '1', '2', 'GEMPMMOIGEVE', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1098', '111', '8', '17', '111', '1', '2', 'MHWJRDWZQGKF', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1099', '111', '8', '18', '111', '1', '2', 'PFYFCHJKVENY', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1100', '111', '8', '16', '111', '1', '2', 'QIPHDXEJEHGP', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1101', '111', '8', '16', '111', '1', '2', 'RUKXIZQBTXNC', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1102', '111', '8', '18', '111', '1', '0', 'OURXAUPRBXKA', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1103', '111', '8', '17', '111', '1', '0', 'NZXGEYPAXYPV', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', null);
INSERT INTO `device_sensor` VALUES ('1104', '111', '8', '16', '111', '1', '1', 'INRORZIIBZEO', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1105', '111', '8', '18', '111', '1', '1', 'QOGFXOGWAWTN', '1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1106', '111', '8', '17', '111', '1', '2', 'RAPIRWALGVCV', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1107', '111', '8', '18', '111', '1', '2', 'SJMJNMSNDMDW', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1108', '111', '8', '16', '111', '1', '2', 'GUBWGESGWXKY', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1109', '111', '8', '17', '111', '1', '2', 'QWZNDNGVLGML', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1110', '111', '8', '16', '111', '1', '2', 'LZCKERTCGZYZ', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1111', '111', '8', '16', '111', '1', '2', 'OKQNMXUFZXEI', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1112', '111', '8', '17', '111', '1', '2', 'YCFPBBDYCZYR', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1113', '111', '8', '17', '111', '1', '2', 'TXWIADEMCNSN', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1114', '111', '8', '16', '111', '1', '2', 'UHHRGTWMSPAT', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1115', '111', '8', '18', '111', '1', '0', 'BYNUGQDBWTKG', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1116', '111', '8', '17', '111', '1', '0', 'PMJCBTYYBRGM', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1117', '111', '8', '18', '111', '1', '1', 'BEZVQFVGVKNV', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1118', '111', '8', '16', '111', '1', '1', 'BJYZFHZRHCLW', '2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1119', '111', '8', '17', '111', '1', '2', 'DBTUVBLPPUXG', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1120', '111', '8', '16', '111', '1', '2', 'IHHXWSBJNIPJ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1121', '111', '8', '18', '111', '1', '2', 'BLQEEZNMEITJ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1122', '111', '8', '18', '111', '1', '2', 'HMQNFKWBFSSY', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1123', '111', '8', '18', '111', '1', '2', 'ONSCVIQWCJBT', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1124', '111', '8', '17', '111', '1', '2', 'TDOFFASNDGRR', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1125', '111', '8', '16', '111', '1', '2', 'FRUZHSPLELFA', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1126', '111', '8', '18', '111', '1', '2', 'SWQKOIHUHCGC', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1127', '111', '8', '16', '111', '1', '2', 'VOIFZATEANSS', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1128', '111', '8', '18', '111', '1', '2', 'FBMLYEELXSEB', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1129', '111', '8', '18', '111', '1', '2', 'WWAGOVEQKBNQ', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1130', '111', '8', '18', '111', '1', '2', 'GJCAPMMZSTSI', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1131', '111', '8', '17', '111', '1', '2', 'JIXRVPQAQADE', '3', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1132', '111', '8', '18', '111', '1', '2', 'CNGRAWFFXYEK', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1133', '111', '8', '18', '111', '1', '2', 'WPTZBNMSKVFT', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1134', '111', '8', '18', '111', '1', '2', 'MBJKKFBQDLPV', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1135', '111', '8', '16', '111', '1', '2', 'TKZCIVAKXDAL', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1136', '111', '8', '16', '111', '1', '2', 'KXKGZIWMSFRO', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1137', '111', '8', '16', '111', '1', '2', 'GYRXPHZTZTRH', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1138', '111', '8', '16', '111', '1', '2', 'BQJEUMIYWZBT', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1139', '111', '8', '16', '111', '1', '2', 'IEKVLDXBICQI', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1140', '111', '8', '16', '111', '1', '2', 'BKJJFHIFDRXN', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1141', '111', '8', '16', '111', '1', '2', 'DNDDPSNTRAOD', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1142', '111', '8', '18', '111', '1', '2', 'WRMGFWLUDVHO', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1143', '111', '8', '18', '111', '1', '2', 'TZCATSENGSPN', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1144', '111', '8', '17', '111', '1', '2', 'ONSYSDHSMJNF', '4', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1145', '111', '8', '18', '111', '1', '2', 'VTLTJKMPGSAR', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1146', '111', '8', '18', '111', '1', '2', 'ZPDZVQOYLARW', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1147', '111', '8', '18', '111', '1', '2', 'PXXJLHEZLJZV', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1148', '111', '8', '17', '111', '1', '2', 'UHAGKXWMJEEU', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1149', '111', '8', '17', '111', '1', '2', 'PAIEXDGZAUIY', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1150', '111', '8', '16', '111', '1', '2', 'BWYKAHVHDLBX', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1151', '111', '8', '17', '111', '1', '2', 'KEXVJRXGFVIA', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1152', '111', '8', '18', '111', '1', '2', 'FIXJUUKQLGFM', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1153', '111', '8', '16', '111', '1', '2', 'CIZKVDNVYANN', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1154', '111', '8', '17', '111', '1', '2', 'BBSQGFBHIIUD', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1155', '111', '8', '18', '111', '1', '2', 'VHBWPPLYXYCH', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1156', '111', '8', '18', '111', '1', '2', 'WEFUYOZETGVW', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1157', '111', '8', '16', '111', '1', '2', 'ZFYVQZSOMUYP', '5', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1158', '111', '8', '16', '111', '1', '2', 'JADKPWQTXLIP', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1159', '111', '8', '16', '111', '1', '2', 'SWWMGGOGIQHX', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1160', '111', '8', '16', '111', '1', '2', 'OAKRAKABHQFM', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1161', '111', '8', '18', '111', '1', '2', 'LISEWZQLNZUB', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1162', '111', '8', '18', '111', '1', '2', 'YHRZHQGDZJIC', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1163', '111', '8', '17', '111', '1', '2', 'UYZNTATGMQNP', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1164', '111', '8', '18', '111', '1', '2', 'THJZVSBTNSYP', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1165', '111', '8', '16', '111', '1', '2', 'PASXIIAEEGKF', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1166', '111', '8', '17', '111', '1', '2', 'LTMISDYLKSAQ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1167', '111', '8', '16', '111', '1', '2', 'TITCONDKPEKZ', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1168', '111', '8', '16', '111', '1', '2', 'EGONYNFSTYRW', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1169', '111', '8', '16', '111', '1', '2', 'VOAGBVVEBXQI', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1170', '111', '8', '16', '111', '1', '2', 'FPKZUOLQKRZO', '6', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1171', '111', '8', '16', '111', '1', '2', 'UZXSCTYRZNWU', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1172', '111', '8', '18', '111', '1', '2', 'UWXRQPYBTQGM', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1173', '111', '8', '18', '111', '1', '2', 'ICJJNLRBCPFP', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1174', '111', '8', '17', '111', '1', '2', 'HKOBTRFIBMKI', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1175', '111', '8', '16', '111', '1', '2', 'OJXVNZTHPLNS', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1176', '111', '8', '17', '111', '1', '2', 'RMAOIGHHBEXD', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1177', '111', '8', '18', '111', '1', '2', 'GBIVKIXDJTON', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1178', '111', '8', '18', '111', '1', '2', 'TOFVQNGSHSZF', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1179', '111', '8', '18', '111', '1', '2', 'DVDYCZOTOQCM', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1180', '111', '8', '16', '111', '1', '2', 'HXLEMWQGMXAG', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1181', '111', '8', '18', '111', '1', '2', 'PAOIDJPHIMVB', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1182', '111', '8', '18', '111', '1', '2', 'BPWJVCWCMNMY', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1183', '111', '8', '18', '111', '1', '2', 'RLIQMFVWLZCI', '7', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1184', '111', '8', '16', '111', '1', '2', 'RCPYWMKUWQBT', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1185', '111', '8', '16', '111', '1', '2', 'ANMPMNIYQAHL', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1186', '111', '8', '16', '111', '1', '2', 'QABXZVTOHDOY', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1187', '111', '8', '16', '111', '1', '2', 'GZCPVUVSGQXF', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1188', '111', '8', '18', '111', '1', '2', 'PYIEWLSQECRR', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1189', '111', '8', '18', '111', '1', '2', 'UGPNQDVSQNUB', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1190', '111', '8', '16', '111', '1', '2', 'NJEYENTBJQRF', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1191', '111', '8', '18', '111', '1', '2', 'UILAYEVCSTBT', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1192', '111', '8', '17', '111', '1', '2', 'LHBRHMJKFGYT', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1193', '111', '8', '16', '111', '1', '2', 'EMJGKCTRJISM', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1194', '111', '8', '17', '111', '1', '2', 'KOBBMRGXUHVA', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1195', '111', '8', '17', '111', '1', '2', 'JHJEALHXKQCL', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1196', '111', '8', '18', '111', '1', '2', 'FOOIDOEIDBDB', '8', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1197', '111', '8', '17', '111', '1', '2', 'TAYXGQULSHLM', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1198', '111', '8', '18', '111', '1', '2', 'FLDMPAPYAFMQ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1199', '111', '8', '18', '111', '1', '2', 'FJSLYBHJXDHA', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1200', '111', '8', '18', '111', '1', '2', 'TVMPYVTQSMHC', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1201', '111', '8', '18', '111', '1', '2', 'NRFIPQKUYRRY', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1202', '111', '8', '18', '111', '1', '2', 'IUERLCDFTBUO', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1203', '111', '8', '18', '111', '1', '2', 'ZGMZRJEDXJLC', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1204', '111', '8', '16', '111', '1', '2', 'QPWZKEKAFRXT', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1205', '111', '8', '18', '111', '1', '2', 'XVKZTUHGZCUZ', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1206', '111', '8', '16', '111', '1', '2', 'PEATXFPAJKFI', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1207', '111', '8', '17', '111', '1', '2', 'QIPQFBVUGCJM', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1208', '111', '8', '17', '111', '1', '2', 'PUNJAQAFSABM', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1209', '111', '8', '16', '111', '1', '2', 'SFYCKHPGYWHK', '9', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1210', '111', '8', '16', '111', '1', '2', 'QADSSPPENKKQ', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1211', '111', '8', '17', '111', '1', '2', 'PQAMAFIXDVJW', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1212', '111', '8', '17', '111', '1', '2', 'TLADKMVETAZU', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1213', '111', '8', '18', '111', '1', '2', 'RTSXQBHWSIZO', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1214', '111', '8', '17', '111', '1', '2', 'YJBEASHLFVWS', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1215', '111', '8', '16', '111', '1', '2', 'UIUOZQBVLUML', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1216', '111', '8', '18', '111', '1', '2', 'QDBZWBTZCBTD', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1217', '111', '8', '18', '111', '1', '2', 'JBYUXSFMZWOY', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1218', '111', '8', '16', '111', '1', '2', 'SGDNCGOZYJCI', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1219', '111', '8', '17', '111', '1', '2', 'MYGWUHDALZGN', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1220', '111', '8', '18', '111', '1', '2', 'FFFFXWQWWAVT', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1221', '111', '8', '18', '111', '1', '2', 'DPOJVRJWQCZE', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1222', '111', '8', '16', '111', '1', '2', 'PUJKKSPVOGHR', '10', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1223', '111', '8', '17', '111', '1', '2', 'NHPGNSGDDQSX', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1224', '111', '8', '16', '111', '1', '2', 'GXAGJQOIOKPP', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1225', '111', '8', '18', '111', '1', '2', 'PPFLWCHFZNLK', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1226', '111', '8', '17', '111', '1', '2', 'COBFEQSTTCND', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1227', '111', '8', '16', '111', '1', '2', 'IAGQNPJTGZTK', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1228', '111', '8', '18', '111', '1', '2', 'TKYNJHWNXOBP', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1229', '111', '8', '18', '111', '1', '2', 'PWAKQROVEMHI', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1230', '111', '8', '18', '111', '1', '2', 'CMBMZRSJMRTD', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1231', '111', '8', '18', '111', '1', '2', 'WBYLMBHWOPBB', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1232', '111', '8', '17', '111', '1', '2', 'AOBNQCOJGKVD', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1233', '111', '8', '17', '111', '1', '2', 'QOFILCXWCMVN', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1234', '111', '8', '16', '111', '1', '2', 'DHUCUQTGZFFE', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1235', '111', '8', '17', '111', '1', '1', 'DPEQLKCBMWXC', '11', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1236', '111', '8', '18', '111', '1', '2', 'QXMRKPKPCTGQ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1237', '111', '8', '18', '111', '1', '2', 'YGXIEJHFPJJY', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1238', '111', '8', '18', '111', '1', '2', 'ZEEHASOBAOYF', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1239', '111', '8', '16', '111', '1', '2', 'USTOHALHOXYS', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1240', '111', '8', '16', '111', '1', '2', 'XXXBDYUXGRUA', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1241', '111', '8', '16', '111', '1', '2', 'PLMUFESKBRCS', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1242', '111', '8', '17', '111', '1', '2', 'CGHITGVOADLQ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1243', '111', '8', '18', '111', '1', '2', 'XALZSLUPEMQF', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1244', '111', '8', '18', '111', '1', '2', 'XBKXCVFCFPNQ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1245', '111', '8', '16', '111', '1', '0', 'MXBGFLRJMPYA', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1246', '111', '8', '16', '111', '1', '0', 'DKXFOOXSWWRN', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1247', '111', '8', '16', '111', '1', '1', 'POQVNXDENGTZ', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1248', '111', '8', '16', '111', '1', '1', 'NJGYPNUGBGQM', '12', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1249', '111', '8', '17', '111', '1', '2', 'WOYCODAQZJWH', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1250', '111', '8', '17', '111', '1', '2', 'GHEWOYXGACVG', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1251', '111', '8', '18', '111', '1', '2', 'VYUZBEFKFCOU', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1252', '111', '8', '16', '111', '1', '2', 'JOJCYDZBONDN', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1253', '111', '8', '18', '111', '1', '2', 'YXBLGFOIEHWY', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1254', '111', '8', '16', '111', '1', '2', 'ELLVASSNHINB', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1255', '111', '8', '17', '111', '1', '2', 'WYRVRNWTICVK', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1256', '111', '8', '17', '111', '1', '2', 'AEUPFNDZVODQ', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1257', '111', '8', '18', '111', '1', '2', 'KUWWDBLKFYKS', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1258', '111', '8', '16', '111', '1', '0', 'CDGCIADSVFWU', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1259', '111', '8', '16', '111', '1', '0', 'GBIDPZQINLAR', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1260', '111', '8', '16', '111', '1', '1', 'NWHLJNVLHMSK', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1261', '111', '8', '18', '111', '1', '1', 'HGTGMQDZHAIK', '-1', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1262', '111', '8', '16', '111', '1', '2', 'UIZTTNUTMVGY', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1263', '111', '8', '17', '111', '1', '2', 'DHDYWHYKPCFB', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1264', '111', '8', '16', '111', '1', '2', 'DVFIYBJKOEJD', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1265', '111', '8', '17', '111', '1', '2', 'GOROKSIEVYIL', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1266', '111', '8', '16', '111', '1', '2', 'SVPAPNAISGEF', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1267', '111', '8', '18', '111', '1', '2', 'HKFHQRQSHWOT', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1268', '111', '8', '17', '111', '1', '2', 'NFZDJDNBSAZL', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1269', '111', '8', '18', '111', '1', '2', 'WBIFDULJUERI', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1270', '111', '8', '17', '111', '1', '2', 'CJFGQBYNCGEK', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1271', '111', '8', '18', '111', '1', '0', 'HORKDLNGGTQO', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1272', '111', '8', '16', '111', '1', '0', 'VMAGANSGRZDI', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1273', '111', '8', '17', '111', '1', '1', 'RNRFVMZMOWBW', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');
INSERT INTO `device_sensor` VALUES ('1274', '111', '8', '16', '111', '1', '1', 'PJOVUCWGZJHW', '-2', '展厅', null, '0', 'admin', '1', '2021-03-12 12:01:12', 'admin', '1', '2021-05-12 12:01:12', null, 'ac88ceb386aa4231b09bf472cb937c24', '2021-05-12 12:01:12', '1');

-- ----------------------------
-- Table structure for device_sensor_copy
-- ----------------------------
DROP TABLE IF EXISTS `device_sensor_copy`;
CREATE TABLE `device_sensor_copy` (
  `ID` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `BUILDING_ID` int DEFAULT NULL COMMENT '建筑列表id',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统id',
  `SENSOR_TYPE_ID` int DEFAULT NULL COMMENT '传感器类型id',
  `CD_ID` int DEFAULT NULL COMMENT '采集设备id(备用字段)',
  `FIELD_STATUS` char(1) DEFAULT NULL COMMENT '数据字段状态[0=不正常/1=正常]（设备，厂商，型号，楼层是否正常）',
  `STATUS` char(1) DEFAULT NULL COMMENT '状态[0=故障/1=报警/2=正常]（需要产品确认）',
  `SENSOR_NO` varchar(200) DEFAULT NULL COMMENT '传感器编号',
  `FLOOR` int DEFAULT NULL COMMENT '楼层',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `POSITION_SIGN` varchar(50) DEFAULT NULL COMMENT '传感器在平面图的位置标记（中间用,号隔开）',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `STATUS_TIME` datetime DEFAULT NULL COMMENT '数据上传时间',
  `HYDRANT_ID` int DEFAULT NULL COMMENT '室外消火栓id',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_sensor_copy
-- ----------------------------

-- ----------------------------
-- Table structure for device_sensor_fd_ext
-- ----------------------------
DROP TABLE IF EXISTS `device_sensor_fd_ext`;
CREATE TABLE `device_sensor_fd_ext` (
  `ID` bigint NOT NULL,
  `FIRE_DOOR_ID` bigint NOT NULL COMMENT '防火门编号',
  `DOOR_STATUS` char(1) NOT NULL DEFAULT '0' COMMENT '传感器表示的门状态0:非门磁传感器　1:开门状态　2:关闭状态',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除状态。1表示已删除 0表示正常',
  `CRT_USER_NAME` varchar(100) NOT NULL COMMENT '创建人名称',
  `CRT_USER_ID` varchar(32) NOT NULL COMMENT '创建人用户ID ',
  `CRT_TIME` datetime NOT NULL,
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '更新人姓名',
  `UPD_USER_ID` varchar(32) DEFAULT NULL COMMENT '更新人用户ID',
  `UPD_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='防火门传感器扩展信息';

-- ----------------------------
-- Records of device_sensor_fd_ext
-- ----------------------------

-- ----------------------------
-- Table structure for device_sensor_manufacturer
-- ----------------------------
DROP TABLE IF EXISTS `device_sensor_manufacturer`;
CREATE TABLE `device_sensor_manufacturer` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `SENSOR_MANUFACTURER` varchar(255) DEFAULT NULL COMMENT '传感器厂商',
  `CODE_NAME` varchar(255) DEFAULT NULL COMMENT '传感器厂商代号',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_sensor_manufacturer
-- ----------------------------
INSERT INTO `device_sensor_manufacturer` VALUES ('1', '北京广微积电科技有限公司', '11111', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('2', '西门子传感器与通讯有限公司', '11112', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('3', '广州市西克传感器有限公司', '11113', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('4', '图尔克（天津）传感器有限公司', '11114', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('5', '麦克传感器股份有限公司', '11115', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('6', '芯福传感器技术有限公司', '11116', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('7', '浙江南洋传感器制造有限公司', '11117', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('8', '上海矽睿科技有限公司', '11118', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('9', '深迪半导体（上海）有限公司', '11119', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('10', '湖南菲尔斯特传感器有限公司', '11120', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('11', '苏州德坤传感器有限公司', '11121', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('12', '埃比电子传感器(苏州)有限公司', '11122', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('13', '泰科思(深圳)传感器有限公司', '11123', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('14', '美捷特（厦门）传感器件有限公司', '11124', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('15', 'MTS传感器中国(MTS', '11125', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('16', '巴鲁夫传感器(成都)有限公司', '11126', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('17', '北京航宇时代传感器有限公司', '11127', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('18', '东莞大泉传感器有限公司', '11128', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('19', '威格勒传感器技术(上海)有限公司', '11129', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('20', '扬州奥力威传感器有限公司', '11130', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('21', '上海瑞以森传感器有限公司', '11131', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('22', '上海尼赛拉传感器有限公司', '11132', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('23', '常州盛士达传感器有限公司', '11133', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('24', '武汉优斯特传感器科技有限公司', '11134', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_manufacturer` VALUES ('25', '西安微纳传感器研究所有限公司', '11135', '0', 'admin', '1', '2021-06-10 12:04:17', 'admin', '1', '2021-06-10 12:04:17', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for device_sensor_mp_relation
-- ----------------------------
DROP TABLE IF EXISTS `device_sensor_mp_relation`;
CREATE TABLE `device_sensor_mp_relation` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `SENSOR_SERIES_ID` int DEFAULT NULL COMMENT '传感器系列id',
  `MP_ID` int DEFAULT NULL COMMENT '测点id',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_sensor_mp_relation
-- ----------------------------
INSERT INTO `device_sensor_mp_relation` VALUES ('1', '1', '152', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-08-08 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('2', '1', '153', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-08-20 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('3', '1', '154', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-04-19 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('4', '1', '155', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-04-19 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('5', '1', '156', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-09-03 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('6', '1', '157', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-11-12 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('7', '1', '158', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-09-06 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('8', '1', '159', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-10-08 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('9', '1', '160', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-09-15 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('10', '2', '152', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-07-17 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('11', '2', '153', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-07-05 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('12', '2', '154', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-05-19 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('13', '2', '155', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-09-09 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('14', '2', '156', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-06-28 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('15', '2', '157', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-11-23 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('16', '2', '158', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-10-05 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('17', '2', '159', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-05-02 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('18', '2', '160', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-05-10 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('19', '3', '152', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-10-01 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('20', '3', '153', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-11-06 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('21', '3', '154', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-10-06 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('22', '3', '155', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-04-14 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('23', '3', '156', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-04-16 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('24', '3', '157', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-09-05 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('25', '3', '158', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-05-13 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('26', '3', '159', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-05-08 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `device_sensor_mp_relation` VALUES ('27', '3', '160', '0', 'admin', '1', '2021-06-02 14:27:33', 'admin', '1', '2021-06-14 00:00:00', null, 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for device_sensor_series
-- ----------------------------
DROP TABLE IF EXISTS `device_sensor_series`;
CREATE TABLE `device_sensor_series` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '类型编号为了统一显示时采用传感器类型ID',
  `COLOR` varchar(20) DEFAULT NULL COMMENT '传感器颜色',
  `SENSOR_TYPE` varchar(100) DEFAULT NULL COMMENT '传感器类型',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `TYPE` char(1) DEFAULT NULL COMMENT '类型[0=室内/1=室外]',
  `SENSOR_TYPE_ID` int DEFAULT NULL COMMENT '传感器类型ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_sensor_series
-- ----------------------------
INSERT INTO `device_sensor_series` VALUES ('1', '银白', '智能集成', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '1');
INSERT INTO `device_sensor_series` VALUES ('2', '银灰色', '开关分立', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '2');
INSERT INTO `device_sensor_series` VALUES ('3', '天蓝色', '多线圈', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '3');
INSERT INTO `device_sensor_series` VALUES ('4', '天蓝色', 'EPS消防应急电源', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '4');
INSERT INTO `device_sensor_series` VALUES ('5', '天蓝色', 'EPS消防应急电源', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '5');
INSERT INTO `device_sensor_series` VALUES ('6', '银白', '智能集成', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '6');
INSERT INTO `device_sensor_series` VALUES ('7', '银灰色', '开关分立', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '7');
INSERT INTO `device_sensor_series` VALUES ('8', '天蓝色', '多线圈', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '8');
INSERT INTO `device_sensor_series` VALUES ('9', '天蓝色', 'EPS消防应急电源', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '9');
INSERT INTO `device_sensor_series` VALUES ('10', '天蓝色', 'EPS消防应急电源', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '10');
INSERT INTO `device_sensor_series` VALUES ('11', '银白', '智能集成', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '11');
INSERT INTO `device_sensor_series` VALUES ('12', '银灰色', '开关分立', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '12');
INSERT INTO `device_sensor_series` VALUES ('13', '天蓝色', '多线圈', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '13');
INSERT INTO `device_sensor_series` VALUES ('14', '天蓝色', 'EPS消防应急电源', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '14');
INSERT INTO `device_sensor_series` VALUES ('15', '天蓝色', 'EPS消防应急电源', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '15');
INSERT INTO `device_sensor_series` VALUES ('16', '银白', '智能集成', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '16');
INSERT INTO `device_sensor_series` VALUES ('17', '银灰色', '开关分立', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '17');
INSERT INTO `device_sensor_series` VALUES ('18', '天蓝色', '多线圈', '0', 'admin', '1', '2021-06-10 20:11:31', 'admin', '1', '2021-06-10 20:11:31', null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '18');

-- ----------------------------
-- Table structure for device_sensor_temp
-- ----------------------------
DROP TABLE IF EXISTS `device_sensor_temp`;
CREATE TABLE `device_sensor_temp` (
  `ID` bigint NOT NULL AUTO_INCREMENT,
  `BUILDING_ID` int DEFAULT NULL,
  `CHANNEL_ID` int DEFAULT NULL,
  `SENSOR_TYPE_ID` int DEFAULT NULL,
  `CD_ID` int DEFAULT NULL,
  `FIELD_STATUS` char(1) DEFAULT NULL,
  `STATUS` char(1) DEFAULT NULL,
  `SENSOR_NO` varchar(50) DEFAULT NULL,
  `FLOOR` int DEFAULT NULL,
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL,
  `POSITION_SIGN` varchar(50) DEFAULT NULL,
  `DEL_FLAG` char(1) DEFAULT '0',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL,
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPD_USER_NAME` varchar(100) DEFAULT NULL,
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP,
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_sensor_temp
-- ----------------------------

-- ----------------------------
-- Table structure for device_sensor_type
-- ----------------------------
DROP TABLE IF EXISTS `device_sensor_type`;
CREATE TABLE `device_sensor_type` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `EQUIPMENT_TYPE` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `MANUFACTURER` varchar(255) DEFAULT NULL COMMENT '厂商',
  `MODEL` varchar(255) DEFAULT NULL COMMENT '型号',
  `DATA_UNIT` varchar(50) DEFAULT NULL COMMENT '数据单位',
  `DATA_ACQUISITION_CYCLE_UNIT` char(1) DEFAULT NULL COMMENT '数据采集周期单位[0=秒/1=分钟/2=小时/3=天/4=月](单位暂定，待产品规定)',
  `DATA_ACQUISITION_CYCLE_VALUE` int DEFAULT NULL COMMENT '数据采集周期数值',
  `DATA_ACQUISITION_CYCLE_SECOND` int DEFAULT NULL COMMENT '数据采集周期转化为秒',
  `ACQUISITION_DELAY_TIME_UNIT` char(1) DEFAULT NULL COMMENT '采集延时时间单位[0=秒/1=分钟/2=小时/3=天/4=月](单位暂定，待产品规定)',
  `ACQUISITION_DELAY_TIME_VALUE` int DEFAULT NULL COMMENT '采集延时时间数值',
  `ACQUISITION_DELAY_TIME_SECOND` int DEFAULT NULL COMMENT '采集延时时间转换为秒',
  `FIRST_LEVEL_ALARM_MIN` int DEFAULT NULL COMMENT '一级报警等级阈值(最小值)',
  `FIRST_LEVEL_ALARM_MAX` int DEFAULT NULL COMMENT '一级报警等级阈值(最大值)',
  `TWO_LEVEL_ALARM_MIN` int DEFAULT NULL COMMENT '二级报警等级阈值(最小值)',
  `TWO_LEVEL_ALARM_MAX` int DEFAULT NULL COMMENT '二级报警等级阈值(最大值)',
  `MAINTENANCE_CYCLE_UNIT` char(1) DEFAULT NULL COMMENT '维保周期单位[0=天/1=月/2=年](单位暂定，待产品规定)',
  `MAINTENANCE_CYCLE_VALUE` int DEFAULT NULL COMMENT '维保周期数值',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `MANUFACTURER_ID` int DEFAULT NULL COMMENT '厂商ID',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_sensor_type
-- ----------------------------
INSERT INTO `device_sensor_type` VALUES ('1', '编码型烟感', '同安迪', '飞天系列', '台', '1', '5', '300', '0', '30', '30', null, null, null, null, null, null, '0', 'admin', '1', '2021-06-10 15:15:54', 'admin', '1', '2021-06-10 15:15:54', null, 'ac88ceb386aa4231b09bf472cb937c24', '11111', '1');
INSERT INTO `device_sensor_type` VALUES ('2', '开关量烟感', '同安迪', '飞天系列', '台', '1', '5', '300', '0', '30', '30', null, null, null, null, null, null, '0', 'admin', '1', '2021-06-10 15:15:54', 'admin', '1', '2021-06-10 15:15:54', null, 'ac88ceb386aa4231b09bf472cb937c24', '11111', '1');
INSERT INTO `device_sensor_type` VALUES ('3', '多线烟感', '同安迪', '飞天系列', '台', '1', '5', '300', '0', '30', '30', null, null, null, null, null, null, '0', 'admin', '1', '2021-06-10 15:15:54', 'admin', '1', '2021-06-10 15:15:54', null, 'ac88ceb386aa4231b09bf472cb937c24', '11111', '1');
INSERT INTO `device_sensor_type` VALUES ('4', '编码型烟感', '百芳', '华云系列', '台', '1', '5', '300', '0', '30', '30', null, null, null, null, null, null, '0', 'admin', '1', '2021-06-10 15:15:54', 'admin', '1', '2021-06-10 15:15:54', null, 'ac88ceb386aa4231b09bf472cb937c24', '11111', '1');
INSERT INTO `device_sensor_type` VALUES ('5', '开关量烟感', '百芳', '华云系列', '台', '1', '5', '300', '0', '30', '30', null, null, null, null, null, null, '0', 'admin', '1', '2021-06-10 15:15:54', 'admin', '1', '2021-06-10 15:15:54', null, 'ac88ceb386aa4231b09bf472cb937c24', '11111', '1');
INSERT INTO `device_sensor_type` VALUES ('6', '多线烟感', '百芳', '华云系列', '台', '1', '5', '300', '0', '30', '30', null, null, null, null, null, null, '0', 'admin', '1', '2021-06-10 15:15:54', 'admin', '1', '2021-06-10 15:15:54', null, 'ac88ceb386aa4231b09bf472cb937c24', '11111', '1');
INSERT INTO `device_sensor_type` VALUES ('7', '单相消防照明型EPS', '宝星', '宝星', '套', '1', '5', '300', '0', '30', '30', null, null, null, null, null, null, '0', 'admin', '1', '2021-06-10 15:15:54', 'admin', '1', '2021-06-10 15:15:54', null, 'ac88ceb386aa4231b09bf472cb937c24', '11111', '6');
INSERT INTO `device_sensor_type` VALUES ('8', '三相动力/照明混合型EPS', '宝星', '宝星', '套', '1', '5', '300', '0', '30', '30', null, null, null, null, null, null, '0', 'admin', '1', '2021-06-10 15:15:54', 'admin', '1', '2021-06-10 15:15:54', null, 'ac88ceb386aa4231b09bf472cb937c24', '11111', '6');
INSERT INTO `device_sensor_type` VALUES ('9', '三相变频动力型EPS', '宝星', '宝星', '套', '1', '5', '300', '0', '30', '30', null, null, null, null, null, null, '0', 'admin', '1', '2021-06-10 15:15:54', 'admin', '1', '2021-06-10 15:15:54', null, 'ac88ceb386aa4231b09bf472cb937c24', '11111', '6');

-- ----------------------------
-- Table structure for device_tenant_setup
-- ----------------------------
DROP TABLE IF EXISTS `device_tenant_setup`;
CREATE TABLE `device_tenant_setup` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `PLAN_SETUP` char(1) DEFAULT NULL COMMENT '是否启用平面图模式 [0/未启用，1/启用]',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_tenant_setup
-- ----------------------------

-- ----------------------------
-- Table structure for device_test_plan
-- ----------------------------
DROP TABLE IF EXISTS `device_test_plan`;
CREATE TABLE `device_test_plan` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `TEST_ROUTE_ID` int DEFAULT NULL COMMENT '检测路线id',
  `START_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '开始日期',
  `END_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '结束日期',
  `PATROL_CYCLE` int DEFAULT NULL COMMENT '巡检周期（单位：天）',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='检测计划表';

-- ----------------------------
-- Records of device_test_plan
-- ----------------------------

-- ----------------------------
-- Table structure for device_test_route
-- ----------------------------
DROP TABLE IF EXISTS `device_test_route`;
CREATE TABLE `device_test_route` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `ROUTE_NAME` varchar(200) DEFAULT NULL COMMENT '路线名',
  `LABEL_COUNT` varchar(200) DEFAULT NULL COMMENT '巡检设施个数',
  `ROUTE_FLAG` char(1) DEFAULT NULL COMMENT '0=室内路线,1=室外路线',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='检测路线表';

-- ----------------------------
-- Records of device_test_route
-- ----------------------------

-- ----------------------------
-- Table structure for device_upload_information
-- ----------------------------
DROP TABLE IF EXISTS `device_upload_information`;
CREATE TABLE `device_upload_information` (
  `id` int NOT NULL AUTO_INCREMENT,
  `FILE_NAME` varchar(255) DEFAULT NULL,
  `FILE_PATH` varchar(255) DEFAULT NULL,
  `FILE_TYPE` varchar(255) DEFAULT NULL,
  `FILE_SIZE` double DEFAULT NULL,
  `SYSTEM` varchar(255) DEFAULT NULL,
  `DEL_FLAG` varchar(255) DEFAULT NULL,
  `CRT_USER_NAME` varchar(255) DEFAULT NULL,
  `CRT_USER_ID` int DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT NULL,
  `UPD_USER_NAME` varchar(255) DEFAULT NULL,
  `UPD_USER_ID` int DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_upload_information
-- ----------------------------

-- ----------------------------
-- Table structure for device_video_analysis_solution
-- ----------------------------
DROP TABLE IF EXISTS `device_video_analysis_solution`;
CREATE TABLE `device_video_analysis_solution` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `ANALYSIS_SOLUTION_NAME` varchar(255) DEFAULT NULL COMMENT '分析方案名称',
  `ANALYSIS_SOLUTION_CODE` varchar(255) DEFAULT NULL COMMENT '分析方案代号',
  `ANALYSIS_SOLUTION_IMAGE` varchar(255) DEFAULT NULL,
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL COMMENT '创建者ID',
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL COMMENT '更新者ID',
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL COMMENT '部门ID',
  `TENANT_ID` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='视频分析方案';

-- ----------------------------
-- Records of device_video_analysis_solution
-- ----------------------------
INSERT INTO `device_video_analysis_solution` VALUES ('1', '烟雾/火焰识别', 'FIRE', 'http://file.tmc.turing.ac.cn/solution_image_fire.png', null, null, '2019-02-21 09:05:21', null, null, '2019-02-21 09:05:21', null, null);
INSERT INTO `device_video_analysis_solution` VALUES ('2', '仪表识别', 'METER', '仪表图片', null, null, '2019-03-01 15:33:41', null, null, '2019-03-01 15:33:41', null, null);

-- ----------------------------
-- Table structure for device_video_ext
-- ----------------------------
DROP TABLE IF EXISTS `device_video_ext`;
CREATE TABLE `device_video_ext` (
  `ID` bigint NOT NULL COMMENT '主键id',
  `DEVICE_GROUP_ID` int DEFAULT NULL COMMENT '设备组ID',
  `DEVICE_VIDEO_NAME` varchar(255) DEFAULT NULL COMMENT '设备名称',
  `SENSOR_NO` varchar(255) DEFAULT NULL COMMENT '设备序列号',
  `DEVICE_VALIDATE_CODE` varchar(255) DEFAULT NULL COMMENT '设备验证码',
  `SHOW_FLAG` char(1) DEFAULT '0' COMMENT '显示标记[1=是/0=否（default）]',
  `ANALYSIS_SOLUTION_ID` int DEFAULT NULL COMMENT '分析方案ID',
  `VIDEO_LIVE_ADDRESS` longtext COMMENT '设备视频播放地址JSON',
  `ALARM_MSG` varchar(255) DEFAULT NULL COMMENT '告警状态信息JSON',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='视频设备扩展信息';

-- ----------------------------
-- Records of device_video_ext
-- ----------------------------

-- ----------------------------
-- Table structure for device_video_group
-- ----------------------------
DROP TABLE IF EXISTS `device_video_group`;
CREATE TABLE `device_video_group` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `DEVICE_GROUP_NAME` varchar(255) DEFAULT NULL COMMENT '设备组名称',
  `DEVICE_GROUP_IMAGE` varchar(255) DEFAULT NULL COMMENT '设备组平面图URL',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL COMMENT '创建者ID',
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL COMMENT '更新者ID',
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL COMMENT '部门ID',
  `TENANT_ID` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='视频设备组';

-- ----------------------------
-- Records of device_video_group
-- ----------------------------

-- ----------------------------
-- Table structure for warning
-- ----------------------------
DROP TABLE IF EXISTS `warning`;
CREATE TABLE `warning` (
  `id` int NOT NULL,
  `warning` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Bitcoin_Address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of warning
-- ----------------------------
