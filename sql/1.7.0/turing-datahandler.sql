/*
Navicat MySQL Data Transfer

Source Server         : 130
Source Server Version : 80025
Source Host           : 192.168.19.130:3306
Source Database       : turing-datahandler

Target Server Type    : MYSQL
Target Server Version : 80025
File Encoding         : 65001

Date: 2021-07-14 14:53:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for device_abnormal
-- ----------------------------
DROP TABLE IF EXISTS `device_abnormal`;
CREATE TABLE `device_abnormal` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `BUILD_ID` int DEFAULT NULL COMMENT '建筑列表id',
  `EQU_ID` int DEFAULT NULL COMMENT '设备id',
  `ALRM_CATEGORY` char(100) DEFAULT NULL COMMENT '报警类型：0：故障，1：火警',
  `ALRM_TYPE` char(100) DEFAULT NULL COMMENT '报警类型',
  `ALRM_DATE` datetime DEFAULT NULL COMMENT '报警时间',
  `HANDLE_FLAG` char(1) DEFAULT NULL COMMENT '是否处理[1=是/0=否]',
  `FIRE_FLAG` char(1) DEFAULT NULL COMMENT '是否真实火警[2=火警测试/1=是/0=否]',
  `CONFIR_DATE` datetime DEFAULT NULL COMMENT '确认时间',
  `HANDLE_DATE` datetime DEFAULT NULL COMMENT '处理时间',
  `RESTORE_DATE` datetime DEFAULT NULL COMMENT '恢复时间',
  `CONFIR_PERSON` varchar(100) DEFAULT NULL COMMENT '确认人',
  `HANDLE_PERSON` varchar(100) DEFAULT NULL COMMENT '处理人',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `ALRM_DATA` double(10,3) DEFAULT NULL COMMENT '传感器数据',
  `LOG_ID` char(64) DEFAULT NULL COMMENT '日志ID',
  `UNIT` varchar(100) DEFAULT NULL COMMENT '测点代号',
  `B_NAME` varchar(200) DEFAULT NULL COMMENT '建筑名称',
  `SENSOR_NO` varchar(50) DEFAULT NULL COMMENT '传感器编号',
  `EQUIPMENT_TYPE` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `FLOOR` int DEFAULT NULL COMMENT '楼层',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `MEASURING_POINT` varchar(100) DEFAULT NULL COMMENT '测点',
  `LEVEL` varchar(20) DEFAULT NULL COMMENT '报警等级',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统id',
  `DATA_UNIT` varchar(100) DEFAULT NULL COMMENT '数据单位',
  PRIMARY KEY (`ID`),
  KEY `device_abnormal_alrm_date` (`ALRM_DATE`) USING BTREE,
  KEY `abnormal_index` (`ALRM_CATEGORY`,`HANDLE_FLAG`,`FIRE_FLAG`) USING BTREE,
  KEY `countIndex` (`EQU_ID`,`ALRM_CATEGORY`) USING BTREE,
  KEY `queryPage` (`ALRM_DATE`,`ALRM_CATEGORY`,`LEVEL`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of device_abnormal
-- ----------------------------
INSERT INTO `device_abnormal` VALUES ('1', '111', '150', null, '0', '2021-06-01 23:31:14', '0', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '1', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c24', null, null, '5', '建筑名称01', '111', '1', '1', '展厅01', ' 测点01', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('2', '111', '151', null, '0', '2021-06-01 23:31:14', '0', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '2', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c25', null, null, '6', '建筑名称02', '111', '1', '1', '展厅02', ' 测点02', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('3', '111', '152', null, '0', '2021-06-01 23:31:14', '1', '0', '2021-06-11 11:49:16', '2021-06-11 11:49:16', null, 'admin', 'admin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '3', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c26', null, null, '7', '建筑名称03', '111', '1', '1', '展厅03', ' 测点03', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('4', '111', '153', null, '0', '2021-06-01 23:31:14', '1', '0', '2021-06-10 21:24:10', '2021-06-10 21:24:10', null, 'admin', 'admin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '4', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c27', null, null, '8', '建筑名称04', '111', '1', '1', '展厅04', ' 测点04', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('5', '111', '154', null, '0', '2021-06-01 23:31:14', '1', '0', '2021-06-04 17:09:56', '2021-06-04 17:09:56', null, 'admin', 'admin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '5', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c28', null, null, '9', '建筑名称05', '111', '1', '1', '展厅05', ' 测点05', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('6', '111', '155', '0', '0', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '6', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c29', null, null, '10', '建筑名称06', '111', '1', '1', '展厅06', ' 测点06', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('7', '111', '156', '0', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '1', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c24', null, null, '5', '建筑名称01', '111', '1', '2', '展厅01', ' 测点01', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('8', '111', '157', '0', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '2', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c25', null, null, '6', '建筑名称02', '111', '1', '2', '展厅02', ' 测点02', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('9', '111', '158', '0', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '3', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c26', null, null, '7', '建筑名称03', '111', '1', '3', '展厅03', ' 测点03', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('10', '111', '159', '0', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '4', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c27', null, null, '8', '建筑名称04', '111', '1', '3', '展厅04', ' 测点04', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('11', '111', '160', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '5', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c28', null, null, '9', '建筑名称05', '111', '1', '3', '展厅05', ' 测点05', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('12', '111', '161', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '6', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c29', null, null, '10', '建筑名称06', '111', '1', '3', '展厅06', ' 测点06', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('13', '111', '162', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '1', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c24', null, null, '5', '建筑名称01', '111', '1', '-1', '房间11201', ' 测点0111', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('14', '111', '163', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '2', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c25', null, null, '6', '建筑名称02', '111', '1', '-1', '房间11304', ' 测点0112', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('15', '111', '164', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '3', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c26', null, null, '7', '建筑名称03', '111', '1', '-1', '房间11403', ' 测点0113', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('16', '111', '165', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '4', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c27', null, null, '8', '建筑名称04', '111', '1', '-1', '房间11508', ' 测点0114', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('17', '111', '166', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '5', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c28', null, null, '9', '建筑名称05', '111', '1', '-1', '房间11621', ' 测点0115', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('18', '111', '167', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '6', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c29', null, null, '10', '建筑名称06', '111', '1', '-1', '房间11751', ' 测点0116', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('19', '111', '168', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '7', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c30', null, null, '11', '建筑名称07', '111', '1', '-1', '房间11881', ' 测点0117', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('20', '111', '169', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '8', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c31', null, null, '12', '建筑名称08', '111', '1', '-1', '房间12011', ' 测点0118', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('21', '111', '170', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '9', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c32', null, null, '13', '建筑名称09', '111', '1', '-1', '房间12141', ' 测点0119', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('22', '111', '171', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '3', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c26', null, null, '7', '建筑名称03', '111', '1', '-2', '房间21751', ' 测点02111', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('23', '111', '172', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '4', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c27', null, null, '8', '建筑名称04', '111', '1', '-2', '房间21881', ' 测点02211', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('24', '111', '173', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '5', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c28', null, null, '9', '建筑名称05', '111', '1', '-2', '房间21881', ' 测点02311', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('25', '111', '174', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '6', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c29', null, null, '10', '建筑名称06', '111', '1', '-2', '房间21751', ' 测点02411', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('26', '111', '175', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '7', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c30', null, null, '11', '建筑名称07', '111', '1', '-2', '房间21881', ' 测点02511', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('27', '111', '176', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '8', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c31', null, null, '12', '建筑名称08', '111', '1', '-2', '房间21881', ' 测点02611', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('28', '111', '177', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '9', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c32', null, null, '13', '建筑名称09', '111', '1', '-2', '房间21751', ' 测点02711', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('29', '111', '178', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '10', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c33', null, null, '14', '建筑名称10', '111', '1', '-2', '房间21881', ' 测点02811', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('30', '111', '179', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '11', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c34', null, null, '15', '建筑名称11', '111', '1', '-2', '房间22011', ' 测点02911', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('31', '111', '180', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '10', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c33', null, null, '14', '建筑名称10', '111', '1', '3', '房间21881', ' 测点02811', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('32', '111', '181', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '11', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c34', null, null, '15', '建筑名称11', '111', '1', '3', '房间22011', ' 测点02911', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('33', '111', '182', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '12', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c35', null, null, '16', '建筑名称12', '111', '1', '4', '房间22141', ' 测点03011', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('34', '111', '183', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '13', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c36', null, null, '17', '建筑名称13', '111', '1', '4', '房间22271', ' 测点03111', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('35', '111', '184', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '14', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c37', null, null, '18', '建筑名称14', '111', '1', '4', '房间22401', ' 测点03211', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('36', '111', '185', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '15', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c38', null, null, '19', '建筑名称15', '111', '1', '5', '房间22531', ' 测点03311', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('37', '111', '186', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '16', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c39', null, null, '20', '建筑名称16', '111', '1', '5', '房间22661', ' 测点03411', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('38', '111', '187', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '17', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c40', null, null, '21', '建筑名称17', '111', '1', '6', '房间22791', ' 测点03511', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('39', '111', '188', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '18', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c41', null, null, '22', '建筑名称18', '111', '1', '6', '房间22921', ' 测点03611', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('40', '111', '189', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '19', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c42', null, null, '23', '建筑名称19', '111', '1', '7', '房间23051', ' 测点03711', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('41', '111', '190', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '20', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c43', null, null, '24', '建筑名称20', '111', '1', '8', '房间23181', ' 测点03811', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('42', '111', '191', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '21', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c44', null, null, '25', '建筑名称21', '111', '1', '8', '房间23311', ' 测点03911', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('43', '111', '192', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '22', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c45', null, null, '26', '建筑名称22', '111', '1', '8', '房间23441', ' 测点04011', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('44', '111', '193', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '23', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c46', null, null, '27', '建筑名称23', '111', '1', '8', '房间23571', ' 测点04111', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('45', '111', '194', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '24', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c47', null, null, '28', '建筑名称24', '111', '1', '8', '房间23701', ' 测点04211', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('46', '111', '195', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '25', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c48', null, null, '29', '建筑名称25', '111', '1', '9', '房间23831', ' 测点04311', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('47', '111', '196', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '26', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c49', null, null, '30', '建筑名称26', '111', '1', '10', '房间23961', ' 测点04411', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('48', '111', '197', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '27', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c50', null, null, '31', '建筑名称27', '111', '1', '11', '房间24091', ' 测点04511', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('49', '111', '198', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '28', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c51', null, null, '32', '建筑名称28', '111', '1', '11', '房间24221', ' 测点04611', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('50', '111', '199', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '29', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c52', null, null, '33', '建筑名称29', '111', '1', '11', '房间24351', ' 测点04711', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('51', '111', '200', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '30', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c53', null, null, '34', '建筑名称30', '111', '1', '11', '房间24481', ' 测点04811', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('52', '111', '201', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '31', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c54', null, null, '35', '建筑名称31', '111', '1', '11', '房间24611', ' 测点04911', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('53', '111', '202', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '32', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c55', null, null, '36', '建筑名称32', '111', '1', '11', '房间24741', ' 测点05011', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('54', '111', '203', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '33', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c56', null, null, '37', '建筑名称33', '111', '1', '12', '房间24871', ' 测点05111', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('55', '111', '204', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '34', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c57', null, null, '38', '建筑名称34', '111', '1', '12', '房间25001', ' 测点05211', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('56', '111', '205', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '35', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c58', null, null, '39', '建筑名称35', '111', '1', '12', '房间25131', ' 测点05311', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('57', '111', '206', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '36', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c59', null, null, '40', '建筑名称36', '111', '1', '12', '房间25261', ' 测点05411', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('58', '111', '207', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '37', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c60', null, null, '41', '建筑名称37', '111', '1', '12', '房间25391', ' 测点05511', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('59', '111', '208', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '38', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c61', null, null, '42', '建筑名称38', '111', '1', '12', '房间25521', ' 测点05611', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('60', '111', '209', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '39', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c62', null, null, '43', '建筑名称39', '111', '1', '12', '房间25651', ' 测点05711', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('61', '111', '210', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '40', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c63', null, null, '44', '建筑名称40', '111', '1', '12', '房间25781', ' 测点05811', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('62', '111', '211', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '41', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c64', null, null, '45', '建筑名称41', '111', '1', '12', '房间25911', ' 测点05911', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('63', '111', '212', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '42', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c65', null, null, '46', '建筑名称42', '111', '1', '12', '房间26041', ' 测点06011', '1', '1', '次');
INSERT INTO `device_abnormal` VALUES ('64', '111', '213', '1', '1', '2021-06-01 23:31:14', '1', '1', '2021-06-01 23:31:14', '2021-06-01 23:31:14', null, 'amdin', 'amdin', '0', 'amdin', '1', '2021-06-01 23:31:14', 'amdin', '43', '2021-06-01 23:31:14', null, 'ac88ceb386aa4231b09bf472cb937c66', null, null, '47', '建筑名称43', '111', '1', '12', '房间26171', ' 测点06111', '1', '1', '次');

-- ----------------------------
-- Table structure for device_facilities_abnormal
-- ----------------------------
DROP TABLE IF EXISTS `device_facilities_abnormal`;
CREATE TABLE `device_facilities_abnormal` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `FIRE_COCK_ID` int DEFAULT NULL COMMENT '消火栓id',
  `EQU_ID` int DEFAULT NULL COMMENT '设备id',
  `ALRM_TYPE` char(100) DEFAULT NULL COMMENT '报警类型',
  `ALRM_DATE` datetime DEFAULT NULL COMMENT '报警时间',
  `HANDLE_FLAG` char(1) DEFAULT NULL COMMENT '是否处理[1=是/0=否]',
  `FAULT_FLAG` char(1) DEFAULT NULL COMMENT '是否故障[1=误报/0=故障]',
  `CONFIR_DATE` datetime DEFAULT NULL COMMENT '确认时间',
  `HANDLE_DATE` datetime DEFAULT NULL COMMENT '处理时间',
  `RESTORE_DATE` datetime DEFAULT NULL COMMENT '恢复时间',
  `CONFIR_PERSON` varchar(100) DEFAULT NULL COMMENT '确认人',
  `HANDLE_PERSON` varchar(100) DEFAULT NULL COMMENT '处理人',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `ALRM_DATA` double(10,3) DEFAULT NULL COMMENT '传感器数据',
  `LOG_ID` char(64) DEFAULT NULL COMMENT '日志ID',
  `UNIT` varchar(100) DEFAULT NULL COMMENT '测点代号',
  `DATA_UNIT` varchar(100) DEFAULT NULL COMMENT '数据单位',
  `HYDRANT_NAME` varchar(200) DEFAULT NULL COMMENT '消火栓名称',
  `SENSOR_NO` varchar(50) DEFAULT NULL COMMENT '传感器编号',
  `EQUIPMENT_TYPE` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `MEASURING_POINT` varchar(100) DEFAULT NULL COMMENT '测点',
  `LEVEL` varchar(20) DEFAULT NULL COMMENT '报警等级',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统id',
  PRIMARY KEY (`ID`),
  KEY `device_abnormal_alrm_date` (`ALRM_DATE`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='硬件设施异常记录表';

-- ----------------------------
-- Records of device_facilities_abnormal
-- ----------------------------

-- ----------------------------
-- Table structure for device_fire_main_abnormal
-- ----------------------------
DROP TABLE IF EXISTS `device_fire_main_abnormal`;
CREATE TABLE `device_fire_main_abnormal` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `BUILD_ID` int DEFAULT NULL COMMENT '建筑列表id',
  `FIRE_MAIN_ID` int DEFAULT NULL COMMENT '消防主机id',
  `EQU_ID` int DEFAULT NULL COMMENT '设备id',
  `SERIES` varchar(255) DEFAULT NULL COMMENT '系列',
  `SENSOR_LOOP` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL COMMENT '地址',
  `ALRM_CATEGORY` char(100) DEFAULT NULL COMMENT '报警类型：0：故障，1：火警',
  `ALRM_TYPE` char(100) DEFAULT NULL COMMENT '报警类型',
  `ALRM_DATE` datetime DEFAULT NULL COMMENT '报警时间',
  `HANDLE_FLAG` char(1) DEFAULT NULL COMMENT '是否处理[1=是/0=否]',
  `FIRE_FLAG` char(1) DEFAULT NULL COMMENT '是否真实火警[2=火警测试/1=是/0=否]',
  `HANDLE_DATE` datetime DEFAULT NULL COMMENT '处理时间',
  `HANDLE_PERSON` varchar(100) DEFAULT NULL COMMENT '处理人',
  `DEL_FLAG` char(1) DEFAULT '0' COMMENT '删除标记[1=是/0=否（default）]',
  `CRT_USER_NAME` varchar(100) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL,
  `CRT_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPD_USER_NAME` varchar(100) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL,
  `UPD_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL,
  `TENANT_ID` varchar(32) DEFAULT NULL,
  `ALRM_DATA` double(10,3) DEFAULT NULL COMMENT '传感器数据',
  `LOG_ID` char(64) DEFAULT NULL COMMENT '日志ID',
  `B_NAME` varchar(200) DEFAULT NULL COMMENT '建筑名称',
  `FLOOR` int DEFAULT NULL COMMENT '楼层',
  `POSITION_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '位置描述',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统id',
  PRIMARY KEY (`ID`),
  KEY `device_abnormal_alrm_date` (`ALRM_DATE`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='消防主机异常表';

-- ----------------------------
-- Records of device_fire_main_abnormal
-- ----------------------------

-- ----------------------------
-- Table structure for notice_log
-- ----------------------------
DROP TABLE IF EXISTS `notice_log`;
CREATE TABLE `notice_log` (
  `ID` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `SENSOR_NO` varchar(255) DEFAULT NULL COMMENT '设备编号',
  `NOTICE_TYPE` varchar(255) DEFAULT NULL COMMENT '通知方式：0(APP),1(短信),2(语音电话)',
  `NOTICE_CONTENT` varchar(256) DEFAULT NULL COMMENT '推送内容',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统 关联turing-admin的channel表',
  `USERNAME` varchar(128) DEFAULT NULL COMMENT '接收账号',
  `MOBILE_PHONE` varchar(256) DEFAULT NULL COMMENT '接收手机号',
  `SERVICE_SUPPLY_NAME` varchar(256) NOT NULL COMMENT '服务方',
  `NOTICE_RESULT` varchar(256) DEFAULT NULL COMMENT '通知结果',
  `ALARM_TIME` datetime DEFAULT NULL COMMENT '报警时间',
  `NOTICE_TIME` datetime DEFAULT NULL COMMENT '通知时间',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记 1：删除，0：未删除',
  `CRT_USER_NAME` varchar(128) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL COMMENT '创建者ID',
  `CRT_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPD_USER_NAME` varchar(128) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL COMMENT '修改者ID',
  `UPD_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL COMMENT '部门ID 未使用的字段',
  `TENANT_ID` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USER_ID` varchar(32) DEFAULT NULL COMMENT '接收用户ID',
  `SENSOR_ID` bigint DEFAULT NULL COMMENT '传感器ID',
  PRIMARY KEY (`ID`,`SERVICE_SUPPLY_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='通知推送日志 ';

-- ----------------------------
-- Records of notice_log
-- ----------------------------

-- ----------------------------
-- Table structure for notice_rule
-- ----------------------------
DROP TABLE IF EXISTS `notice_rule`;
CREATE TABLE `notice_rule` (
  `ID` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `RULE_NAME` varchar(128) DEFAULT NULL COMMENT '规则名称',
  `RULE_DESCRIPTION` varchar(256) DEFAULT NULL COMMENT '规则描述',
  `INTERVAL_TIME_TXT` varchar(128) DEFAULT NULL COMMENT '间隔时间文字',
  `INTERVAL_TIME_MINUTES` int DEFAULT NULL COMMENT '间隔时间(分钟)',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记 1：删除，0：未删除',
  `CRT_USER_NAME` varchar(128) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL COMMENT '创建者ID',
  `CRT_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPD_USER_NAME` varchar(128) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL COMMENT '修改者ID',
  `UPD_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL COMMENT '部门ID 未使用的字段',
  `TENANT_ID` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='通知推送规则';

-- ----------------------------
-- Records of notice_rule
-- ----------------------------

-- ----------------------------
-- Table structure for notice_rule_sensor
-- ----------------------------
DROP TABLE IF EXISTS `notice_rule_sensor`;
CREATE TABLE `notice_rule_sensor` (
  `ID` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `NOTICE_RULE_ID` bigint DEFAULT NULL COMMENT '通知规则ID',
  `SENSOR_ID` bigint DEFAULT NULL COMMENT '设备ID 设备的ID',
  `CHANNEL_ID` int DEFAULT NULL COMMENT '所属系统ID',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记 1：删除，0：未删除',
  `CRT_USER_NAME` varchar(128) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL COMMENT '创建者ID',
  `CRT_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPD_USER_NAME` varchar(128) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL COMMENT '修改者ID',
  `UPD_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL COMMENT '部门ID 未使用的字段',
  `TENANT_ID` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='通知规则-设备关联表 规则-设备：1-N关联';

-- ----------------------------
-- Records of notice_rule_sensor
-- ----------------------------

-- ----------------------------
-- Table structure for notice_rule_user
-- ----------------------------
DROP TABLE IF EXISTS `notice_rule_user`;
CREATE TABLE `notice_rule_user` (
  `ID` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `NOTICE_RULE_ID` bigint DEFAULT NULL COMMENT '通知规则ID',
  `USER_ID` varchar(128) DEFAULT NULL COMMENT '用户ID',
  `DEL_FLAG` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记 1：删除，0：未删除',
  `CRT_USER_NAME` varchar(128) DEFAULT NULL COMMENT '创建者名称',
  `CRT_USER_ID` varchar(32) DEFAULT NULL COMMENT '创建者ID',
  `CRT_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPD_USER_NAME` varchar(128) DEFAULT NULL COMMENT '修改者名称',
  `UPD_USER_ID` varchar(32) DEFAULT NULL COMMENT '修改者ID',
  `UPD_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  `DEPART_ID` varchar(32) DEFAULT NULL COMMENT '部门ID 未使用的字段',
  `TENANT_ID` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `NOTICE_TYPE` char(1) DEFAULT '1' COMMENT '1:报警通知 2:故障通知',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='通知规则-用户关联表 ';

-- ----------------------------
-- Records of notice_rule_user
-- ----------------------------

-- ----------------------------
-- Table structure for warning
-- ----------------------------
DROP TABLE IF EXISTS `warning`;
CREATE TABLE `warning` (
  `id` int NOT NULL,
  `warning` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Bitcoin_Address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of warning
-- ----------------------------
