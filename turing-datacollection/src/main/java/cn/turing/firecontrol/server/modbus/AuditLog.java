package cn.turing.firecontrol.server.modbus;

import lombok.*;

import java.util.Date;


@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuditLog {
    private Integer logNum;

    private String logData;

    private String logType;

    private String logTableName;

    private Integer srcNum;

    private Date createDt;

    public Integer getLogNum() {
        return logNum;
    }

    public void setLogNum(Integer logNum) {
        this.logNum = logNum;
    }

    public String getLogData() {
        return logData;
    }

    public void setLogData(String logData) {
        this.logData = logData;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getLogTableName() {
        return logTableName;
    }

    public void setLogTableName(String logTableName) {
        this.logTableName = logTableName;
    }

    public Integer getSrcNum() {
        return srcNum;
    }

    public void setSrcNum(Integer srcNum) {
        this.srcNum = srcNum;
    }

    public Date getCreateDt() {
        return createDt;
    }

    public void setCreateDt(Date createDt) {
        this.createDt = createDt;
    }
}
