package cn.turing.firecontrol.server.modbus;

import cn.turing.firecontrol.server.modbus.Action;

import java.lang.annotation.*;
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuditAction {
    /**
     * 操作类型
     */
    Action action() default Action.GET;

    /**
     * 目标table
     */
    String targetTable() default "";
}
