package cn.turing.firecontrol.server.modbus;

public enum Action {

    ADD,
    DELETE,
    UPDATE,
    GET
}
